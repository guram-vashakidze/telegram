<?php

namespace Vashakidze\Telegram;

use Closure;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\AnswerCallbackQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerInlineQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerPreCheckoutQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerShippingQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerWebAppQuery;
use Vashakidze\Telegram\Api\InputTypes\ApproveChatJoinRequest;
use Vashakidze\Telegram\Api\InputTypes\BanChatMember;
use Vashakidze\Telegram\Api\InputTypes\BanChatSenderChat;
use Vashakidze\Telegram\Api\InputTypes\CopyMessage;
use Vashakidze\Telegram\Api\InputTypes\CreateChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\CreateInvoiceLink;
use Vashakidze\Telegram\Api\InputTypes\DeclineChatJoinRequest;
use Vashakidze\Telegram\Api\InputTypes\DeleteChatPhoto;
use Vashakidze\Telegram\Api\InputTypes\DeleteChatStickerSet;
use Vashakidze\Telegram\Api\InputTypes\DeleteMyCommands;
use Vashakidze\Telegram\Api\InputTypes\EditChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\EditMessageLiveLocation;
use Vashakidze\Telegram\Api\InputTypes\ExportChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\ForwardMessage;
use Vashakidze\Telegram\Api\InputTypes\GetChat;
use Vashakidze\Telegram\Api\InputTypes\GetChatAdministrators;
use Vashakidze\Telegram\Api\InputTypes\GetChatMember;
use Vashakidze\Telegram\Api\InputTypes\GetChatMemberCount;
use Vashakidze\Telegram\Api\InputTypes\GetChatMenuButton;
use Vashakidze\Telegram\Api\InputTypes\GetFile;
use Vashakidze\Telegram\Api\InputTypes\GetMyCommands;
use Vashakidze\Telegram\Api\InputTypes\GetMyDefaultAdministratorRights;
use Vashakidze\Telegram\Api\InputTypes\GetUpdates;
use Vashakidze\Telegram\Api\InputTypes\GetUserProfilePhotos;
use Vashakidze\Telegram\Api\InputTypes\InputFile;
use Vashakidze\Telegram\Api\InputTypes\LeaveChat;
use Vashakidze\Telegram\Api\InputTypes\PinChatMessage;
use Vashakidze\Telegram\Api\InputTypes\PromoteChatMember;
use Vashakidze\Telegram\Api\InputTypes\RestrictChatMember;
use Vashakidze\Telegram\Api\InputTypes\RevokeChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\SendAnimation;
use Vashakidze\Telegram\Api\InputTypes\SendAudio;
use Vashakidze\Telegram\Api\InputTypes\SendChatAction;
use Vashakidze\Telegram\Api\InputTypes\SendContact;
use Vashakidze\Telegram\Api\InputTypes\SendDice;
use Vashakidze\Telegram\Api\InputTypes\SendDocument;
use Vashakidze\Telegram\Api\InputTypes\SendInvoice;
use Vashakidze\Telegram\Api\InputTypes\SendLocation;
use Vashakidze\Telegram\Api\InputTypes\SendMediaGroup;
use Vashakidze\Telegram\Api\InputTypes\SendMessage;
use Vashakidze\Telegram\Api\InputTypes\SendPhoto;
use Vashakidze\Telegram\Api\InputTypes\SendPoll;
use Vashakidze\Telegram\Api\InputTypes\SendVenue;
use Vashakidze\Telegram\Api\InputTypes\SendVideo;
use Vashakidze\Telegram\Api\InputTypes\SendVideoNote;
use Vashakidze\Telegram\Api\InputTypes\SendVoice;
use Vashakidze\Telegram\Api\InputTypes\SetChatAdministratorCustomTitle;
use Vashakidze\Telegram\Api\InputTypes\SetChatDescription;
use Vashakidze\Telegram\Api\InputTypes\SetChatMenuButton;
use Vashakidze\Telegram\Api\InputTypes\SetChatPermissions;
use Vashakidze\Telegram\Api\InputTypes\SetChatPhoto;
use Vashakidze\Telegram\Api\InputTypes\SetChatStickerSet;
use Vashakidze\Telegram\Api\InputTypes\SetChatTitle;
use Vashakidze\Telegram\Api\InputTypes\SetMyCommands;
use Vashakidze\Telegram\Api\InputTypes\SetMyDefaultAdministratorRights;
use Vashakidze\Telegram\Api\InputTypes\SetWebhook;
use Vashakidze\Telegram\Api\InputTypes\StopMessageLiveLocation;
use Vashakidze\Telegram\Api\InputTypes\UnbanChatMember;
use Vashakidze\Telegram\Api\InputTypes\UnbanChatSenderChat;
use Vashakidze\Telegram\Api\InputTypes\UnpinAllChatMessages;
use Vashakidze\Telegram\Api\InputTypes\UnpinChatMessage;
use Vashakidze\Telegram\Api\TelegramObject;
use Vashakidze\Telegram\Api\Types\BotCommand;
use Vashakidze\Telegram\Api\Types\Chat;
use Vashakidze\Telegram\Api\Types\ChatAdministratorRights;
use Vashakidze\Telegram\Api\Types\ChatInviteLink;
use Vashakidze\Telegram\Api\Types\ChatMember;
use Vashakidze\Telegram\Api\Types\File;
use Vashakidze\Telegram\Api\Types\MenuButton;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageId;
use Vashakidze\Telegram\Api\Types\SentWebAppMessage;
use Vashakidze\Telegram\Api\Types\Update;
use Vashakidze\Telegram\Api\Types\User;
use Vashakidze\Telegram\Api\Types\UserProfilePhotos;
use Vashakidze\Telegram\Api\Types\WebhookInfo;
use Vashakidze\Telegram\Exceptions\TelegramApiException;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

use function array_map;

/**
 * Class TelegramApi
 * @package Vashakidze\Telegram
 *
 * @author Guram Vashakidze
 */
class TelegramApi
{
    public function __construct(protected string $token, protected string $url = 'https://api.telegram.org/')
    {
        $this->url = rtrim($this->url, '/') . '/';
    }

    /**
     * @param string $method
     * @param bool $post
     * @param array|null $args
     * @return array
     * @throws TelegramApiException
     */
    private function sendRequest(string $method, bool $post = false, ?array $args = null): mixed
    {
        $request = Http::acceptJson();
        $url = $this->getUrl($method);
        return $this->thrownResponse(
            fn() => !$post ? $request->get($url, $args) : $request->asJson()
                ->post($url, $args)
        );
    }

    /**
     * @param string $method
     * @param InputFile|InputFile[] $file
     * @param array $args
     * @return mixed
     * @throws TelegramApiException
     */
    private function sendMultipartRequest(string $method, array $file, array $args): mixed
    {
        $request = Http::acceptJson();
        foreach ($file as $item) {
            $request->attach($item->name, $item->contents, $item->filename);
        }
        return $this->thrownResponse(
            fn() => $request
                ->post($this->getUrl($method), $args)
        );
    }

    private function getUrl(string $method): string
    {
        return sprintf("%sbot%s/%s", $this->url, $this->token, $method);
    }

    /**
     * @param Closure $closure
     * @return mixed
     * @throws TelegramApiException
     */
    private function thrownResponse(Closure $closure): mixed
    {
        try {
            /** @var Response $response */
            $response = $closure();
        } catch (RequestException $exception) {
            throw new TelegramApiException(
                [
                    'response_code' => $exception->getResponse()
                        ->getStatusCode(),
                    'message' => $exception->getMessage()
                ]
            );
        } catch (ConnectionException $exception) {
            throw new TelegramApiException(
                [
                    'response_code' => null,
                    'message' => $exception->getMessage()
                ]
            );
        }
        if (!$response->successful()) {
            throw new TelegramApiException(
                [
                    'response_code' => $response->json('error_code'),
                    'message' => $response->json('description')
                ]
            );
        }
        return $response->json('result');
    }

    /**
     * @return User
     * @throws TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#getme
     */
    public function getMe(): User
    {
        $response = $this->sendRequest(__FUNCTION__);
        return User::init($response);
    }

    /**
     * @return bool
     * @throws TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#logout
     */
    public function logOut(): bool
    {
        return (bool)$this->sendRequest(__FUNCTION__);
    }

    /**
     * @return bool
     * @throws TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#close
     */
    public function close(): bool
    {
        return (bool)$this->sendRequest(__FUNCTION__);
    }

    /**
     * @return WebhookInfo
     * @throws TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#getwebhookinfo
     */
    public function getWebhookInfo(): WebhookInfo
    {
        return WebhookInfo::init(
            $this->sendRequest(__FUNCTION__)
        );
    }

    /**
     * @param SetWebhook $args
     * @return bool
     * @throws TelegramArgsException|TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#setwebhook
     */
    public function setWebhook(SetWebhook $args): bool
    {
        if (!isset($args->certificate)) {
            return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
        }
        $request = $args->toRequest();
        return $this->sendMultipartRequest(__FUNCTION__, $args->certificate, $request);
    }

    /**
     * @return bool
     * @throws TelegramApiException
     *
     * @link https://core.telegram.org/bots/api#deletewebhook
     */
    public function deleteWebhook(): bool
    {
        return $this->sendRequest(__FUNCTION__, true);
    }

    /**
     * @param GetUpdates|null $args
     * @return Update[]|null
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to receive incoming updates using long polling (wiki). An Array of Update objects is returned
     *
     * @link https://core.telegram.org/bots/api#getupdates
     */
    public function getUpdates(?GetUpdates $args = null): ?array
    {
        $response = $this->sendRequest(method: __FUNCTION__, args: $args?->toRequest());
        if (empty($response)) {
            return null;
        }
        return array_map(
            fn(array $item) => Update::init($item),
            $response
        );
    }

    /**
     * @param SendMessage $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send text messages. On success, the sent Message is returned
     *
     * @link https://core.telegram.org/bots/api#sendmessage
     */
    public function sendMessage(SendMessage $args): Message
    {
        return Message::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param ForwardMessage $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to forward messages of any kind. Service messages can't be forwarded. On success, the sent Message is returned
     *
     * @link https://core.telegram.org/bots/api#forwardmessage
     */
    public function forwardMessage(ForwardMessage $args): Message
    {
        return Message::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param CopyMessage $args
     * @return MessageId
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to copy messages of any kind. Service messages and invoice messages can't be copied. The method
     * is analogous to the method forwardMessage, but the copied message doesn't have a link to the original message.
     * Returns the MessageId of the sent message on success
     *
     * @link https://core.telegram.org/bots/api#copymessage
     */
    public function copyMessage(CopyMessage $args): MessageId
    {
        return MessageId::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param InputType $args
     * @param string $method
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     */
    private function sendFile(InputType $args, string $method): Message
    {
        $request = $args->toRequest();
        if (!$args->isSetFiles()) {
            return Message::init(
                $this->sendRequest($method, true, $request)
            );
        }
        return Message::init(
            $this->sendMultipartRequest($method, $args->getFiles(), $request)
        );
    }

    /**
     * @param SendPhoto $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send photos. On success, the sent Message is returned
     *
     * @link https://core.telegram.org/bots/api#sendphoto
     */
    public function sendPhoto(SendPhoto $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendAudio $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send audio files, if you want Telegram clients to display them in the music player. Your audio
     * must be in the .MP3 or .M4A format. On success, the sent Message is returned. Bots can currently send audio files
     * of up to 50 MB in size, this limit may be changed in the future
     *
     * @link https://core.telegram.org/bots/api#sendaudio
     */
    public function sendAudio(SendAudio $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendDocument $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send general files. On success, the sent Message is returned. Bots can currently send files of
     * any type of up to 50 MB in size, this limit may be changed in the future
     *
     * @link https://core.telegram.org/bots/api#senddocument
     */
    public function sendDocument(SendDocument $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendVideo $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send video files, Telegram clients support mp4 videos (other formats may be sent as Document).
     * On success, the sent Message is returned. Bots can currently send video files of up to 50 MB in size, this limit
     * may be changed in the future
     *
     * @link https://core.telegram.org/bots/api#sendvideo
     */
    public function sendVideo(SendVideo $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendAnimation $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound). On success, the sent
     * Message is returned. Bots can currently send animation files of up to 50 MB in size, this limit may be changed
     * in the future
     *
     * @link https://core.telegram.org/bots/api#sendanimation
     */
    public function sendAnimation(SendAnimation $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendVoice $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message.
     * For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or Document).
     * On success, the sent Message is returned. Bots can currently send voice messages of up to 50 MB in size, this limit
     * may be changed in the future
     *
     * @link https://core.telegram.org/bots/api#sendvoice
     */
    public function sendVoice(SendVoice $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendVideoNote $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * As of v.4.0, Telegram clients support rounded square mp4 videos of up to 1 minute long. Use this method to send
     * video messages. On success, the sent Message is returned
     *
     * @link https://core.telegram.org/bots/api#sendvideonote
     */
    public function sendVideoNote(SendVideoNote $args): Message
    {
        return $this->sendFile($args, __FUNCTION__);
    }

    /**
     * @param SendMediaGroup $args
     * @return Message[]
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can
     * be only grouped in an album with messages of the same type. On success, an array of Messages that were sent is
     * returned.
     *
     * @link https://core.telegram.org/bots/api#sendmediagroup
     */
    public function sendMediaGroup(SendMediaGroup $args): array
    {
        $request = $args->toRequest();
        $response = $this->sendMultipartRequest(__FUNCTION__, $args->getFiles(), $request);
        return array_map(
            fn(array $item) => Message::init($item),
            $response
        );
    }

    /**
     * @param string $method
     * @param TelegramObject $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     */
    private function sendRequestWithMessageResponse(string $method, TelegramObject $args): Message
    {
        return Message::init(
            $this->sendRequest($method, true, $args->toRequest())
        );
    }

    /**
     * @param SendLocation $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send point on the map. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#sendlocation
     */
    public function sendLocation(SendLocation $args): Message
    {
        return $this->sendRequestWithMessageResponse(__FUNCTION__, $args);
    }

    /**
     * @param EditMessageLiveLocation $args
     * @return Message|bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to edit live location messages. A location can be edited until its live_period expires or editing
     * is explicitly disabled by a call to stopMessageLiveLocation. On success, if the edited message is not an inline
     * message, the edited Message is returned, otherwise True is returned.
     *
     * @link https://core.telegram.org/bots/api#editmessagelivelocation
     */
    public function editMessageLiveLocation(EditMessageLiveLocation $args): Message|bool
    {
        $response = $this->sendRequest(__FUNCTION__, true, $args->toRequest());
        if (is_array($response) && !empty($response['message_id'])) {
            return Message::init($response);
        }
        return (bool)$response;
    }

    /**
     * @param StopMessageLiveLocation $args
     * @return Message|bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to stop updating a live location message before live_period expires. On success, if the message
     * is not an inline message, the edited Message is returned, otherwise True is returned.
     *
     * @link https://core.telegram.org/bots/api#stopmessagelivelocation
     */
    public function stopMessageLiveLocation(StopMessageLiveLocation $args): Message|bool
    {
        $response = $this->sendRequest(__FUNCTION__, true, $args->toRequest());
        if (is_array($response) && !empty($response['message_id'])) {
            return Message::init($response);
        }
        return (bool)$response;
    }

    /**
     * @param SendVenue $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send information about a venue. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#sendvenue
     */
    public function sendVenue(SendVenue $args): Message
    {
        return $this->sendRequestWithMessageResponse(__FUNCTION__, $args);
    }

    /**
     * @param SendContact $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send phone contacts. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#sendcontact
     */
    public function sendContact(SendContact $args): Message
    {
        return $this->sendRequestWithMessageResponse(__FUNCTION__, $args);
    }

    /**
     * @param SendPoll $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send a native poll. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#sendpoll
     */
    public function sendPoll(SendPoll $args): Message
    {
        return $this->sendRequestWithMessageResponse(__FUNCTION__, $args);
    }

    /**
     * @param SendDice $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send an animated emoji that will display a random value. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#senddice
     */
    public function sendDice(SendDice $args): Message
    {
        return $this->sendRequestWithMessageResponse(__FUNCTION__, $args);
    }

    /**
     * @param SendChatAction $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method when you need to tell the user that something is happening on the bot's side. The status is
     * set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status).
     * Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#sendchataction
     */
    public function sendChatAction(SendChatAction $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param GetUserProfilePhotos $args
     * @return UserProfilePhotos
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get a list of profile pictures for a user. Returns a UserProfilePhotos object. User photos can
     * be return only if user gave permissions.
     *
     * @link https://core.telegram.org/bots/api#getuserprofilephotos
     */
    public function getUserProfilePhotos(GetUserProfilePhotos $args): UserProfilePhotos
    {
        return UserProfilePhotos::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param GetFile $args
     * @return File
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get basic information about a file and prepare it for downloading. For the moment, bots can
     * download files of up to 20MB in size. On success, a File object is returned. The file can then be downloaded via
     * the link https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is taken from the response. It
     * is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested
     * by calling getFile again.
     *
     * @link https://core.telegram.org/bots/api#getfile
     */
    public function getFile(GetFile $args): File
    {
        return File::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param string $filePath
     * @return string
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function downloadFile(string $filePath): string
    {
        $url = sprintf("%sfile/bot%s/%s", $this->url, $this->token, $filePath);
        return Http::get($url)
            ->throw()
            ->body();
    }

    /**
     * @param BanChatMember $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to ban a user in a group, a supergroup or a channel. In the case of supergroups and channels, the
     * user will not be able to return to the chat on their own using invite links, etc., unless unbanned first. The bot
     * must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns
     * True on success.
     *
     * @link https://core.telegram.org/bots/api#banchatmember
     */
    public function banChatMember(BanChatMember $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param BanChatMember $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to unban a previously banned user in a supergroup or channel. The user will not return to the
     * group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this
     * to work. By default, this method guarantees that after the call the user is not a member of the chat, but will be
     * able to join it. So if the user is a member of the chat they will also be removed from the chat. If you don't want
     * this, use the parameter only_if_banned. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#unbanchatmember
     */
    public function unbanChatMember(UnbanChatMember $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param RestrictChatMember $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this
     * to work and must have the appropriate administrator rights. Pass True for all permissions to lift restrictions
     * from a user. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#restrictchatmember
     */
    public function restrictChatMember(RestrictChatMember $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param PromoteChatMember $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the
     * chat for this to work and must have the appropriate administrator rights. Pass False for all boolean parameters to
     * demote a user. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#promotechatmember
     */
    public function promoteChatMember(PromoteChatMember $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetChatAdministratorCustomTitle $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#setchatadministratorcustomtitle
     */
    public function setChatAdministratorCustomTitle(SetChatAdministratorCustomTitle $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param BanChatSenderChat $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to ban a channel chat in a supergroup or a channel. Until the chat is unbanned, the owner of the
     * banned chat won't be able to send messages on behalf of any of their channels. The bot must be an administrator
     * in the supergroup or channel for this to work and must have the appropriate administrator rights. Returns True on
     * success.
     *
     * @link https://core.telegram.org/bots/api#banchatsenderchat
     */
    public function banChatSenderChat(BanChatSenderChat $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param UnbanChatSenderChat $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to unban a previously banned channel chat in a supergroup or channel. The bot must be an
     * administrator for this to work and must have the appropriate administrator rights. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#unbanchatsenderchat
     */
    public function unbanChatSenderChat(UnbanChatSenderChat $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetChatPermissions $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to set default chat permissions for all members. The bot must be an administrator in the group or
     * a supergroup for this to work and must have the can_restrict_members administrator rights. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#setchatpermissions
     */
    public function setChatPermissions(SetChatPermissions $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param ExportChatInviteLink $args
     * @return string
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to generate a new primary invite link for a chat; any previously generated primary link is revoked.
     * The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
     * Returns the new invite link as String on success.
     *
     * @link https://core.telegram.org/bots/api#exportchatinvitelink
     */
    public function exportChatInviteLink(ExportChatInviteLink $args): string
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param CreateChatInviteLink $args
     * @return ChatInviteLink
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to create an additional invite link for a chat. The bot must be an administrator in the chat for
     * this to work and must have the appropriate administrator rights. The link can be revoked using the method
     * revokeChatInviteLink. Returns the new invite link as ChatInviteLink object.
     *
     * @link https://core.telegram.org/bots/api#createchatinvitelink
     */
    public function createChatInviteLink(CreateChatInviteLink $args): ChatInviteLink
    {
        return ChatInviteLink::init(
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param EditChatInviteLink $args
     * @return ChatInviteLink
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to edit a non-primary invite link created by the bot. The bot must be an administrator in the chat
     * for this to work and must have the appropriate administrator rights. Returns the edited invite link as
     * a ChatInviteLink object.
     *
     * @link https://core.telegram.org/bots/api#editchatinvitelink
     */
    public function editChatInviteLink(EditChatInviteLink $args): ChatInviteLink
    {
        return ChatInviteLink::init(
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param RevokeChatInviteLink $args
     * @return ChatInviteLink
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to revoke an invite link created by the bot. If the primary link is revoked, a new link is
     * automatically generated. The bot must be an administrator in the chat for this to work and must have the
     * appropriate administrator rights. Returns the revoked invite link as ChatInviteLink object.
     *
     * @link https://core.telegram.org/bots/api#revokechatinvitelink
     */
    public function revokeChatInviteLink(RevokeChatInviteLink $args): ChatInviteLink
    {
        return ChatInviteLink::init(
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param ApproveChatJoinRequest $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to approve a chat join request. The bot must be an administrator in the chat for this to work and
     * must have the can_invite_users administrator right. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#approvechatjoinrequest
     */
    public function approveChatJoinRequest(ApproveChatJoinRequest $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param DeclineChatJoinRequest $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to decline a chat join request. The bot must be an administrator in the chat for this to work and
     * must have the can_invite_users administrator right. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#declinechatjoinrequest
     */
    public function declineChatJoinRequest(DeclineChatJoinRequest $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetChatPhoto $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to set a new profile photo for the chat. Photos can't be changed for private chats. The bot must
     * be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns True
     * on success.
     *
     * @link https://core.telegram.org/bots/api#setchatphoto
     */
    public function setChatPhoto(SetChatPhoto $args): bool
    {
        $request = $args->toRequest();
        return $this->sendMultipartRequest(__FUNCTION__, $args->getFiles(), $request);
    }

    /**
     * @param DeleteChatPhoto $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to delete a chat photo. Photos can't be changed for private chats. The bot must be an administrator
     * in the chat for this to work and must have the appropriate administrator rights. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#deletechatphoto
     */
    public function deleteChatPhoto(DeleteChatPhoto $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetChatTitle $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to change the title of a chat. Titles can't be changed for private chats. The bot must be an
     * administrator in the chat for this to work and must have the appropriate administrator rights. Returns True on
     * success.
     *
     * @link https://core.telegram.org/bots/api#setchattitle
     */
    public function setChatTitle(SetChatTitle $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetChatDescription $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to change the description of a group, a supergroup or a channel. The bot must be an administrator
     * in the chat for this to work and must have the appropriate administrator rights. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#setchatdescription
     */
    public function setChatDescription(SetChatDescription $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param PinChatMessage $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to add a message to the list of pinned messages in a chat. If the chat is not a private chat, the
     * bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right
     * in a supergroup or 'can_edit_messages' administrator right in a channel. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#pinchatmessage
     */
    public function pinChatMessage(PinChatMessage $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param UnpinChatMessage $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to remove a message from the list of pinned messages in a chat. If the chat is not a private chat,
     * the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator
     * right in a supergroup or 'can_edit_messages' administrator right in a channel. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#unpinchatmessage
     */
    public function unpinChatMessage(UnpinChatMessage $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param UnpinAllChatMessages $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must
     * be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right in a
     * supergroup or 'can_edit_messages' administrator right in a channel. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#unpinallchatmessages
     */
    public function unpinAllChatMessages(UnpinAllChatMessages $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param LeaveChat $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method for your bot to leave a group, supergroup or channel. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#leavechat
     */
    public function leaveChat(LeaveChat $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param GetChat $args
     * @return Chat
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get up to date information about the chat (current name of the user for one-on-one conversations,
     * current username of a user, group or channel, etc.). Returns a Chat object on success.
     *
     * @link https://core.telegram.org/bots/api#getchat
     */
    public function getChat(GetChat $args): Chat
    {
        return Chat::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param GetChatAdministrators $args
     * @return ChatMember[]
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get a list of administrators in a chat, which aren't bots.
     *
     * @link https://core.telegram.org/bots/api#getchatadministrators
     */
    public function getChatAdministrators(GetChatAdministrators $args): array
    {
        return array_map(
            fn(array $item) => ChatMember::init($item),
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param GetChatMemberCount $args
     * @return int
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get the number of members in a chat.
     *
     * @link https://core.telegram.org/bots/api#getchatmembercount
     */
    public function getChatMemberCount(GetChatMemberCount $args): int
    {
        return $this->sendRequest(method: __FUNCTION__, args: $args->toRequest());
    }

    /**
     * @param GetChatMember $args
     * @return ChatMember
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get information about a member of a chat.
     *
     * @link https://core.telegram.org/bots/api#getchatmember
     */
    public function getChatMember(GetChatMember $args): ChatMember
    {
        return ChatMember::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param SetChatStickerSet $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to set a new group sticker set for a supergroup. The bot must be an administrator in the chat for
     * this to work and must have the appropriate administrator rights. Use the field can_set_sticker_set optionally
     * returned in getChat requests to check if the bot can use this method.
     *
     * @link https://core.telegram.org/bots/api#setchatstickerset
     */
    public function setChatStickerSet(SetChatStickerSet $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param DeleteChatStickerSet $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to delete a group sticker set from a supergroup. The bot must be an administrator in the chat for
     * this to work and must have the appropriate administrator rights. Use the field can_set_sticker_set optionally
     * returned in getChat requests to check if the bot can use this method.
     *
     * @link https://core.telegram.org/bots/api#deletechatstickerset
     */
    public function deleteChatStickerSet(DeleteChatStickerSet $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param AnswerCallbackQuery $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send answers to callback queries sent from inline keyboards. The answer will be displayed to
     * the user as a notification at the top of the chat screen or as an alert.
     *
     * @link https://core.telegram.org/bots/api#answercallbackquery
     */
    public function answerCallbackQuery(AnswerCallbackQuery $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param SetMyCommands $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to change the list of the bot's commands. See https://core.telegram.org/bots#commands for more
     * details about bot commands. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#setmycommands
     */
    public function setMyCommands(SetMyCommands $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param DeleteMyCommands $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to delete the list of the bot's commands for the given scope and user language. After deletion,
     * higher level commands will be shown to affected users. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#deletemycommands
     */
    public function deleteMyCommands(DeleteMyCommands $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param GetMyCommands $args
     * @return BotCommand[]
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to delete the list of the bot's commands for the given scope and user language. After deletion,
     * higher level commands will be shown to affected users. Returns True on success.
     *
     * @link https://core.telegram.org/bots/api#getmycommands
     */
    public function getMyCommands(GetMyCommands $args): array
    {
        return array_map(
            fn (array $item) => BotCommand::init($item),
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param SetChatMenuButton $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to change the bot's menu button in a private chat, or the default menu button. Returns True on
     * success.
     *
     * @link https://core.telegram.org/bots/api#setchatmenubutton
     */
    public function setChatMenuButton(SetChatMenuButton $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param GetChatMenuButton $args
     * @return MenuButton
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get the current value of the bot's menu button in a private chat, or the default menu button.
     *
     * @link https://core.telegram.org/bots/api#getchatmenubutton
     */
    public function getChatMenuButton(GetChatMenuButton $args): MenuButton
    {
        return MenuButton::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param SetMyDefaultAdministratorRights $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to change the default administrator rights requested by the bot when it's added as an administrator
     * to groups or channels. These rights will be suggested to users, but they are are free to modify the list before
     * adding the bot.
     *
     * @link https://core.telegram.org/bots/api#setmydefaultadministratorrights
     */
    public function setMyDefaultAdministratorRights(SetMyDefaultAdministratorRights $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param GetMyDefaultAdministratorRights $args
     * @return ChatAdministratorRights
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to get the current default administrator rights of the bot.
     *
     * @link https://core.telegram.org/bots/api#getmydefaultadministratorrights
     */
    public function getMyDefaultAdministratorRights(GetMyDefaultAdministratorRights $args): ChatAdministratorRights
    {
        return ChatAdministratorRights::init(
            $this->sendRequest(method: __FUNCTION__, args: $args->toRequest())
        );
    }

    /**
     * @param AnswerInlineQuery $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send answers to an inline query. On success, True is returned. No more than 50 results per
     * query are allowed.
     *
     * @link https://core.telegram.org/bots/api#answerinlinequery
     */
    public function answerInlineQuery(AnswerInlineQuery $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param AnswerWebAppQuery $args
     * @return SentWebAppMessage
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to set the result of an interaction with a Web App and send a corresponding message on behalf of
     * the user to the chat from which the query originated. On success, a SentWebAppMessage object is returned.
     *
     * @link https://core.telegram.org/bots/api#answerwebappquery
     */
    public function answerWebAppQuery(AnswerWebAppQuery $args): SentWebAppMessage
    {
        return SentWebAppMessage::init(
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param SendInvoice $args
     * @return Message
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to send invoices. On success, the sent Message is returned.
     *
     * @link https://core.telegram.org/bots/api#sendinvoice
     */
    public function sendInvoice(SendInvoice $args): Message
    {
        return Message::init(
            $this->sendRequest(__FUNCTION__, true, $args->toRequest())
        );
    }

    /**
     * @param CreateInvoiceLink $args
     * @return string
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Use this method to create a link for an invoice. Returns the created invoice link as String on success.
     *
     * @link https://core.telegram.org/bots/api#createinvoicelink
     */
    public function createInvoiceLink(CreateInvoiceLink $args): string
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param AnswerShippingQuery $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will
     * send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success,
     * True is returned.
     *
     * @link https://core.telegram.org/bots/api#answershippingquery
     */
    public function answerShippingQuery(AnswerShippingQuery $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }

    /**
     * @param AnswerPreCheckoutQuery $args
     * @return bool
     * @throws TelegramApiException
     * @throws TelegramArgsException
     *
     * Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the
     * form of an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries. On
     * success, True is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query
     * was sent.
     *
     * @link https://core.telegram.org/bots/api#answerprecheckoutquery
     */
    public function answerPreCheckoutQuery(AnswerPreCheckoutQuery $args): bool
    {
        return $this->sendRequest(__FUNCTION__, true, $args->toRequest());
    }
}

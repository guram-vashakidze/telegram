<?php

use Illuminate\Support\Facades\Route;
use Vashakidze\Telegram\Http\Controllers\WebhookController;
use Vashakidze\Telegram\Http\Middleware\WebhookMiddleware;

Route::middleware(WebhookMiddleware::class)
    ->post(
        'webhook/telegram',
        WebhookController::class
    )
    ->name('webhook.telegram');

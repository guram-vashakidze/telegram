<?php

namespace Vashakidze\Telegram\Api\Enums;

use BenSampo\Enum\Attributes\Description;
use Vashakidze\Telegram\Api\Attributes\CurrencyDescription;
use Vashakidze\Telegram\TelegramEnum;

/**
 * Class Currency
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @method static static AED()
 * @method static static AFN()
 * @method static static ALL()
 * @method static static AMD()
 * @method static static ARS()
 * @method static static AUD()
 * @method static static AZN()
 * @method static static BAM()
 * @method static static BDT()
 * @method static static BGN()
 * @method static static BND()
 * @method static static BOB()
 * @method static static BRL()
 * @method static static CAD()
 * @method static static CHF()
 * @method static static CLP()
 * @method static static CNY()
 * @method static static COP()
 * @method static static CRC()
 * @method static static CZK()
 * @method static static DKK()
 * @method static static DOP()
 * @method static static DZD()
 * @method static static EGP()
 * @method static static EUR()
 * @method static static GBP()
 * @method static static GEL()
 * @method static static GTQ()
 * @method static static HKD()
 * @method static static HNL()
 * @method static static HRK()
 * @method static static HUF()
 * @method static static IDR()
 * @method static static ILS()
 * @method static static INR()
 * @method static static ISK()
 * @method static static JMD()
 * @method static static JPY()
 * @method static static KES()
 * @method static static KGS()
 * @method static static KRW()
 * @method static static KZT()
 * @method static static LBP()
 * @method static static LKR()
 * @method static static MAD()
 * @method static static MDL()
 * @method static static MNT()
 * @method static static MUR()
 * @method static static MVR()
 * @method static static MXN()
 * @method static static MYR()
 * @method static static MZN()
 * @method static static NGN()
 * @method static static NIO()
 * @method static static NOK()
 * @method static static NPR()
 * @method static static NZD()
 * @method static static PAB()
 * @method static static PEN()
 * @method static static PHP()
 * @method static static PKR()
 * @method static static PLN()
 * @method static static PYG()
 * @method static static QAR()
 * @method static static RON()
 * @method static static RSD()
 * @method static static RUB()
 * @method static static SAR()
 * @method static static SEK()
 * @method static static SGD()
 * @method static static THB()
 * @method static static TJS()
 * @method static static TRY()
 * @method static static TTD()
 * @method static static TWD()
 * @method static static TZS()
 * @method static static UAH()
 * @method static static UGX()
 * @method static static USD()
 * @method static static UYU()
 * @method static static UZS()
 * @method static static VND()
 * @method static static YER()
 * @method static static ZAR()
 *
 * @method bool isAED()
 * @method bool isAFN()
 * @method bool isALL()
 * @method bool isAMD()
 * @method bool isARS()
 * @method bool isAUD()
 * @method bool isAZN()
 * @method bool isBAM()
 * @method bool isBDT()
 * @method bool isBGN()
 * @method bool isBND()
 * @method bool isBOB()
 * @method bool isBRL()
 * @method bool isCAD()
 * @method bool isCHF()
 * @method bool isCLP()
 * @method bool isCNY()
 * @method bool isCOP()
 * @method bool isCRC()
 * @method bool isCZK()
 * @method bool isDKK()
 * @method bool isDOP()
 * @method bool isDZD()
 * @method bool isEGP()
 * @method bool isEUR()
 * @method bool isGBP()
 * @method bool isGEL()
 * @method bool isGTQ()
 * @method bool isHKD()
 * @method bool isHNL()
 * @method bool isHRK()
 * @method bool isHUF()
 * @method bool isIDR()
 * @method bool isILS()
 * @method bool isINR()
 * @method bool isISK()
 * @method bool isJMD()
 * @method bool isJPY()
 * @method bool isKES()
 * @method bool isKGS()
 * @method bool isKRW()
 * @method bool isKZT()
 * @method bool isLBP()
 * @method bool isLKR()
 * @method bool isMAD()
 * @method bool isMDL()
 * @method bool isMNT()
 * @method bool isMUR()
 * @method bool isMVR()
 * @method bool isMXN()
 * @method bool isMYR()
 * @method bool isMZN()
 * @method bool isNGN()
 * @method bool isNIO()
 * @method bool isNOK()
 * @method bool isNPR()
 * @method bool isNZD()
 * @method bool isPAB()
 * @method bool isPEN()
 * @method bool isPHP()
 * @method bool isPKR()
 * @method bool isPLN()
 * @method bool isPYG()
 * @method bool isQAR()
 * @method bool isRON()
 * @method bool isRSD()
 * @method bool isRUB()
 * @method bool isSAR()
 * @method bool isSEK()
 * @method bool isSGD()
 * @method bool isTHB()
 * @method bool isTJS()
 * @method bool isTRY()
 * @method bool isTTD()
 * @method bool isTWD()
 * @method bool isTZS()
 * @method bool isUAH()
 * @method bool isUGX()
 * @method bool isUSD()
 * @method bool isUYU()
 * @method bool isUZS()
 * @method bool isVND()
 * @method bool isYER()
 * @method bool isZAR()
 *
 * @method bool isNotAED()
 * @method bool isNotAFN()
 * @method bool isNotALL()
 * @method bool isNotAMD()
 * @method bool isNotARS()
 * @method bool isNotAUD()
 * @method bool isNotAZN()
 * @method bool isNotBAM()
 * @method bool isNotBDT()
 * @method bool isNotBGN()
 * @method bool isNotBND()
 * @method bool isNotBOB()
 * @method bool isNotBRL()
 * @method bool isNotCAD()
 * @method bool isNotCHF()
 * @method bool isNotCLP()
 * @method bool isNotCNY()
 * @method bool isNotCOP()
 * @method bool isNotCRC()
 * @method bool isNotCZK()
 * @method bool isNotDKK()
 * @method bool isNotDOP()
 * @method bool isNotDZD()
 * @method bool isNotEGP()
 * @method bool isNotEUR()
 * @method bool isNotGBP()
 * @method bool isNotGEL()
 * @method bool isNotGTQ()
 * @method bool isNotHKD()
 * @method bool isNotHNL()
 * @method bool isNotHRK()
 * @method bool isNotHUF()
 * @method bool isNotIDR()
 * @method bool isNotILS()
 * @method bool isNotINR()
 * @method bool isNotISK()
 * @method bool isNotJMD()
 * @method bool isNotJPY()
 * @method bool isNotKES()
 * @method bool isNotKGS()
 * @method bool isNotKRW()
 * @method bool isNotKZT()
 * @method bool isNotLBP()
 * @method bool isNotLKR()
 * @method bool isNotMAD()
 * @method bool isNotMDL()
 * @method bool isNotMNT()
 * @method bool isNotMUR()
 * @method bool isNotMVR()
 * @method bool isNotMXN()
 * @method bool isNotMYR()
 * @method bool isNotMZN()
 * @method bool isNotNGN()
 * @method bool isNotNIO()
 * @method bool isNotNOK()
 * @method bool isNotNPR()
 * @method bool isNotNZD()
 * @method bool isNotPAB()
 * @method bool isNotPEN()
 * @method bool isNotPHP()
 * @method bool isNotPKR()
 * @method bool isNotPLN()
 * @method bool isNotPYG()
 * @method bool isNotQAR()
 * @method bool isNotRON()
 * @method bool isNotRSD()
 * @method bool isNotRUB()
 * @method bool isNotSAR()
 * @method bool isNotSEK()
 * @method bool isNotSGD()
 * @method bool isNotTHB()
 * @method bool isNotTJS()
 * @method bool isNotTRY()
 * @method bool isNotTTD()
 * @method bool isNotTWD()
 * @method bool isNotTZS()
 * @method bool isNotUAH()
 * @method bool isNotUGX()
 * @method bool isNotUSD()
 * @method bool isNotUYU()
 * @method bool isNotUZS()
 * @method bool isNotVND()
 * @method bool isNotYER()
 * @method bool isNotZAR()
 */
class Currency extends TelegramEnum
{
    #[Description('United Arab Emirates Dirham')]
    #[CurrencyDescription('د.إ.‏', ',', '.', true, true, 2, 367, 3673010)]
    public const AED = 'AED';
    #[Description('Afghan Afghani')]
    #[CurrencyDescription('؋', ',', '.', true, false, 2, 8700, 87000376)]
    public const AFN = 'AFN';
    #[Description('Albanian Lek')]
    #[CurrencyDescription('Lek', '.', ',', false, false, 2, 11559, 115592858)]
    public const ALL = 'ALL';
    #[Description('Armenian Dram')]
    #[CurrencyDescription('դր.', ',', '.', false, true, 2, 45499, 454990037)]
    public const AMD = 'AMD';
    #[Description('Argentine Peso')]
    #[CurrencyDescription('$', '.', ',', true, true, 2, 11780, 117803783)]
    public const ARS = 'ARS';
    #[Description('Australian Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 142, 1423380)]
    public const AUD = 'AUD';
    #[Description('Azerbaijani Manat')]
    #[CurrencyDescription('ман.', ' ', ',', false, true, 2, 173, 1735859)]
    public const AZN = 'AZN';
    #[Description('Bosnia & Herzegovina Convertible Mark')]
    #[CurrencyDescription('KM', '.', ',', false, true, 2, 187, 1874639)]
    public const BAM = 'BAM';
    #[Description('Bangladeshi Taka')]
    #[CurrencyDescription('৳', ',', '.', true, true, 2, 8757, 87574738)]
    public const BDT = 'BDT';
    #[Description('Bulgarian Lev')]
    #[CurrencyDescription('лв.', ' ', ',', false, true, 2, 186, 1869320)]
    public const BGN = 'BGN';
    #[Description('Brunei Dollar')]
    #[CurrencyDescription('$', '.', ',', true, false, 2, 139, 1393929)]
    public const BND = 'BND';
    #[Description('Bolivian Boliviano')]
    #[CurrencyDescription('Bs', '.', ',', true, true, 2, 688, 6886927)]
    public const BOB = 'BOB';
    #[Description('Brazilian Real')]
    #[CurrencyDescription('R$', '.', ',', true, true, 2, 506, 5061050)]
    public const BRL = 'BRL';
    #[Description('Canadian Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 128, 1281230)]
    public const CAD = 'CAD';
    #[Description('Swiss Franc')]
    #[CurrencyDescription('CHF', '\'', '.', false, true, 2, 99, 996545)]
    public const CHF = 'CHF';
    #[Description('Chilean Peso')]
    #[CurrencyDescription('$', '.', ',', true, true, 0, 857, 8577795)]
    public const CLP = 'CLP';
    #[Description('Chinese Renminbi Yuan')]
    #[CurrencyDescription('CN¥', ',', '.', true, false, 2, 674, 6747802)]
    public const CNY = 'CNY';
    #[Description('Colombian Peso')]
    #[CurrencyDescription('$', '.', ',', true, true, 2, 405700, 4057000000)]
    public const COP = 'COP';
    #[Description('Costa Rican Colón')]
    #[CurrencyDescription('₡', '.', ',', true, false, 2, 67162, 671622049)]
    public const CRC = 'CRC';
    #[Description('Czech Koruna')]
    #[CurrencyDescription('Kč', ' ', ',', false, true, 2, 2362, 23620804)]
    public const CZK = 'CZK';
    #[Description('Danish Krone')]
    #[CurrencyDescription('kr', '', ',', false, true, 2, 710, 7108030)]
    public const DKK = 'DKK';
    #[Description('Dominican Peso')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 5520, 55204543)]
    public const DOP = 'DOP';
    #[Description('Algerian Dinar')]
    #[CurrencyDescription('د.ج.‏', ',', '.', true, true, 2, 14619, 146192949)]
    public const DZD = 'DZD';
    #[Description('Egyptian Pound')]
    #[CurrencyDescription('ج.م.‏', ',', '.', true, true, 2, 1829, 18296101)]
    public const EGP = 'EGP';
    #[Description('Euro')]
    #[CurrencyDescription('€', ' ', ',', false, true, 2, 95, 955130)]
    public const EUR = 'EUR';
    #[Description('British Pound')]
    #[CurrencyDescription('£', ',', '.', true, false, 2, 80, 802810)]
    public const GBP = 'GBP';
    #[Description('Georgian Lari')]
    #[CurrencyDescription('GEL', ' ', ',', false, true, 2, 299, 2990071)]
    public const GEL = 'GEL';
    #[Description('Guatemalan Quetzal')]
    #[CurrencyDescription('Q', ',', '.', true, false, 2, 767, 7672696)]
    public const GTQ = 'GTQ';
    #[Description('Hong Kong Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 784, 7849050)]
    public const HKD = 'HKD';
    #[Description('Honduran Lempira')]
    #[CurrencyDescription('L', ',', '.', true, true, 2, 2447, 24474969)]
    public const HNL = 'HNL';
    #[Description('Croatian Kuna')]
    #[CurrencyDescription('kn', '.', ',', false, true, 2, 718, 7186964)]
    public const HRK = 'HRK';
    #[Description('Hungarian Forint')]
    #[CurrencyDescription('Ft', ' ', ',', false, true, 2, 37197, 371972956)]
    public const HUF = 'HUF';
    #[Description('Indonesian Rupiah')]
    #[CurrencyDescription('Rp', '.', ',', true, false, 2, 1463815, 14638150000)]
    public const IDR = 'IDR';
    #[Description('Israeli New Sheqel')]
    #[CurrencyDescription('₪', ',', '.', true, true, 2, 337, 3372420)]
    public const ILS = 'ILS';
    #[Description('Indian Rupee')]
    #[CurrencyDescription('₹', ',', '.', true, false, 2, 7763, 77633050)]
    public const INR = 'INR';
    #[Description('Icelandic Króna')]
    #[CurrencyDescription('kr', '.', ',', false, true, 0, 131, 1319301)]
    public const ISK = 'ISK';
    #[Description('Jamaican Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 15503, 155033499)]
    public const JMD = 'JMD';
    #[Description('Japanese Yen')]
    #[CurrencyDescription('￥', ',', '.', true, false, 0, 129, 1294944)]
    public const JPY = 'JPY';
    #[Description('Kenyan Shilling')]
    #[CurrencyDescription('Ksh', ',', '.', true, false, 2, 11629, 116299600)]
    public const KES = 'KES';
    #[Description('Kyrgyzstani Som')]
    #[CurrencyDescription('KGS', ' ', '-', false, true, 2, 8244, 82442298)]
    public const KGS = 'KGS';
    #[Description('South Korean Won')]
    #[CurrencyDescription('₩', ',', '.', true, false, 0, 1271, 12714097)]
    public const KRW = 'KRW';
    #[Description('Kazakhstani Tenge')]
    #[CurrencyDescription('₸', ' ', '-', true, false, 2, 43286, 432868152)]
    public const KZT = 'KZT';
    #[Description('Lebanese Pound')]
    #[CurrencyDescription('ل.ل.‏', ',', '.', true, true, 2, 156082, 1560822727)]
    public const LBP = 'LBP';
    #[Description('Sri Lankan Rupee')]
    #[CurrencyDescription('රු.', ',', '.', true, true, 2, 34933, 349330400)]
    public const LKR = 'LKR';
    #[Description('Moroccan Dirham')]
    #[CurrencyDescription('د.م.‏', ',', '.', true, true, 2, 1009, 10090500)]
    public const MAD = 'MAD';
    #[Description('Moldovan Leu')]
    #[CurrencyDescription('MDL', ',', '.', false, true, 2, 1896, 18965600)]
    public const MDL = 'MDL';
    #[Description('Mongolian Tögrög')]
    #[CurrencyDescription('MNT', ' ', ',', true, false, 2, 307577, 3075770799)]
    public const MNT = 'MNT';
    #[Description('Mauritian Rupee')]
    #[CurrencyDescription('MUR', ',', '.', true, false, 2, 4319, 43195892)]
    public const MUR = 'MUR';
    #[Description('Maldivian Rufiyaa')]
    #[CurrencyDescription('MVR', ',', '.', false, true, 2, 1550, 15504980)]
    public const MVR = 'MVR';
    #[Description('Mexican Peso')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 1999, 19998200)]
    public const MXN = 'MXN';
    #[Description('Malaysian Ringgit')]
    #[CurrencyDescription('RM', ',', '.', true, false, 2, 438, 4387503)]
    public const MYR = 'MYR';
    #[Description('Mozambican Metical')]
    #[CurrencyDescription('MTn', ',', '.', true, false, 2, 6382, 63829985)]
    public const MZN = 'MZN';
    #[Description('Nigerian Naira')]
    #[CurrencyDescription('₦', ',', '.', true, false, 2, 41525, 415250091)]
    public const NGN = 'NGN';
    #[Description('Nicaraguan Córdoba')]
    #[CurrencyDescription('C$', ',', '.', true, true, 2, 3576, 35760189)]
    public const NIO = 'NIO';
    #[Description('Norwegian Krone')]
    #[CurrencyDescription('kr', ' ', ',', true, true, 2, 966, 9667339)]
    public const NOK = 'NOK';
    #[Description('Nepalese Rupee')]
    #[CurrencyDescription('नेरू', ',', '.', true, false, 2, 12416, 124162521)]
    public const NPR = 'NPR';
    #[Description('New Zealand Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 157, 1573745)]
    public const NZD = 'NZD';
    #[Description('Panamanian Balboa')]
    #[CurrencyDescription('B/.', ',', '.', true, true, 2, 100, 1000278)]
    public const PAB = 'PAB';
    #[Description('Peruvian Nuevo Sol')]
    #[CurrencyDescription('S/.', ',', '.', true, true, 2, 377, 3770008)]
    public const PEN = 'PEN';
    #[Description('Philippine Peso')]
    #[CurrencyDescription('₱', ',', '.', true, false, 2, 5243, 52433502)]
    public const PHP = 'PHP';
    #[Description('Pakistani Rupee')]
    #[CurrencyDescription('₨', ',', '.', true, false, 2, 19364, 193649951)]
    public const PKR = 'PKR';
    #[Description('Polish Złoty')]
    #[CurrencyDescription('zł', ' ', ',', false, true, 2, 444, 4445605)]
    public const PLN = 'PLN';
    #[Description('Paraguayan Guaraní')]
    #[CurrencyDescription('₲', '.', ',', true, true, 0, 6881, 68819430)]
    public const PYG = 'PYG';
    #[Description('Qatari Riyal')]
    #[CurrencyDescription('ر.ق.‏', ',', '.', true, true, 2, 364, 3641017)]
    public const QAR = 'QAR';
    #[Description('Romanian Leu')]
    #[CurrencyDescription('RON', '.', ',', false, true, 2, 472, 4727103)]
    public const RON = 'RON';
    #[Description('Serbian Dinar')]
    #[CurrencyDescription('дин.', '.', ',', false, true, 2, 11223, 112234429)]
    public const RSD = 'RSD';
    #[Description('Russian Ruble')]
    #[CurrencyDescription('руб.', ' ', ',', false, true, 2, 6460, 64604991)]
    public const RUB = 'RUB';
    #[Description('Saudi Riyal')]
    #[CurrencyDescription('ر.س.‏', ',', '.', true, true, 2, 375, 3751344)]
    public const SAR = 'SAR';
    #[Description('Swedish Krona')]
    #[CurrencyDescription('kr', '.', ',', false, true, 2, 995, 9958560)]
    public const SEK = 'SEK';
    #[Description('Singapore Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 138, 1386125)]
    public const SGD = 'SGD';
    #[Description('Thai Baht')]
    #[CurrencyDescription('฿', ',', '.', true, false, 2, 3454, 34545497)]
    public const THB = 'THB';
    #[Description('Tajikistani Somoni')]
    #[CurrencyDescription('TJS', ' ', ';', false, true, 2, 1251, 12513419)]
    public const TJS = 'TJS';
    #[Description('Turkish Lira')]
    #[CurrencyDescription('TL', '.', ',', false, true, 2, 1584, 15842301)]
    public const TRY = 'TRY';
    #[Description('Trinidad and Tobago Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 681, 6814465)]
    public const TTD = 'TTD';
    #[Description('New Taiwan Dollar')]
    #[CurrencyDescription('NT$', ',', '.', true, false, 2, 2966, 29668010)]
    public const TWD = 'TWD';
    #[Description('Tanzanian Shilling')]
    #[CurrencyDescription('TSh', ',', '.', true, false, 2, 232500, 2325000573)]
    public const TZS = 'TZS';
    #[Description('Ukrainian Hryvnia')]
    #[CurrencyDescription('₴', ' ', ',', false, false, 2, 2955, 29552099)]
    public const UAH = 'UAH';
    #[Description('Ugandan Shilling')]
    #[CurrencyDescription('USh', ',', '.', true, false, 0, 3624, 36240774)]
    public const UGX = 'UGX';
    #[Description('United States Dollar')]
    #[CurrencyDescription('$', ',', '.', true, false, 2, 100, 1000000)]
    public const USD = 'USD';
    #[Description('Uruguayan Peso')]
    #[CurrencyDescription('$', '.', ',', true, true, 2, 4178, 41781446)]
    public const UYU = 'UYU';
    #[Description('Uzbekistani Som')]
    #[CurrencyDescription('UZS', ' ', ',', false, true, 2, 1116499, 11164999907)]
    public const UZS = 'UZS';
    #[Description('Vietnamese Đồng')]
    #[CurrencyDescription('₫', '.', ',', false, true, 0, 23119, 231195000)]
    public const VND = 'VND';
    #[Description('Yemeni Rial')]
    #[CurrencyDescription('ر.ي.‏', ',', '.', true, true, 2, 25030, 250301199)]
    public const YER = 'YER';
    #[Description('South African Rand')]
    #[CurrencyDescription('R', ',', '.', true, true, 2, 1606, 16067120)]
    public const ZAR = 'ZAR';
    protected static bool $useSnake = false;
    public string $code;
    public string $symbol;
    public string $thousandsSep;
    public string $decimalSep;
    public bool $symbolLeft;
    public bool $spaceBetween;
    public int $exp;
    public int $minAmount;
    public int $maxAmount;

    public function __construct($enumValue)
    {
        parent::__construct($enumValue);
        /**@var CurrencyDescription $description */
        $description = self::getReflection()
            ->getReflectionConstant($this->key)
            ->getAttributes(CurrencyDescription::class)[0]
            ->newInstance();
        $this->code = $this->value;
        $this->symbol = $description->symbol;
        $this->thousandsSep = $description->thousandsSep;
        $this->decimalSep = $description->decimalSep;
        $this->symbolLeft = $description->symbolLeft;
        $this->spaceBetween = $description->spaceBetween;
        $this->exp = $description->exp;
        $this->minAmount = $description->minAmount;
        $this->maxAmount = $description->maxAmount;
    }
}

<?php

namespace Vashakidze\Telegram\Api\Enums;

use Illuminate\Support\Str;
use Vashakidze\Telegram\TelegramEnum;

/**
 * Class ChatMemberType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * List member types for ChatMemberStatus
 *
 * @link https://core.telegram.org/bots/api#chatmember
 *
 * @method static static owner()
 * @method static static administrator()
 * @method static static member()
 * @method static static restricted()
 * @method static static left()
 * @method static static banned()
 *
 * @method bool isOwner()
 * @method bool isAdministrator()
 * @method bool isMember()
 * @method bool isRestricted()
 * @method bool isLeft()
 * @method bool isBanned()
 *
 * @method bool isNotOwner()
 * @method bool isNotAdministrator()
 * @method bool isNotMember()
 * @method bool isNotRestricted()
 * @method bool isNotLeft()
 * @method bool isNotBanned()
 */
final class ChatMemberType extends TelegramEnum
{
    public const owner = 'owner';
    public const administrator = 'administrator';
    public const member = 'member';
    public const restricted = 'restricted';
    public const left = 'left';
    public const banned = 'banned';

    public static function fromStatus(string $status): self
    {
        return match ($status) {
            ChatMemberStatus::creator => self::owner(),
            ChatMemberStatus::kicked => self::banned(),
            default => self::fromValue($status)
        };
    }

    public function toSetMethodName(): string
    {
        return 'set' . Str::studly($this->value);
    }
}

<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class InlineQueryResultType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresult
 *
 * @method static static article()
 * @method static static photo()
 * @method static static gif()
 * @method static static mpeg4_gif()
 * @method static static video()
 * @method static static audio()
 * @method static static voice()
 * @method static static document()
 * @method static static location()
 * @method static static venue()
 * @method static static contact()
 * @method static static game()
 * @method static static sticker()
 *
 * @method bool isArticle()
 * @method bool isPhoto()
 * @method bool isGif()
 * @method bool isMpeg4Gif()
 * @method bool isVideo()
 * @method bool isAudio()
 * @method bool isVoice()
 * @method bool isDocument()
 * @method bool isLocation()
 * @method bool isVenue()
 * @method bool isContact()
 * @method bool isGame()
 * @method bool isSticker()
 *
 * @method bool isNotArticle()
 * @method bool isNotPhoto()
 * @method bool isNotGif()
 * @method bool isNotMpeg4Gif()
 * @method bool isNotVideo()
 * @method bool isNotAudio()
 * @method bool isNotVoice()
 * @method bool isNotDocument()
 * @method bool isNotLocation()
 * @method bool isNotVenue()
 * @method bool isNotContact()
 * @method bool isNotGame()
 * @method bool isNotSticker()
 */
class InlineQueryResultType extends TelegramEnum
{
    public const article = 'article';
    public const photo = 'photo';
    public const gif = 'gif';
    public const mpeg4_gif = 'mpeg4_gif';
    public const video = 'video';
    public const audio = 'audio';
    public const voice = 'voice';
    public const document = 'document';
    public const location = 'location';
    public const venue = 'venue';
    public const contact = 'contact';
    public const game = 'game';
    public const sticker = 'sticker';
}

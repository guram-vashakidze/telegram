<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class GooglePlaceType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://developers.google.com/maps/documentation/places/web-service/supported_types#table1
 *
 * @method static static accounting()
 * @method static static airport()
 * @method static static amusement_park()
 * @method static static aquarium()
 * @method static static art_gallery()
 * @method static static atm()
 * @method static static bakery()
 * @method static static bank()
 * @method static static bar()
 * @method static static beauty_salon()
 * @method static static bicycle_store()
 * @method static static book_store()
 * @method static static bowling_alley()
 * @method static static bus_station()
 * @method static static cafe()
 * @method static static campground()
 * @method static static car_dealer()
 * @method static static car_rental()
 * @method static static car_repair()
 * @method static static car_wash()
 * @method static static casino()
 * @method static static cemetery()
 * @method static static church()
 * @method static static city_hall()
 * @method static static clothing_store()
 * @method static static convenience_store()
 * @method static static courthouse()
 * @method static static dentist()
 * @method static static department_store()
 * @method static static doctor()
 * @method static static drugstore()
 * @method static static electrician()
 * @method static static electronics_store()
 * @method static static embassy()
 * @method static static fire_station()
 * @method static static florist()
 * @method static static funeral_home()
 * @method static static furniture_store()
 * @method static static gas_station()
 * @method static static gym()
 * @method static static hair_care()
 * @method static static hardware_store()
 * @method static static hindu_temple()
 * @method static static home_goods_store()
 * @method static static hospital()
 * @method static static insurance_agency()
 * @method static static jewelry_store()
 * @method static static laundry()
 * @method static static lawyer()
 * @method static static library()
 * @method static static light_rail_station()
 * @method static static liquor_store()
 * @method static static local_government_office()
 * @method static static locksmith()
 * @method static static lodging()
 * @method static static meal_delivery()
 * @method static static meal_takeaway()
 * @method static static mosque()
 * @method static static movie_rental()
 * @method static static movie_theater()
 * @method static static moving_company()
 * @method static static museum()
 * @method static static night_club()
 * @method static static painter()
 * @method static static park()
 * @method static static parking()
 * @method static static pet_store()
 * @method static static pharmacy()
 * @method static static physiotherapist()
 * @method static static plumber()
 * @method static static police()
 * @method static static post_office()
 * @method static static primary_school()
 * @method static static real_estate_agency()
 * @method static static restaurant()
 * @method static static roofing_contractor()
 * @method static static rv_park()
 * @method static static school()
 * @method static static secondary_school()
 * @method static static shoe_store()
 * @method static static shopping_mall()
 * @method static static spa()
 * @method static static stadium()
 * @method static static storage()
 * @method static static store()
 * @method static static subway_station()
 * @method static static supermarket()
 * @method static static synagogue()
 * @method static static taxi_stand()
 * @method static static tourist_attraction()
 * @method static static train_station()
 * @method static static transit_station()
 * @method static static travel_agency()
 * @method static static university()
 * @method static static veterinary_care()
 * @method static static zoo()
 *
 * @method bool isAccounting()
 * @method bool isAirport()
 * @method bool isAmusementPark()
 * @method bool isAquarium()
 * @method bool isArtGallery()
 * @method bool isAtm()
 * @method bool isBakery()
 * @method bool isBank()
 * @method bool isBar()
 * @method bool isBeautySalon()
 * @method bool isBicycleStore()
 * @method bool isBookStore()
 * @method bool isBowlingAlley()
 * @method bool isBusStation()
 * @method bool isCafe()
 * @method bool isCampground()
 * @method bool isCarDealer()
 * @method bool isCarRental()
 * @method bool isCarRepair()
 * @method bool isCarWash()
 * @method bool isCasino()
 * @method bool isCemetery()
 * @method bool isChurch()
 * @method bool isCityHall()
 * @method bool isClothingStore()
 * @method bool isConvenienceStore()
 * @method bool isCourthouse()
 * @method bool isDentist()
 * @method bool isDepartmentStore()
 * @method bool isDoctor()
 * @method bool isDrugstore()
 * @method bool isElectrician()
 * @method bool isElectronicsStore()
 * @method bool isEmbassy()
 * @method bool isFireStation()
 * @method bool isFlorist()
 * @method bool isFuneralHome()
 * @method bool isFurnitureStore()
 * @method bool isGasStation()
 * @method bool isGym()
 * @method bool isHairCare()
 * @method bool isHardwareStore()
 * @method bool isHinduTemple()
 * @method bool isHomeGoodsStore()
 * @method bool isHospital()
 * @method bool isInsuranceAgency()
 * @method bool isJewelryStore()
 * @method bool isLaundry()
 * @method bool isLawyer()
 * @method bool isLibrary()
 * @method bool isLightRailStation()
 * @method bool isLiquorStore()
 * @method bool isLocalGovernmentOffice()
 * @method bool isLocksmith()
 * @method bool isLodging()
 * @method bool isMealDelivery()
 * @method bool isMealTakeaway()
 * @method bool isMosque()
 * @method bool isMovieRental()
 * @method bool isMovieTheater()
 * @method bool isMovingCompany()
 * @method bool isMuseum()
 * @method bool isNightClub()
 * @method bool isPainter()
 * @method bool isPark()
 * @method bool isParking()
 * @method bool isPetStore()
 * @method bool isPharmacy()
 * @method bool isPhysiotherapist()
 * @method bool isPlumber()
 * @method bool isPolice()
 * @method bool isPostOffice()
 * @method bool isPrimarySchool()
 * @method bool isRealEstateAgency()
 * @method bool isRestaurant()
 * @method bool isRoofingContractor()
 * @method bool isRvPark()
 * @method bool isSchool()
 * @method bool isSecondarySchool()
 * @method bool isShoeStore()
 * @method bool isShoppingMall()
 * @method bool isSpa()
 * @method bool isStadium()
 * @method bool isStorage()
 * @method bool isStore()
 * @method bool isSubwayStation()
 * @method bool isSupermarket()
 * @method bool isSynagogue()
 * @method bool isTaxiStand()
 * @method bool isTouristAttraction()
 * @method bool isTrainStation()
 * @method bool isTransitStation()
 * @method bool isTravelAgency()
 * @method bool isUniversity()
 * @method bool isVeterinaryCare()
 * @method bool isZoo()
 *
 * @method bool isNotAccounting()
 * @method bool isNotAirport()
 * @method bool isNotAmusementPark()
 * @method bool isNotAquarium()
 * @method bool isNotArtGallery()
 * @method bool isNotAtm()
 * @method bool isNotBakery()
 * @method bool isNotBank()
 * @method bool isNotBar()
 * @method bool isNotBeautySalon()
 * @method bool isNotBicycleStore()
 * @method bool isNotBookStore()
 * @method bool isNotBowlingAlley()
 * @method bool isNotBusStation()
 * @method bool isNotCafe()
 * @method bool isNotCampground()
 * @method bool isNotCarDealer()
 * @method bool isNotCarRental()
 * @method bool isNotCarRepair()
 * @method bool isNotCarWash()
 * @method bool isNotCasino()
 * @method bool isNotCemetery()
 * @method bool isNotChurch()
 * @method bool isNotCityHall()
 * @method bool isNotClothingStore()
 * @method bool isNotConvenienceStore()
 * @method bool isNotCourthouse()
 * @method bool isNotDentist()
 * @method bool isNotDepartmentStore()
 * @method bool isNotDoctor()
 * @method bool isNotDrugstore()
 * @method bool isNotElectrician()
 * @method bool isNotElectronicsStore()
 * @method bool isNotEmbassy()
 * @method bool isNotFireStation()
 * @method bool isNotFlorist()
 * @method bool isNotFuneralHome()
 * @method bool isNotFurnitureStore()
 * @method bool isNotGasStation()
 * @method bool isNotGym()
 * @method bool isNotHairCare()
 * @method bool isNotHardwareStore()
 * @method bool isNotHinduTemple()
 * @method bool isNotHomeGoodsStore()
 * @method bool isNotHospital()
 * @method bool isNotInsuranceAgency()
 * @method bool isNotJewelryStore()
 * @method bool isNotLaundry()
 * @method bool isNotLawyer()
 * @method bool isNotLibrary()
 * @method bool isNotLightRailStation()
 * @method bool isNotLiquorStore()
 * @method bool isNotLocalGovernmentOffice()
 * @method bool isNotLocksmith()
 * @method bool isNotLodging()
 * @method bool isNotMealDelivery()
 * @method bool isNotMealTakeaway()
 * @method bool isNotMosque()
 * @method bool isNotMovieRental()
 * @method bool isNotMovieTheater()
 * @method bool isNotMovingCompany()
 * @method bool isNotMuseum()
 * @method bool isNotNightClub()
 * @method bool isNotPainter()
 * @method bool isNotPark()
 * @method bool isNotParking()
 * @method bool isNotPetStore()
 * @method bool isNotPharmacy()
 * @method bool isNotPhysiotherapist()
 * @method bool isNotPlumber()
 * @method bool isNotPolice()
 * @method bool isNotPostOffice()
 * @method bool isNotPrimarySchool()
 * @method bool isNotRealEstateAgency()
 * @method bool isNotRestaurant()
 * @method bool isNotRoofingContractor()
 * @method bool isNotRvPark()
 * @method bool isNotSchool()
 * @method bool isNotSecondarySchool()
 * @method bool isNotShoeStore()
 * @method bool isNotShoppingMall()
 * @method bool isNotSpa()
 * @method bool isNotStadium()
 * @method bool isNotStorage()
 * @method bool isNotStore()
 * @method bool isNotSubwayStation()
 * @method bool isNotSupermarket()
 * @method bool isNotSynagogue()
 * @method bool isNotTaxiStand()
 * @method bool isNotTouristAttraction()
 * @method bool isNotTrainStation()
 * @method bool isNotTransitStation()
 * @method bool isNotTravelAgency()
 * @method bool isNotUniversity()
 * @method bool isNotVeterinaryCare()
 * @method bool isNotZoo()
 */
final class GooglePlaceType extends TelegramEnum
{
    public const accounting = 'accounting';
    public const airport = 'airport';
    public const amusement_park = 'amusement_park';
    public const aquarium = 'aquarium';
    public const art_gallery = 'art_gallery';
    public const atm = 'atm';
    public const bakery = 'bakery';
    public const bank = 'bank';
    public const bar = 'bar';
    public const beauty_salon = 'beauty_salon';
    public const bicycle_store = 'bicycle_store';
    public const book_store = 'book_store';
    public const bowling_alley = 'bowling_alley';
    public const bus_station = 'bus_station';
    public const cafe = 'cafe';
    public const campground = 'campground';
    public const car_dealer = 'car_dealer';
    public const car_rental = 'car_rental';
    public const car_repair = 'car_repair';
    public const car_wash = 'car_wash';
    public const casino = 'casino';
    public const cemetery = 'cemetery';
    public const church = 'church';
    public const city_hall = 'city_hall';
    public const clothing_store = 'clothing_store';
    public const convenience_store = 'convenience_store';
    public const courthouse = 'courthouse';
    public const dentist = 'dentist';
    public const department_store = 'department_store';
    public const doctor = 'doctor';
    public const drugstore = 'drugstore';
    public const electrician = 'electrician';
    public const electronics_store = 'electronics_store';
    public const embassy = 'embassy';
    public const fire_station = 'fire_station';
    public const florist = 'florist';
    public const funeral_home = 'funeral_home';
    public const furniture_store = 'furniture_store';
    public const gas_station = 'gas_station';
    public const gym = 'gym';
    public const hair_care = 'hair_care';
    public const hardware_store = 'hardware_store';
    public const hindu_temple = 'hindu_temple';
    public const home_goods_store = 'home_goods_store';
    public const hospital = 'hospital';
    public const insurance_agency = 'insurance_agency';
    public const jewelry_store = 'jewelry_store';
    public const laundry = 'laundry';
    public const lawyer = 'lawyer';
    public const library = 'library';
    public const light_rail_station = 'light_rail_station';
    public const liquor_store = 'liquor_store';
    public const local_government_office = 'local_government_office';
    public const locksmith = 'locksmith';
    public const lodging = 'lodging';
    public const meal_delivery = 'meal_delivery';
    public const meal_takeaway = 'meal_takeaway';
    public const mosque = 'mosque';
    public const movie_rental = 'movie_rental';
    public const movie_theater = 'movie_theater';
    public const moving_company = 'moving_company';
    public const museum = 'museum';
    public const night_club = 'night_club';
    public const painter = 'painter';
    public const park = 'park';
    public const parking = 'parking';
    public const pet_store = 'pet_store';
    public const pharmacy = 'pharmacy';
    public const physiotherapist = 'physiotherapist';
    public const plumber = 'plumber';
    public const police = 'police';
    public const post_office = 'post_office';
    public const primary_school = 'primary_school';
    public const real_estate_agency = 'real_estate_agency';
    public const restaurant = 'restaurant';
    public const roofing_contractor = 'roofing_contractor';
    public const rv_park = 'rv_park';
    public const school = 'school';
    public const secondary_school = 'secondary_school';
    public const shoe_store = 'shoe_store';
    public const shopping_mall = 'shopping_mall';
    public const spa = 'spa';
    public const stadium = 'stadium';
    public const storage = 'storage';
    public const store = 'store';
    public const subway_station = 'subway_station';
    public const supermarket = 'supermarket';
    public const synagogue = 'synagogue';
    public const taxi_stand = 'taxi_stand';
    public const tourist_attraction = 'tourist_attraction';
    public const train_station = 'train_station';
    public const transit_station = 'transit_station';
    public const travel_agency = 'travel_agency';
    public const university = 'university';
    public const veterinary_care = 'veterinary_care';
    public const zoo = 'zoo';
}

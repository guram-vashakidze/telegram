<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class TelegramPollTypes
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#poll
 *
 * @method static static regular()
 * @method static static quiz()
 *
 * @method bool isRegular()
 * @method bool isQuiz()
 *
 * @method bool isNotRegular()
 * @method bool isNotQuiz()
 */
final class PollType extends TelegramEnum
{
    public const regular = 'regular';
    public const quiz = 'quiz';
}

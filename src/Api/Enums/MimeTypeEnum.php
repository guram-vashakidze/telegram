<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class MimeTypeEnum
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @method static static image_jpeg()
 * @method static static image_gif()
 * @method static static video_mp4()
 * @method static static text_html()
 * @method static static application_pdf()
 * @method static static application_zip()
 *
 * @method bool isImageJpeg()
 * @method bool isImageGif()
 * @method bool isVideoMp4()
 * @method bool isTextHtml()
 * @method bool isApplicationPdf()
 * @method bool isApplicationZip()
 *
 * @method bool isNotImageJpeg()
 * @method bool isNotImageGif()
 * @method bool isNotVideoMp4()
 * @method bool isNotTextHtml()
 * @method bool isNotApplicationPdf()
 * @method bool isNotApplicationZip()
 */
class MimeTypeEnum extends TelegramEnum
{
    public const image_jpeg = 'image/jpeg';
    public const image_gif = 'image/gif';
    public const video_mp4 = 'video/mp4';
    public const text_html = 'text/html';
    public const application_pdf = 'application/pdf';
    public const application_zip = 'application/zip';

    public function isGifMimes(): bool
    {
        return in_array($this->value, self::gifMimes(), true);
    }

    public static function gifMimes(): array
    {
        return [
            self::image_gif,
            self::image_jpeg,
            self::video_mp4
        ];
    }

    public function isMpeg4GifMimes(): bool
    {
        return in_array($this->value, self::mpeg4GifMimes(), true);
    }

    public static function mpeg4GifMimes(): array
    {
        return [
            self::image_gif,
            self::image_jpeg,
            self::video_mp4
        ];
    }

    public function isVideoMimes(): bool
    {
        return in_array($this->value, self::videoMimes(), true);
    }

    public static function videoMimes(): array
    {
        return [
            self::video_mp4,
            self::text_html,
        ];
    }

    public function isDocumentMimes(): bool
    {
        return in_array($this->value, self::documentMimes(), true);
    }

    public static function documentMimes(): array
    {
        return [
            self::application_pdf,
            self::application_zip,
        ];
    }
}

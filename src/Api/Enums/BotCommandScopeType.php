<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class BotCommandScopeType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#botcommandscope
 *
 * @method static static default()
 * @method static static all_private_chats()
 * @method static static all_group_chats()
 * @method static static all_chat_administrators()
 * @method static static chat()
 * @method static static chat_administrators()
 * @method static static chat_member()
 *
 * @method bool isDefault()
 * @method bool isAllPrivateChats()
 * @method bool isAllGroupChats()
 * @method bool isAllChatAdministrators()
 * @method bool isChat()
 * @method bool isChatAdministrators()
 * @method bool isChatMember()
 *
 * @method bool isNotDefault()
 * @method bool isNotAllPrivateChats()
 * @method bool isNotAllGroupChats()
 * @method bool isNotAllChatAdministrators()
 * @method bool isNotChat()
 * @method bool isNotChatAdministrators()
 * @method bool isNotChatMember()
 */
class BotCommandScopeType extends TelegramEnum
{
    public const default = 'default';
    public const all_private_chats = 'all_private_chats';
    public const all_group_chats = 'all_group_chats';
    public const all_chat_administrators = 'all_chat_administrators';
    public const chat = 'chat';
    public const chat_administrators = 'chat_administrators';
    public const chat_member = 'chat_member';
}

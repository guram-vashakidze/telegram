<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class StickerType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#sticker
 *
 * @method static static regular()
 * @method static static mask()
 * @method static static custom_emoji()
 *
 * @method bool isRegular()
 * @method bool isMask()
 * @method bool isCustomEmoji()
 *
 * @method bool isNotRegular()
 * @method bool isNotMask()
 * @method bool isNotCustomEmoji()
 */
final class StickerType extends TelegramEnum
{
    public const regular = 'regular';
    public const mask = 'mask';
    public const custom_emoji = 'custom_emoji';
}

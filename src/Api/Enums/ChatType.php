<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class ChatType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#chat
 *
 * @method static static private ()
 * @method static static group()
 * @method static static supergroup()
 * @method static static channel()
 *
 * @method bool isPrivate ()
 * @method bool isGroup()
 * @method bool isSupergroup()
 * @method bool isChannel()
 *
 * @method bool isNotPrivate ()
 * @method bool isNotGroup()
 * @method bool isNotSupergroup()
 * @method bool isNotChannel()
 */
final class ChatType extends TelegramEnum
{
    public const private = 'private';
    public const group = 'group';
    public const supergroup = 'supergroup';
    public const channel = 'channel';
}

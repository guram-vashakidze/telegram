<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class ChatActionType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#sendchataction
 *
 * @method static static typing()
 * @method static static upload_photo()
 * @method static static record_video()
 * @method static static upload_video()
 * @method static static record_voice()
 * @method static static upload_voice()
 * @method static static upload_document()
 * @method static static choose_sticker()
 * @method static static find_location()
 * @method static static record_video_note()
 * @method static static upload_video_note()
 *
 * @method bool isTyping()
 * @method bool isUploadPhoto()
 * @method bool isRecordVideo()
 * @method bool isUploadVideo()
 * @method bool isRecordVoice()
 * @method bool isUploadVoice()
 * @method bool isUploadDocument()
 * @method bool isChooseSticker()
 * @method bool isFindLocation()
 * @method bool isRecordVideoNote()
 * @method bool isUploadVideoNote()
 *
 * @method bool isNotTyping()
 * @method bool isNotUploadPhoto()
 * @method bool isNotRecordVideo()
 * @method bool isNotUploadVideo()
 * @method bool isNotRecordVoice()
 * @method bool isNotUploadVoice()
 * @method bool isNotUploadDocument()
 * @method bool isNotChooseSticker()
 * @method bool isNotFindLocation()
 * @method bool isNotRecordVideoNote()
 * @method bool isNotUploadVideoNote()
 */
final class ChatActionType extends TelegramEnum
{
    public const typing = 'typing';
    public const upload_photo = 'upload_photo';
    public const record_video = 'record_video';
    public const upload_video = 'upload_video';
    public const record_voice = 'record_voice';
    public const upload_voice = 'upload_voice';
    public const upload_document = 'upload_document';
    public const choose_sticker = 'choose_sticker';
    public const find_location = 'find_location';
    public const record_video_note = 'record_video_note';
    public const upload_video_note = 'upload_video_note';
}

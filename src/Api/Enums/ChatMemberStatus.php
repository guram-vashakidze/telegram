<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class ChatMemberStatus
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#chatmember
 *
 * @method static static creator()
 * @method static static administrator()
 * @method static static member()
 * @method static static restricted()
 * @method static static left()
 * @method static static kicked()
 *
 * @method bool isCreator()
 * @method bool isAdministrator()
 * @method bool isMember()
 * @method bool isRestricted()
 * @method bool isLeft()
 * @method bool isKicked()
 *
 * @method bool isNotCreator()
 * @method bool isNotAdministrator()
 * @method bool isNotMember()
 * @method bool isNotRestricted()
 * @method bool isNotLeft()
 * @method bool isNotKicked()
 */
final class ChatMemberStatus extends TelegramEnum
{
    public const creator = 'creator';
    public const administrator = 'administrator';
    public const member = 'member';
    public const restricted = 'restricted';
    public const left = 'left';
    public const kicked = 'kicked';
}

<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class EmojiType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @method static static dice()
 * @method static static darts()
 * @method static static basketball()
 * @method static static football()
 * @method static static bowling()
 * @method static static slot_machine()
 *
 * @method bool isDice()
 * @method bool isDarts()
 * @method bool isBasketball()
 * @method bool isFootball()
 * @method bool isBowling()
 * @method bool isSlotMachine()
 *
 * @method bool isNotDice()
 * @method bool isNotDarts()
 * @method bool isNotBasketball()
 * @method bool isNotFootball()
 * @method bool isNotBowling()
 * @method bool isNotSlotMachine()
 */
final class EmojiType extends TelegramEnum
{
    public const dice = '🎲';
    public const darts = '🎯';
    public const basketball = '🏀';
    public const football = '⚽';
    public const bowling = '🎳';
    public const slot_machine = '🎰';
}

<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class LanguageCodes
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @method static static abkhazian()
 * @method static static afar()
 * @method static static afrikaans()
 * @method static static akan()
 * @method static static albanian()
 * @method static static amharic()
 * @method static static arabic()
 * @method static static aragonese()
 * @method static static armenian()
 * @method static static assamese()
 * @method static static avaric()
 * @method static static avestan()
 * @method static static aymara()
 * @method static static azerbaijani()
 * @method static static bambara()
 * @method static static bashkir()
 * @method static static basque()
 * @method static static belarusian()
 * @method static static bengali()
 * @method static static bihari()
 * @method static static bislama()
 * @method static static bosnian()
 * @method static static breton()
 * @method static static bulgarian()
 * @method static static burmese()
 * @method static static catalan()
 * @method static static central_khmer()
 * @method static static chamorro()
 * @method static static chechen()
 * @method static static chichewa()
 * @method static static chinese()
 * @method static static church_slavonic()
 * @method static static chuvash()
 * @method static static cornish()
 * @method static static corsican()
 * @method static static cree()
 * @method static static croatian()
 * @method static static czech()
 * @method static static danish()
 * @method static static divehi()
 * @method static static dutch()
 * @method static static dzongkha()
 * @method static static english()
 * @method static static esperanto()
 * @method static static estonian()
 * @method static static ewe()
 * @method static static faroese()
 * @method static static fijian()
 * @method static static finnish()
 * @method static static french()
 * @method static static fulah()
 * @method static static gaelic()
 * @method static static galician()
 * @method static static ganda()
 * @method static static georgian()
 * @method static static german()
 * @method static static gikuyu()
 * @method static static greek()
 * @method static static greenlandic()
 * @method static static guarani()
 * @method static static gujarati()
 * @method static static haitian()
 * @method static static hausa()
 * @method static static hebrew()
 * @method static static herero()
 * @method static static hindi()
 * @method static static hiri_motu()
 * @method static static hungarian()
 * @method static static icelandic()
 * @method static static ido()
 * @method static static igbo()
 * @method static static indonesian()
 * @method static static interlingua()
 * @method static static interlingue()
 * @method static static inuktitut()
 * @method static static inupiaq()
 * @method static static irish()
 * @method static static italian()
 * @method static static japanese()
 * @method static static javanese()
 * @method static static kannada()
 * @method static static kanuri()
 * @method static static kashmiri()
 * @method static static kazakh()
 * @method static static kinyarwanda()
 * @method static static komi()
 * @method static static kongo()
 * @method static static korean()
 * @method static static kwanyama()
 * @method static static kurdish()
 * @method static static kyrgyz()
 * @method static static lao()
 * @method static static latin()
 * @method static static latvian()
 * @method static static letzeburgesch()
 * @method static static limburgish()
 * @method static static lingala()
 * @method static static lithuanian()
 * @method static static luba_katanga()
 * @method static static macedonian()
 * @method static static malagasy()
 * @method static static malay()
 * @method static static malayalam()
 * @method static static maltese()
 * @method static static manx()
 * @method static static maori()
 * @method static static marathi()
 * @method static static marshallese()
 * @method static static moldovan_romanian()
 * @method static static mongolian()
 * @method static static nauru()
 * @method static static navajo()
 * @method static static northern_ndebele()
 * @method static static ndonga()
 * @method static static nepali()
 * @method static static northern_sami()
 * @method static static norwegian()
 * @method static static norwegian_bokmal()
 * @method static static norwegian_nynorsk()
 * @method static static nuosu()
 * @method static static occitan()
 * @method static static ojibwa()
 * @method static static oriya()
 * @method static static oromo()
 * @method static static ossetian()
 * @method static static pali()
 * @method static static panjabi()
 * @method static static pashto()
 * @method static static persian()
 * @method static static polish()
 * @method static static portuguese()
 * @method static static quechua()
 * @method static static romansh()
 * @method static static rundi()
 * @method static static russian()
 * @method static static samoan()
 * @method static static sango()
 * @method static static sanskrit()
 * @method static static sardinian()
 * @method static static serbian()
 * @method static static shona()
 * @method static static sindhi()
 * @method static static sinhala()
 * @method static static slovak()
 * @method static static slovenian()
 * @method static static somali()
 * @method static static sotho()
 * @method static static south_ndebele()
 * @method static static spanish()
 * @method static static sundanese()
 * @method static static swahili()
 * @method static static swati()
 * @method static static swedish()
 * @method static static tagalog()
 * @method static static tahitian()
 * @method static static tajik()
 * @method static static tamil()
 * @method static static tatar()
 * @method static static telugu()
 * @method static static thai()
 * @method static static tibetan()
 * @method static static tigrinya()
 * @method static static tonga()
 * @method static static tsonga()
 * @method static static tswana()
 * @method static static turkish()
 * @method static static turkmen()
 * @method static static twi()
 * @method static static uighur()
 * @method static static ukrainian()
 * @method static static urdu()
 * @method static static uzbek()
 * @method static static venda()
 * @method static static vietnamese()
 * @method static static volap_k()
 * @method static static walloon()
 * @method static static welsh()
 * @method static static western_frisian()
 * @method static static wolof()
 * @method static static xhosa()
 * @method static static yiddish()
 * @method static static yoruba()
 * @method static static zhuang()
 * @method static static zul()
 *
 * @method bool isAbkhazian()
 * @method bool isAfar()
 * @method bool isAfrikaans()
 * @method bool isAkan()
 * @method bool isAlbanian()
 * @method bool isAmharic()
 * @method bool isArabic()
 * @method bool isAragonese()
 * @method bool isArmenian()
 * @method bool isAssamese()
 * @method bool isAvaric()
 * @method bool isAvestan()
 * @method bool isAymara()
 * @method bool isAzerbaijani()
 * @method bool isBambara()
 * @method bool isBashkir()
 * @method bool isBasque()
 * @method bool isBelarusian()
 * @method bool isBengali()
 * @method bool isBihari()
 * @method bool isBislama()
 * @method bool isBosnian()
 * @method bool isBreton()
 * @method bool isBulgarian()
 * @method bool isBurmese()
 * @method bool isCatalan()
 * @method bool isCentralKhmer()
 * @method bool isChamorro()
 * @method bool isChechen()
 * @method bool isChichewa()
 * @method bool isChinese()
 * @method bool isChurchSlavonic()
 * @method bool isChuvash()
 * @method bool isCornish()
 * @method bool isCorsican()
 * @method bool isCree()
 * @method bool isCroatian()
 * @method bool isCzech()
 * @method bool isDanish()
 * @method bool isDivehi()
 * @method bool isDutch()
 * @method bool isDzongkha()
 * @method bool isEnglish()
 * @method bool isEsperanto()
 * @method bool isEstonian()
 * @method bool isEwe()
 * @method bool isFaroese()
 * @method bool isFijian()
 * @method bool isFinnish()
 * @method bool isFrench()
 * @method bool isFulah()
 * @method bool isGaelic()
 * @method bool isGalician()
 * @method bool isGanda()
 * @method bool isGeorgian()
 * @method bool isGerman()
 * @method bool isGikuyu()
 * @method bool isGreek()
 * @method bool isGreenlandic()
 * @method bool isGuarani()
 * @method bool isGujarati()
 * @method bool isHaitian()
 * @method bool isHausa()
 * @method bool isHebrew()
 * @method bool isHerero()
 * @method bool isHindi()
 * @method bool isHiriMotu()
 * @method bool isHungarian()
 * @method bool isIcelandic()
 * @method bool isIdo()
 * @method bool isIgbo()
 * @method bool isIndonesian()
 * @method bool isInterlingua()
 * @method bool isInterlingue()
 * @method bool isInuktitut()
 * @method bool isInupiaq()
 * @method bool isIrish()
 * @method bool isItalian()
 * @method bool isJapanese()
 * @method bool isJavanese()
 * @method bool isKannada()
 * @method bool isKanuri()
 * @method bool isKashmiri()
 * @method bool isKazakh()
 * @method bool isKinyarwanda()
 * @method bool isKomi()
 * @method bool isKongo()
 * @method bool isKorean()
 * @method bool isKwanyama()
 * @method bool isKurdish()
 * @method bool isKyrgyz()
 * @method bool isLao()
 * @method bool isLatin()
 * @method bool isLatvian()
 * @method bool isLetzeburgesch()
 * @method bool isLimburgish()
 * @method bool isLingala()
 * @method bool isLithuanian()
 * @method bool isLubaKatanga()
 * @method bool isMacedonian()
 * @method bool isMalagasy()
 * @method bool isMalay()
 * @method bool isMalayalam()
 * @method bool isMaltese()
 * @method bool isManx()
 * @method bool isMaori()
 * @method bool isMarathi()
 * @method bool isMarshallese()
 * @method bool isMoldovanRomanian()
 * @method bool isMongolian()
 * @method bool isNauru()
 * @method bool isNavajo()
 * @method bool isNorthernNdebele()
 * @method bool isNdonga()
 * @method bool isNepali()
 * @method bool isNorthernSami()
 * @method bool isNorwegian()
 * @method bool isNorwegianBokmal()
 * @method bool isNorwegianNynorsk()
 * @method bool isNuosu()
 * @method bool isOccitan()
 * @method bool isOjibwa()
 * @method bool isOriya()
 * @method bool isOromo()
 * @method bool isOssetian()
 * @method bool isPali()
 * @method bool isPanjabi()
 * @method bool isPashto()
 * @method bool isPersian()
 * @method bool isPolish()
 * @method bool isPortuguese()
 * @method bool isQuechua()
 * @method bool isRomansh()
 * @method bool isRundi()
 * @method bool isRussian()
 * @method bool isSamoan()
 * @method bool isSango()
 * @method bool isSanskrit()
 * @method bool isSardinian()
 * @method bool isSerbian()
 * @method bool isShona()
 * @method bool isSindhi()
 * @method bool isSinhala()
 * @method bool isSlovak()
 * @method bool isSlovenian()
 * @method bool isSomali()
 * @method bool isSotho()
 * @method bool isSouthNdebele()
 * @method bool isSpanish()
 * @method bool isSundanese()
 * @method bool isSwahili()
 * @method bool isSwati()
 * @method bool isSwedish()
 * @method bool isTagalog()
 * @method bool isTahitian()
 * @method bool isTajik()
 * @method bool isTamil()
 * @method bool isTatar()
 * @method bool isTelugu()
 * @method bool isThai()
 * @method bool isTibetan()
 * @method bool isTigrinya()
 * @method bool isTonga()
 * @method bool isTsonga()
 * @method bool isTswana()
 * @method bool isTurkish()
 * @method bool isTurkmen()
 * @method bool isTwi()
 * @method bool isUighur()
 * @method bool isUkrainian()
 * @method bool isUrdu()
 * @method bool isUzbek()
 * @method bool isVenda()
 * @method bool isVietnamese()
 * @method bool isVolapK()
 * @method bool isWalloon()
 * @method bool isWelsh()
 * @method bool isWesternFrisian()
 * @method bool isWolof()
 * @method bool isXhosa()
 * @method bool isYiddish()
 * @method bool isYoruba()
 * @method bool isZhuang()
 * @method bool isZul()
 *
 * @method bool isNotAbkhazian()
 * @method bool isNotAfar()
 * @method bool isNotAfrikaans()
 * @method bool isNotAkan()
 * @method bool isNotAlbanian()
 * @method bool isNotAmharic()
 * @method bool isNotArabic()
 * @method bool isNotAragonese()
 * @method bool isNotArmenian()
 * @method bool isNotAssamese()
 * @method bool isNotAvaric()
 * @method bool isNotAvestan()
 * @method bool isNotAymara()
 * @method bool isNotAzerbaijani()
 * @method bool isNotBambara()
 * @method bool isNotBashkir()
 * @method bool isNotBasque()
 * @method bool isNotBelarusian()
 * @method bool isNotBengali()
 * @method bool isNotBihari()
 * @method bool isNotBislama()
 * @method bool isNotBosnian()
 * @method bool isNotBreton()
 * @method bool isNotBulgarian()
 * @method bool isNotBurmese()
 * @method bool isNotCatalan()
 * @method bool isNotCentralKhmer()
 * @method bool isNotChamorro()
 * @method bool isNotChechen()
 * @method bool isNotChichewa()
 * @method bool isNotChinese()
 * @method bool isNotChurchSlavonic()
 * @method bool isNotChuvash()
 * @method bool isNotCornish()
 * @method bool isNotCorsican()
 * @method bool isNotCree()
 * @method bool isNotCroatian()
 * @method bool isNotCzech()
 * @method bool isNotDanish()
 * @method bool isNotDivehi()
 * @method bool isNotDutch()
 * @method bool isNotDzongkha()
 * @method bool isNotEnglish()
 * @method bool isNotEsperanto()
 * @method bool isNotEstonian()
 * @method bool isNotEwe()
 * @method bool isNotFaroese()
 * @method bool isNotFijian()
 * @method bool isNotFinnish()
 * @method bool isNotFrench()
 * @method bool isNotFulah()
 * @method bool isNotGaelic()
 * @method bool isNotGalician()
 * @method bool isNotGanda()
 * @method bool isNotGeorgian()
 * @method bool isNotGerman()
 * @method bool isNotGikuyu()
 * @method bool isNotGreek()
 * @method bool isNotGreenlandic()
 * @method bool isNotGuarani()
 * @method bool isNotGujarati()
 * @method bool isNotHaitian()
 * @method bool isNotHausa()
 * @method bool isNotHebrew()
 * @method bool isNotHerero()
 * @method bool isNotHindi()
 * @method bool isNotHiriMotu()
 * @method bool isNotHungarian()
 * @method bool isNotIcelandic()
 * @method bool isNotIdo()
 * @method bool isNotIgbo()
 * @method bool isNotIndonesian()
 * @method bool isNotInterlingua()
 * @method bool isNotInterlingue()
 * @method bool isNotInuktitut()
 * @method bool isNotInupiaq()
 * @method bool isNotIrish()
 * @method bool isNotItalian()
 * @method bool isNotJapanese()
 * @method bool isNotJavanese()
 * @method bool isNotKannada()
 * @method bool isNotKanuri()
 * @method bool isNotKashmiri()
 * @method bool isNotKazakh()
 * @method bool isNotKinyarwanda()
 * @method bool isNotKomi()
 * @method bool isNotKongo()
 * @method bool isNotKorean()
 * @method bool isNotKwanyama()
 * @method bool isNotKurdish()
 * @method bool isNotKyrgyz()
 * @method bool isNotLao()
 * @method bool isNotLatin()
 * @method bool isNotLatvian()
 * @method bool isNotLetzeburgesch()
 * @method bool isNotLimburgish()
 * @method bool isNotLingala()
 * @method bool isNotLithuanian()
 * @method bool isNotLubaKatanga()
 * @method bool isNotMacedonian()
 * @method bool isNotMalagasy()
 * @method bool isNotMalay()
 * @method bool isNotMalayalam()
 * @method bool isNotMaltese()
 * @method bool isNotManx()
 * @method bool isNotMaori()
 * @method bool isNotMarathi()
 * @method bool isNotMarshallese()
 * @method bool isNotMoldovanRomanian()
 * @method bool isNotMongolian()
 * @method bool isNotNauru()
 * @method bool isNotNavajo()
 * @method bool isNotNorthernNdebele()
 * @method bool isNotNdonga()
 * @method bool isNotNepali()
 * @method bool isNotNorthernSami()
 * @method bool isNotNorwegian()
 * @method bool isNotNorwegianBokmal()
 * @method bool isNotNorwegianNynorsk()
 * @method bool isNotNuosu()
 * @method bool isNotOccitan()
 * @method bool isNotOjibwa()
 * @method bool isNotOriya()
 * @method bool isNotOromo()
 * @method bool isNotOssetian()
 * @method bool isNotPali()
 * @method bool isNotPanjabi()
 * @method bool isNotPashto()
 * @method bool isNotPersian()
 * @method bool isNotPolish()
 * @method bool isNotPortuguese()
 * @method bool isNotQuechua()
 * @method bool isNotRomansh()
 * @method bool isNotRundi()
 * @method bool isNotRussian()
 * @method bool isNotSamoan()
 * @method bool isNotSango()
 * @method bool isNotSanskrit()
 * @method bool isNotSardinian()
 * @method bool isNotSerbian()
 * @method bool isNotShona()
 * @method bool isNotSindhi()
 * @method bool isNotSinhala()
 * @method bool isNotSlovak()
 * @method bool isNotSlovenian()
 * @method bool isNotSomali()
 * @method bool isNotSotho()
 * @method bool isNotSouthNdebele()
 * @method bool isNotSpanish()
 * @method bool isNotSundanese()
 * @method bool isNotSwahili()
 * @method bool isNotSwati()
 * @method bool isNotSwedish()
 * @method bool isNotTagalog()
 * @method bool isNotTahitian()
 * @method bool isNotTajik()
 * @method bool isNotTamil()
 * @method bool isNotTatar()
 * @method bool isNotTelugu()
 * @method bool isNotThai()
 * @method bool isNotTibetan()
 * @method bool isNotTigrinya()
 * @method bool isNotTonga()
 * @method bool isNotTsonga()
 * @method bool isNotTswana()
 * @method bool isNotTurkish()
 * @method bool isNotTurkmen()
 * @method bool isNotTwi()
 * @method bool isNotUighur()
 * @method bool isNotUkrainian()
 * @method bool isNotUrdu()
 * @method bool isNotUzbek()
 * @method bool isNotVenda()
 * @method bool isNotVietnamese()
 * @method bool isNotVolapK()
 * @method bool isNotWalloon()
 * @method bool isNotWelsh()
 * @method bool isNotWesternFrisian()
 * @method bool isNotWolof()
 * @method bool isNotXhosa()
 * @method bool isNotYiddish()
 * @method bool isNotYoruba()
 * @method bool isNotZhuang()
 * @method bool isNotZul()
 */
class LanguageCode extends TelegramEnum
{
    public const abkhazian = 'ab';
    public const afar = 'aa';
    public const afrikaans = 'af';
    public const akan = 'ak';
    public const albanian = 'sq';
    public const amharic = 'am';
    public const arabic = 'ar';
    public const aragonese = 'an';
    public const armenian = 'hy';
    public const assamese = 'as';
    public const avaric = 'av';
    public const avestan = 'ae';
    public const aymara = 'ay';
    public const azerbaijani = 'az';
    public const bambara = 'bm';
    public const bashkir = 'ba';
    public const basque = 'eu';
    public const belarusian = 'be';
    public const bengali = 'bn';
    public const bihari = 'bh';
    public const bislama = 'bi';
    public const bosnian = 'bs';
    public const breton = 'br';
    public const bulgarian = 'bg';
    public const burmese = 'my';
    public const catalan = 'ca';
    public const central_khmer = 'km';
    public const chamorro = 'ch';
    public const chechen = 'ce';
    public const chichewa = 'ny';
    public const chinese = 'zh';
    public const church_slavonic = 'cu';
    public const chuvash = 'cv';
    public const cornish = 'kw';
    public const corsican = 'co';
    public const cree = 'cr';
    public const croatian = 'hr';
    public const czech = 'cs';
    public const danish = 'da';
    public const divehi = 'dv';
    public const dutch = 'nl';
    public const dzongkha = 'dz';
    public const english = 'en';
    public const esperanto = 'eo';
    public const estonian = 'et';
    public const ewe = 'ee';
    public const faroese = 'fo';
    public const fijian = 'fj';
    public const finnish = 'fi';
    public const french = 'fr';
    public const fulah = 'ff';
    public const gaelic = 'gd';
    public const galician = 'gl';
    public const ganda = 'lg';
    public const georgian = 'ka';
    public const german = 'de';
    public const gikuyu = 'ki';
    public const greek = 'el';
    public const greenlandic = 'kl';
    public const guarani = 'gn';
    public const gujarati = 'gu';
    public const haitian = 'ht';
    public const hausa = 'ha';
    public const hebrew = 'he';
    public const herero = 'hz';
    public const hindi = 'hi';
    public const hiri_motu = 'ho';
    public const hungarian = 'hu';
    public const icelandic = 'is';
    public const ido = 'io';
    public const igbo = 'ig';
    public const indonesian = 'id';
    public const interlingua = 'ia';
    public const interlingue = 'ie';
    public const inuktitut = 'iu';
    public const inupiaq = 'ik';
    public const irish = 'ga';
    public const italian = 'it';
    public const japanese = 'ja';
    public const javanese = 'jv';
    public const kannada = 'kn';
    public const kanuri = 'kr';
    public const kashmiri = 'ks';
    public const kazakh = 'kk';
    public const kinyarwanda = 'rw';
    public const komi = 'kv';
    public const kongo = 'kg';
    public const korean = 'ko';
    public const kwanyama = 'kj';
    public const kurdish = 'ku';
    public const kyrgyz = 'ky';
    public const lao = 'lo';
    public const latin = 'la';
    public const latvian = 'lv';
    public const letzeburgesch = 'lb';
    public const limburgish = 'li';
    public const lingala = 'ln';
    public const lithuanian = 'lt';
    public const luba_katanga = 'lu';
    public const macedonian = 'mk';
    public const malagasy = 'mg';
    public const malay = 'ms';
    public const malayalam = 'ml';
    public const maltese = 'mt';
    public const manx = 'gv';
    public const maori = 'mi';
    public const marathi = 'mr';
    public const marshallese = 'mh';
    public const moldovan_romanian = 'ro';
    public const mongolian = 'mn';
    public const nauru = 'na';
    public const navajo = 'nv';
    public const northern_ndebele = 'nd';
    public const ndonga = 'ng';
    public const nepali = 'ne';
    public const northern_sami = 'se';
    public const norwegian = 'no';
    public const norwegian_bokmal = 'nb';
    public const norwegian_nynorsk = 'nn';
    public const nuosu = 'ii';
    public const occitan = 'oc';
    public const ojibwa = 'oj';
    public const oriya = 'or';
    public const oromo = 'om';
    public const ossetian = 'os';
    public const pali = 'pi';
    public const panjabi = 'pa';
    public const pashto = 'ps';
    public const persian = 'fa';
    public const polish = 'pl';
    public const portuguese = 'pt';
    public const quechua = 'qu';
    public const romansh = 'rm';
    public const rundi = 'rn';
    public const russian = 'ru';
    public const samoan = 'sm';
    public const sango = 'sg';
    public const sanskrit = 'sa';
    public const sardinian = 'sc';
    public const serbian = 'sr';
    public const shona = 'sn';
    public const sindhi = 'sd';
    public const sinhala = 'si';
    public const slovak = 'sk';
    public const slovenian = 'sl';
    public const somali = 'so';
    public const sotho = 'st';
    public const south_ndebele = 'nr';
    public const spanish = 'es';
    public const sundanese = 'su';
    public const swahili = 'sw';
    public const swati = 'ss';
    public const swedish = 'sv';
    public const tagalog = 'tl';
    public const tahitian = 'ty';
    public const tajik = 'tg';
    public const tamil = 'ta';
    public const tatar = 'tt';
    public const telugu = 'te';
    public const thai = 'th';
    public const tibetan = 'bo';
    public const tigrinya = 'ti';
    public const tonga = 'to';
    public const tsonga = 'ts';
    public const tswana = 'tn';
    public const turkish = 'tr';
    public const turkmen = 'tk';
    public const twi = 'tw';
    public const uighur = 'ug';
    public const ukrainian = 'uk';
    public const urdu = 'ur';
    public const uzbek = 'uz';
    public const venda = 've';
    public const vietnamese = 'vi';
    public const volap_k = 'vo';
    public const walloon = 'wa';
    public const welsh = 'cy';
    public const western_frisian = 'fy';
    public const wolof = 'wo';
    public const xhosa = 'xh';
    public const yiddish = 'yi';
    public const yoruba = 'yo';
    public const zhuang = 'za';
    public const zul = 'zu';
}

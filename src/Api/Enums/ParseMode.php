<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class ParseMode
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#formatting-options
 *
 * @method static static MarkdownV2()
 * @method static static HTML()
 * @method static static Markdown()
 */
final class ParseMode extends TelegramEnum
{
    public const MarkdownV2 = 'MarkdownV2';
    public const HTML = 'HTML';
    public const Markdown = 'Markdown';

    public function isHTML(): bool
    {
        return $this->is(self::HTML);
    }

    public function isNotHTML(): bool
    {
        return $this->isNot(self::HTML);
    }

    public function isMarkdownV2(): bool
    {
        return $this->is(self::MarkdownV2);
    }

    public function isNotMarkdownV2(): bool
    {
        return $this->isNot(self::MarkdownV2);
    }

    public function isMarkdown(): bool
    {
        return $this->is(self::Markdown);
    }

    public function isNotMarkdown(): bool
    {
        return $this->isNot(self::Markdown);
    }
}

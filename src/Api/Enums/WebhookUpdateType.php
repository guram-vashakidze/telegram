<?php

namespace Vashakidze\Telegram\Api\Enums;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\Types\Poll;
use Vashakidze\Telegram\Api\Types\PreCheckoutQuery;
use Vashakidze\Telegram\Events\CallbackQueryEvent;
use Vashakidze\Telegram\Events\ChannelPostEvent;
use Vashakidze\Telegram\Events\ChatJoinRequestEvent;
use Vashakidze\Telegram\Events\ChatMemberEvent;
use Vashakidze\Telegram\Events\ChosenInlineResultEvent;
use Vashakidze\Telegram\Events\EditedChannelPostEvent;
use Vashakidze\Telegram\Events\EditMessageEvent;
use Vashakidze\Telegram\Events\InlineQueryEvent;
use Vashakidze\Telegram\Events\MessageEvent;
use Vashakidze\Telegram\Events\MyChatMemberEvent;
use Vashakidze\Telegram\Events\PollAnswerEvent;
use Vashakidze\Telegram\Events\ShippingQueryEvent;
use Vashakidze\Telegram\TelegramEnum;

/**
 * Class TelegramWebhookAllowedUpdates
 * @package Vashakidze\Telegram\Enums
 *
 * @method static static message()
 * @method static static edited_message()
 * @method static static channel_post()
 * @method static static edited_channel_post()
 * @method static static inline_query()
 * @method static static chosen_inline_result()
 * @method static static callback_query()
 * @method static static shipping_query()
 * @method static static pre_checkout_query()
 * @method static static poll()
 * @method static static poll_answer()
 * @method static static my_chat_member()
 * @method static static chat_member()
 * @method static static chat_join_request()
 *
 * @method bool isMessage()
 * @method bool isEditedMessage()
 * @method bool isChannelPost()
 * @method bool isEditedChannelPost()
 * @method bool isInlineQuery()
 * @method bool isChosenInlineResult()
 * @method bool isCallbackQuery()
 * @method bool isShippingQuery()
 * @method bool isPreCheckoutQuery()
 * @method bool isPoll()
 * @method bool isPollAnswer()
 * @method bool isMyChatMember()
 * @method bool isChatMember()
 * @method bool isChatJoinRequest()
 *
 * @method bool isNotMessage()
 * @method bool isNotEditedMessage()
 * @method bool isNotChannelPost()
 * @method bool isNotEditedChannelPost()
 * @method bool isNotInlineQuery()
 * @method bool isNotChosenInlineResult()
 * @method bool isNotCallbackQuery()
 * @method bool isNotShippingQuery()
 * @method bool isNotPreCheckoutQuery()
 * @method bool isNotPoll()
 * @method bool isNotPollAnswer()
 * @method bool isNotMyChatMember()
 * @method bool isNotChatMember()
 * @method bool isNotChatJoinRequest()
 */
final class WebhookUpdateType extends TelegramEnum
{
    public const message = 'message';
    public const edited_message = 'edited_message';
    public const channel_post = 'channel_post';
    public const edited_channel_post = 'edited_channel_post';
    public const inline_query = 'inline_query';
    public const chosen_inline_result = 'chosen_inline_result';
    public const callback_query = 'callback_query';
    public const shipping_query = 'shipping_query';
    public const pre_checkout_query = 'pre_checkout_query';
    public const poll = 'poll';
    public const poll_answer = 'poll_answer';
    public const my_chat_member = 'my_chat_member';
    public const chat_member = 'chat_member';
    public const chat_join_request = 'chat_join_request';
    private static array $events = [
        self::message => MessageEvent::class,
        self::edited_message => EditMessageEvent::class,
        self::channel_post => ChannelPostEvent::class,
        self::edited_channel_post => EditedChannelPostEvent::class,
        self::inline_query => InlineQueryEvent::class,
        self::chosen_inline_result => ChosenInlineResultEvent::class,
        self::callback_query => CallbackQueryEvent::class,
        self::shipping_query => ShippingQueryEvent::class,
        self::pre_checkout_query => PreCheckoutQuery::class,
        self::poll => Poll::class,
        self::poll_answer => PollAnswerEvent::class,
        self::my_chat_member => MyChatMemberEvent::class,
        self::chat_member => ChatMemberEvent::class,
        self::chat_join_request => ChatJoinRequestEvent::class,
    ];

    public function toSetMethodName(): string
    {
        return 'set' . Str::studly($this->value);
    }

    public function toPropertyName(): string
    {
        return Str::camel($this->value);
    }

    public function getEvent(): string
    {
        return self::$events[$this->value];
    }
}

<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class InputMediaType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @method static static audio()
 * @method static static document()
 * @method static static photo()
 * @method static static video()
 * @method static static animation()
 *
 * @method bool isAudio()
 * @method bool isDocument()
 * @method bool isPhoto()
 * @method bool isVideo()
 * @method bool isAnimation()
 *
 * @method bool isNotAudio()
 * @method bool isNotDocument()
 * @method bool isNotPhoto()
 * @method bool isNotVideo()
 * @method bool isNotAnimation()
 */
final class InputMediaType extends TelegramEnum
{
    public const audio = 'audio';
    public const document = 'document';
    public const photo = 'photo';
    public const video = 'video';
    public const animation = 'animation';
}

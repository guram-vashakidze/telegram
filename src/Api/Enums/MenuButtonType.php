<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\Api\Types\MenuButton;
use Vashakidze\Telegram\Api\Types\MenuButtonCommands;
use Vashakidze\Telegram\Api\Types\MenuButtonDefault;
use Vashakidze\Telegram\Api\Types\MenuButtonWebApp;
use Vashakidze\Telegram\TelegramEnum;

/**
 * Class MenuButtonType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#menubutton
 *
 * @method static static commands()
 * @method static static web_app()
 * @method static static default()
 *
 * @method bool isCommands()
 * @method bool isWebApp()
 * @method bool isDefault()
 *
 * @method bool isNotCommands()
 * @method bool isNotWebApp()
 * @method bool isNotDefault()
 */
class MenuButtonType extends TelegramEnum
{
    public const commands = 'commands';
    public const web_app = 'web_app';
    public const default = 'default';

    public function init(array $data): MenuButton
    {
        return match ($this->value) {
            self::commands => MenuButtonCommands::init($data),
            self::web_app => MenuButtonWebApp::init($data),
            self::default => MenuButtonDefault::init($data)
        };
    }
}

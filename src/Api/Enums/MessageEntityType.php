<?php

namespace Vashakidze\Telegram\Api\Enums;

use Vashakidze\Telegram\TelegramEnum;

/**
 * Class MessageEntityType
 * @package Vashakidze\Telegram\Api\Enums
 *
 * @link https://core.telegram.org/bots/api#messageentity
 *
 * @method static static mention()
 * @method static static hashtag()
 * @method static static cashtag()
 * @method static static bot_command()
 * @method static static url()
 * @method static static email()
 * @method static static phone_number()
 * @method static static bold()
 * @method static static italic()
 * @method static static underline()
 * @method static static strikethrough()
 * @method static static spoiler()
 * @method static static code()
 * @method static static pre()
 * @method static static text_link()
 * @method static static text_mention()
 * @method static static custom_emoji()
 *
 * @method bool isMention()
 * @method bool isHashtag()
 * @method bool isCashtag()
 * @method bool isBotCommand()
 * @method bool isUrl()
 * @method bool isEmail()
 * @method bool isPhoneNumber()
 * @method bool isBold()
 * @method bool isItalic()
 * @method bool isUnderline()
 * @method bool isStrikethrough()
 * @method bool isSpoiler()
 * @method bool isCode()
 * @method bool isPre()
 * @method bool isTextLink()
 * @method bool isTextMention()
 * @method bool isCustomEmoji()
 *
 * @method bool isNotMention()
 * @method bool isNotHashtag()
 * @method bool isNotCashtag()
 * @method bool isNotBotCommand()
 * @method bool isNotUrl()
 * @method bool isNotEmail()
 * @method bool isNotPhoneNumber()
 * @method bool isNotBold()
 * @method bool isNotItalic()
 * @method bool isNotUnderline()
 * @method bool isNotStrikethrough()
 * @method bool isNotSpoiler()
 * @method bool isNotCode()
 * @method bool isNotPre()
 * @method bool isNotTextLink()
 * @method bool isNotTextMention()
 * @method bool isNotCustomEmoji()
 */
final class MessageEntityType extends TelegramEnum
{
    public const mention = 'mention';
    public const hashtag = 'hashtag';
    public const cashtag = 'cashtag';
    public const bot_command = 'bot_command';
    public const url = 'url';
    public const email = 'email';
    public const phone_number = 'phone_number';
    public const bold = 'bold';
    public const italic = 'italic';
    public const underline = 'underline';
    public const strikethrough = 'strikethrough';
    public const spoiler = 'spoiler';
    public const code = 'code';
    public const pre = 'pre';
    public const text_link = 'text_link';
    public const text_mention = 'text_mention';
    public const custom_emoji = 'custom_emoji';
}

<?php

namespace Vashakidze\Telegram\Api;

use Vashakidze\Telegram\Telegram;

abstract class InputType extends TelegramObject
{
    protected const NON_REQUEST_ARGS = [];

    public function send(): mixed
    {
        $requestName = lcfirst(
            preg_replace("/^.+\\\\/", "", static::class)
        );
        return Telegram::{$requestName}($this);
    }
}

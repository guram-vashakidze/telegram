<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class StopMessageLiveLocation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#stopmessagelivelocation
 *
 * @property-read int|string|null $chatId - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read int|null $messageId - Required if inline_message_id is not specified. Identifier of the message with live location to stop
 * @property-read string|null $inlineMessageId - Required if chat_id and message_id are not specified. Identifier of the inline message
 * @property-read InlineKeyboardMarkup $replyMarkup - A JSON-serialized object for a new inline keyboard
 *
 * @method self setChatId(int|string $chatId)
 * @method self setMessageId(int $messageId)
 * @method self setInlineMessageId(string $inlineMessageId)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 *
 * @method Message|bool send()
 */
class StopMessageLiveLocation extends InputType
{
    protected int|string|null $chatId;
    protected ?int $messageId;
    protected ?string $inlineMessageId;
    protected InlineKeyboardMarkup|null $replyMarkup;

    /**
     * @throws TelegramArgsException
     */
    protected function customCheckRequest(): void
    {
        if (!isset($this->inlineMessageId) || empty($this->inlineMessageId)) {
            if (!isset($this->messageId) || empty($this->messageId)) {
                throw new TelegramArgsException('The field "messageId" is required without "inlineMessageId"');
            }
            if (!isset($this->chatId) || empty($this->chatId)) {
                throw new TelegramArgsException('The field "chatId" is required without "inlineMessageId"');
            }
            return;
        }
        if ((isset($this->messageId) && !empty($this->messageId)) || (isset($this->chatId) && !empty($this->chatId))) {
            throw new TelegramArgsException('Only "inlineMessageId" or "messageId"+"chatId" can be in request');
        }
    }
}

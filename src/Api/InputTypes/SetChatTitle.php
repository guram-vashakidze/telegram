<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class SetChatTitle
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchattitle
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read string $title - New chat title, 1-255 characters
 *
 * @method bool send()
 */
class SetChatTitle extends InputType
{
    use HasChatId;

    protected string $title;

    /**
     * @param string $title
     * @return $this
     * @throws TelegramArgsException
     */
    public function setTitle(string $title): self
    {
        $length = Str::length($title);
        if ($length >= 1 && $length <= 255) {
            $this->title = $title;
            return $this;
        }
        throw new TelegramArgsException('The length of field "title" must be between 1 and 255 charsets');
    }
}

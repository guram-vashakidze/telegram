<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Vashakidze\Telegram\Api\InputTypes\InputFile;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

trait HasThumb
{
    protected InputFile|null $thumbFile;
    protected InputFile|string|null $thumb;

    /**
     * @param InputFile|string $thumb
     * @return $this
     * @throws TelegramArgsException
     */
    public function setThumb(InputFile|string $thumb): self
    {
        if (is_string($thumb)) {
            $this->thumb = $thumb;
            return $this;
        }
        if (!in_array($thumb->extension, ['jpeg', 'jpg'])) {
            throw new TelegramArgsException("You set incorrect \"thumb\" file. File type must be JPEG");
        }
        if ($thumb->size > 204800) {
            throw new TelegramArgsException("You set incorrect \"thumb\" file. File is to big. Max size: 200 kB");
        }
        if (!$this->isMultiUpload()) {
            $thumb->setName('thumb');
        } else {
            $this->thumb = 'attach://' . $thumb->name;
            $this->thumbFile = $thumb;
        }
        return $this;
    }

    protected function isMultiUpload(): bool
    {
        return false;
    }
}

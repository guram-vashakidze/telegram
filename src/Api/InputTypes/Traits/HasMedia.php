<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Vashakidze\Telegram\Api\InputTypes\InputFile;

/**
 * Trait HasMedia
 * @package Vashakidze\Telegram\Api\InputTypes\Traits
 *
 * @property-read string $media - File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name
 */
trait HasMedia
{
    protected InputFile $mediaFile;
    protected string $media;

    public function setMedia(InputFile|string $file): self
    {
        if (is_string($file)) {
            $this->media = $file;
            return $this;
        }
        $this->media = 'attach://' . $file->name;
        $this->mediaFile = $file;
        return $this;
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Vashakidze\Telegram\Api\Types\ForceReply;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\ReplyKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\ReplyKeyboardRemove;

/**
 * Trait HasDefaultFields
 * @package Vashakidze\Telegram\Api\InputTypes\Traits
 *
 * @property-read bool|null $disableNotification - Sends the message silently. Users will receive a notification with no sound.
 * @property-read bool|null $protectContent - Protects the contents of the sent message from forwarding and saving
 * @property-read int|null $replyToMessageId - If the message is a reply, ID of the original message
 * @property-read bool|null $allowSendingWithoutReply - Pass True if the message should be sent even if the specified replied-to message is not found
 * @property-read InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $replyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user
 *
 * @method self setDisableNotification()
 * @method self setProtectContent()
 * @method self setReplyToMessageId(int $replyToMessageId)
 * @method self setAllowSendingWithoutReply()
 * @method self setReplyMarkup(InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply $replyMarkup)
 */
trait HasDefaultFields
{
    protected ?bool $disableNotification;
    protected ?bool $protectContent;
    protected ?int $replyToMessageId;
    protected ?bool $allowSendingWithoutReply;
    protected InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $replyMarkup;
}

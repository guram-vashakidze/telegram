<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

/**
 * Trait HasChatId
 * @package Vashakidze\Telegram\Api\InputTypes\Traits
 *
 * @property-read int|string $chatId Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 *
 * @method self setChatId(int|string $chatId)
 */
trait HasChatId
{
    protected int|string $chatId;
}

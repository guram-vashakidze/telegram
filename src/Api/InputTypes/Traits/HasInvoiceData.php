<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Illuminate\Support\Str;
use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\Currency;
use Vashakidze\Telegram\Api\Types\LabeledPrice;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * @property-read string $title - Product name, 1-32 characters
 * @property-read string $description - Product description, 1-255 characters
 * @property-read string $payload - Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
 * @property-read string $providerToken - Payment provider token, obtained via @BotFather
 * @property-read Currency $currency - Three-letter ISO 4217 currency code, see more on currencies
 * @property-read LabeledPrice[] $prices - Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
 * @property-read int|null $maxTipAmount - The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
 * @property-read int[]|null $suggestedTipAmounts - A JSON-serialized array of suggested amounts of tip in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount
 * @property-read array|null $providerData - A JSON-serialized object for data about the invoice, which will be shared with the payment provider. A detailed description of the required fields should be provided by the payment provider.
 * @property-read string|null $photoUrl - URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
 * @property-read int|null $photoSize - Photo size in bytes
 * @property-read int|null $photoWidth - Photo width
 * @property-read int|null $photoHeight - Photo height
 * @property-read bool|null $needName - Pass True if you require the user's full name to complete the order
 * @property-read bool|null $needPhoneNumber - Pass True if you require the user's phone number to complete the order
 * @property-read bool|null $needEmail - Pass True if you require the user's email address to complete the order
 * @property-read bool|null $needShippingAddress - Pass True if you require the user's shipping address to complete the order
 * @property-read bool|null $sendPhoneNumberToProvider - Pass True if the user's phone number should be sent to provider
 * @property-read bool|null $sendEmailToProvider - Pass True if the user's email address should be sent to provider
 * @property-read bool|null $isFlexible - Pass True if the final price depends on the shipping method
 *
 * @method self setProviderToken(string $providerToken)
 * @method self setCurrency(Currency $currency)
 * @method self setPrices(LabeledPrice[] $prices)
 * @method self setProviderData(array $providerData)
 * @method self setPhotoUrl(string $photoUrl)
 * @method self setPhotoSize(int $photoSize)
 * @method self setPhotoWidth(int $photoWidth)
 * @method self setPhotoHeight(int $photoHeight)
 * @method self setNeedName()
 * @method self setNeedPhoneNumber()
 * @method self setNeedEmail()
 * @method self setNeedShippingAddress()
 * @method self setSendPhoneNumberToProvider()
 * @method self setSendEmailToProvider()
 * @method self setIsFlexible()
 */
trait HasInvoiceData
{
    protected string $title;
    protected string $description;
    protected string $payload;
    protected string $providerToken;
    protected Currency $currency;
    protected array|JsonSerializable $prices;
    protected ?int $maxTipAmount;
    protected array|JsonSerializable|null $suggestedTipAmounts;
    protected array|JsonSerializable|null $providerData;
    protected ?string $photoUrl;
    protected ?int $photoSize;
    protected ?int $photoWidth;
    protected ?int $photoHeight;
    protected ?bool $needName;
    protected ?bool $needPhoneNumber;
    protected ?bool $needEmail;
    protected ?bool $needShippingAddress;
    protected ?bool $sendPhoneNumberToProvider;
    protected ?bool $sendEmailToProvider;
    protected ?bool $isFlexible;

    /**
     * @param string $title
     * @return $this
     * @throws TelegramArgsException
     */
    public function setTitle(string $title): self
    {
        $length = Str::length($title);
        if ($length >= 1 && $length <= 32) {
            $this->title = $title;
            return $this;
        }
        throw new TelegramArgsException('The field "title" must be between 1 and 32 charsets');
    }

    /**
     * @param string $description
     * @return $this
     * @throws TelegramArgsException
     */
    public function setDescription(string $description): self
    {
        $length = Str::length($description);
        if ($length >= 1 && $length <= 255) {
            $this->description = $description;
            return $this;
        }
        throw new TelegramArgsException('The field "description" must be between 1 and 255 charsets');
    }

    /**
     * @param string $payload
     * @return $this
     * @throws TelegramArgsException
     */
    public function setPayload(string $payload): self
    {
        $length = mb_strlen($payload);
        if ($length >= 1 && $length <= 128) {
            $this->payload = $payload;
            return $this;
        }
        throw new TelegramArgsException('The field "payload" must be between 1 and 128 bytes');
    }

    /**
     * @param int $maxTipAmount
     * @return $this
     * @throws TelegramArgsException
     */
    public function setMaxTipAmount(int $maxTipAmount): self
    {
        if ($maxTipAmount < 0) {
            throw new TelegramArgsException('The field "max_tip_amount" bust be positive');
        }
        $this->maxTipAmount = $maxTipAmount;
        if (!isset($this->suggestedTipAmounts) || empty($this->suggestedTipAmounts)) {
            return $this;
        }
        $this->checkTips();
        return $this;
    }

    /**
     * @throws TelegramArgsException
     */
    private function checkTips(): void
    {
        foreach ($this->suggestedTipAmounts as $suggestedTipAmount) {
            if ($suggestedTipAmount > $this->maxTipAmount) {
                throw new TelegramArgsException(
                    'The field "suggested_tip_amount" must not exceed field "max_tip_amount"'
                );
            }
        }
    }

    /**
     * @param int[] $suggestedTipAmounts
     * @return $this
     * @throws TelegramArgsException
     */
    public function setSuggestedTipAmounts(array $suggestedTipAmounts): self
    {
        if (count($suggestedTipAmounts) > 4) {
            throw new TelegramArgsException('The field "suggested_tip_amounts" must contains at most 4 suggested tip');
        }
        foreach ($suggestedTipAmounts as $key => $suggestedTipAmount) {
            if (!is_int($suggestedTipAmount)) {
                throw new TelegramArgsException('The field "suggested_tip_amounts" must be type "int[]"');
            }
            if ($key === 0) {
                continue;
            }
            if ($suggestedTipAmount <= $suggestedTipAmounts[$key - 1]) {
                throw new TelegramArgsException(
                    'The field "suggested_tip_amounts" must passed in a strictly increased order'
                );
            }
        }
        $this->suggestedTipAmounts = $suggestedTipAmounts;
        if (!isset($this->maxTipAmount)) {
            return $this;
        }
        $this->checkTips();
        return $this;
    }
}

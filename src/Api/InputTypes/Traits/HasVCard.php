<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Vashakidze\Telegram\Exceptions\TelegramArgsException;

trait HasVCard
{
    /**
     * @param string $vcard
     * @return $this
     * @throws TelegramArgsException
     *
     * @link https://en.wikipedia.org/wiki/VCard
     * @link https://docs.fileformat.com/email/vcf/
     */
    public function setVcard(string $vcard): self
    {
        if (preg_match("/^BEGIN:VCARD\nVERSION:(2\.1|3\.0|4\.0)\n.+\nEND:VCARD$/s", $vcard)) {
            $this->vcard = $vcard;
            return $this;
        }
        throw new TelegramArgsException(
            'Field "vcard" has incorrect structure. It should begins with "BEGIN:VCARD", next line "VERSION:2.1/3.0/4.0" and end with END:VCARD'
        );
    }
}

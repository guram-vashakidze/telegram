<?php

namespace Vashakidze\Telegram\Api\InputTypes\Traits;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputTypes\InlineQueryResult;
use Vashakidze\Telegram\Api\InputTypes\InputMessageContent;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\MessageEntity;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Trait HasInlineQueryResultDefaultFields
 * @package Vashakidze\Telegram\Api\InputTypes\Traits
 *
 * @property-read string|null $title - Recording title
 * @property-read string|null $caption - Caption, 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode - Mode for parsing entities in the voice message caption. See formatting options for more details.
 * @property-read MessageEntity[]|null $captionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
 * @property-read InputMessageContent|null $inputMessageContent
 * @property-read InlineKeyboardMarkup|null $replyMarkup
 *
 * @method self setTitle(string $title)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 */
trait HasInlineQueryResultDefaultFields
{
    protected ?string $title;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected ?array $captionEntities;
    protected ?InputMessageContent $inputMessageContent;
    protected ?InlineKeyboardMarkup $replyMarkup;

    /**
     * @param string $caption
     * @return InlineQueryResult
     * @throws TelegramArgsException
     */
    public function setCaption(string $caption): self
    {
        $length = Str::length($caption);
        if ($length >= 0 && $length <= 1024) {
            $this->caption = $caption;
            return $this;
        }
        throw new TelegramArgsException('The field "caption" must be between 0 and 1024 charsets');
    }
}

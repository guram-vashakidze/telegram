<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedGif
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to an animated GIF file stored on the Telegram servers. By default, this animated GIF file will be
 * sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with
 * specified content instead of the animation.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedgif
 *
 * @property-read string $gifFileId - A valid file identifier for the GIF file
 *
 * @method self setGifFileId(string $gifFileId)
 */
class InlineQueryResultCachedGif extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $gifFileId;

    public function __construct()
    {
        $this->type = InlineQueryResultType::gif();
    }
}

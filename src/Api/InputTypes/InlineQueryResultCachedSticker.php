<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;

/**
 * Class InlineQueryResultCachedSticker
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a sticker stored on the Telegram servers. By default, this sticker will be sent by the user.
 * Alternatively, you can use input_message_content to send a message with the specified content instead of the sticker.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedsticker
 *
 * @property-read string $stickerFileId - A valid file identifier of the sticker
 * @property-read InlineKeyboardMarkup|null $replyMarkup - Inline keyboard attached to the message
 * @property-read InputMessageContent|null $inputMessageContent - Content of the message to be sent instead of the location
 *
 * @method self setStickerFileId(string $stickerFileId)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 */
class InlineQueryResultCachedSticker extends InlineQueryResult
{
    protected string $stickerFileId;
    protected ?InlineKeyboardMarkup $replyMarkup;
    protected ?InputMessageContent $inputMessageContent;

    public function __construct()
    {
        $this->type = InlineQueryResultType::sticker();
    }
}

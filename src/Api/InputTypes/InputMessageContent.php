<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;

/**
 * Class InputMessageContent
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#inputmessagecontent
 */
abstract class InputMessageContent extends InputType
{
}

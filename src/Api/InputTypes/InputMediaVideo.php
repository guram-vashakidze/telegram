<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InputMediaType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;

/**
 * Class InputMediaVideo
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a video to be sent.
 *
 * @link https://core.telegram.org/bots/api#inputmediavideo
 *
 * @property-read int|null $width - Video width
 * @property-read int|null $height - Video height
 * @property-read int|null $duration - Video duration in seconds
 * @property-read bool|null $supportsStreaming - Pass True if the uploaded video is suitable for streaming
 *
 * @method self setWidth(int $width)
 * @method self setHeight(int $height)
 * @method self setDuration(int $duration)
 * @method self setSupportsStreaming()
 */
class InputMediaVideo extends InputMedia
{
    use HasThumb;

    protected ?int $width;
    protected ?int $height;
    protected ?int $duration;
    protected ?bool $supportsStreaming;

    public function __construct()
    {
        $this->type = InputMediaType::video();
    }

    protected function isMultiUpload(): bool
    {
        return true;
    }
}

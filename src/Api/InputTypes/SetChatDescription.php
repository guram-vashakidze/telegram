<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class SetChatDescription
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchatdescription
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read string $description - New chat title, 1-255 characters
 *
 * @method bool send()
 */
class SetChatDescription extends InputType
{
    use HasChatId;

    protected string $description;

    /**
     * @param string $description
     * @return $this
     * @throws TelegramArgsException
     */
    public function setDescription(string $description): self
    {
        $length = Str::length($description);
        if ($length >= 0 && $length <= 255) {
            $this->description = $description;
            return $this;
        }
        throw new TelegramArgsException('The length of field "description" must be between 0 and 255 charsets');
    }
}

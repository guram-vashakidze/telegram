<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\UserProfilePhotos;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class GetUserProfilePhotos
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#getuserprofilephotos
 *
 * @property-read int $userId - Unique identifier of the target user.
 * @property-read int|null $offset - Sequential number of the first photo to be returned. By default, all photos are returned.
 * @property-read int|null $limit - Limits the number of photos to be retrieved. Values between 1-100 are accepted. Defaults to 100.
 *
 * @method setUserId(int $userId)
 * @method setOffset(int $offset)
 *
 * @method UserProfilePhotos send()
 */
class GetUserProfilePhotos extends InputType
{
    protected int $userId;
    protected ?int $offset;
    protected ?int $limit;

    /**
     * @param int $limit
     * @return $this
     * @throws TelegramArgsException
     */
    public function setLimit(int $limit): self
    {
        if ($limit >= 1 && $limit <= 100) {
            $this->limit = $limit;
            return $this;
        }
        throw new TelegramArgsException('Field "limit" must be between 1 and 100');
    }
}

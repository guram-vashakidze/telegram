<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\LanguageCode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\BotCommand;
use Vashakidze\Telegram\Api\Types\BotCommandScope;

/**
 * Class GetMyCommands
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#getmycommands
 *
 * @property-read BotCommandScope|null $scope - A JSON-serialized object, describing scope of users. Defaults to BotCommandScopeDefault.
 * @property-read LanguageCode|null $languageCode - A two-letter ISO 639-1 language code or an empty string
 *
 * @method self setScope(BotCommandScope $scope)
 * @method self setLanguageCode(LanguageCode $code)
 *
 * @method BotCommand[] send()
 */
class GetMyCommands extends InputType
{
    protected ?BotCommandScope $scope;
    protected ?LanguageCode $languageCode;
}

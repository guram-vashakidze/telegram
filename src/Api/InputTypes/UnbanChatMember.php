<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class UnbanChatMember
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#unbanchatmember
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 * @property-read bool|null $onlyIfBanned - Do nothing if the user is not banned
 *
 * @method self setUserId(int $userId)
 * @method self setOnlyIfBanned()
 *
 * @method bool send()
 */
class UnbanChatMember extends InputType
{
    use HasChatId;

    protected int $userId;
    protected ?bool $onlyIfBanned;
}

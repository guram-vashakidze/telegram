<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageEntity;

/**
 * Class SendAudio
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendaudio
 *
 * @property-read InputFile|string $audio Audio file to send. Pass a file_id as String to send an audio file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an audio file from the Internet, or upload a new one using multipart/form-data
 * @property-read string|null $caption Photo caption (may also be used when resending audios by file_id), 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $captionEntities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
 * @property-read int|null $duration Duration of the audio in seconds
 * @property-read string|null $performer Performer
 * @property-read string|null $title Track name
 * @property-read InputFile|string|null $thumb Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>
 *
 * @method self setCaption(string $caption)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 * @method self setDuration(int $duration)
 * @method self setPerformer(string $performer)
 * @method self setTitle(string $title)
 *
 * @method Message send()
 */
class SendAudio extends InputType
{
    use HasThumb;
    use HasDefaultFields;
    use HasChatId;

    protected InputFile|string $audio;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $captionEntities;
    protected ?int $duration;
    protected ?string $performer;
    protected ?string $title;
    protected InputFile|string|null $thumb;

    public function setAudio(InputFile|string $audio): self
    {
        if ($audio instanceof InputFile) {
            $this->audio = $audio->setName('audio');
            return $this;
        }
        $this->audio = $audio;
        return $this;
    }
}

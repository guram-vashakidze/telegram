<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputTypes\Traits\HasInvoiceData;

/**
 * Class InputInvoiceMessageContent
 * @package Vashakidze\Telegram\Api\InputTypes
 */
class InputInvoiceMessageContent extends InputMessageContent
{
    use HasInvoiceData;
}

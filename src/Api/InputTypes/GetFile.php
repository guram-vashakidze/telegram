<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\File;

/**
 * Class GetFile
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#getfile
 *
 * @property-read string $fileId - File identifier to get information about
 *
 * @method self setFileId(string $fileId)
 *
 * @method File send()
 */
class GetFile extends InputType
{
    protected string $fileId;
}

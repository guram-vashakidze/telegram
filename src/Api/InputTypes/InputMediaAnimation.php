<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InputMediaType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;

/**
 * Class InputMediaAnimation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents an animation file (GIF or H.264/MPEG-4 AVC video without sound) to be sent.
 *
 * @link https://core.telegram.org/bots/api#inputmediaanimation
 *
 * @property-read int|null $width - Animation width
 * @property-read int|null $height - Animation height
 * @property-read int|null $duration - Animation duration in seconds
 *
 * @method self setWidth(int $width)
 * @method self setHeight(int $height)
 * @method self setDuration(int $duration)
 */
class InputMediaAnimation extends InputMedia
{
    use HasThumb;

    protected ?int $width;
    protected ?int $height;
    protected ?int $duration;

    public function __construct()
    {
        $this->type = InputMediaType::animation();
    }

    protected function isMultiUpload(): bool
    {
        return true;
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\LanguageCode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\BotCommandScope;

/**
 * Class DeleteMyCommands
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#deletemycommands
 *
 * @property-read BotCommandScope|null $scope - A JSON-serialized object, describing scope of users for which the commands are relevant
 * @property-read LanguageCode|null $languageCode - A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
 *
 * @method self setScope(BotCommandScope $scope)
 * @method self setLanguageCode(LanguageCode $code)
 *
 * @method bool send()
 */
class DeleteMyCommands extends InputType
{
    protected ?BotCommandScope $scope;
    protected ?LanguageCode $languageCode;
}

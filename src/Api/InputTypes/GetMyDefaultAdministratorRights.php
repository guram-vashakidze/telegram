<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\ChatAdministratorRights;

/**
 * Class GetMyDefaultAdministratorRights
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#getmydefaultadministratorrights
 *
 * @property-read bool|null $forChannels
 *
 * @method self setForChannels()
 *
 * @method ChatAdministratorRights send()
 */
class GetMyDefaultAdministratorRights extends InputType
{
    protected ?bool $forChannels;
}

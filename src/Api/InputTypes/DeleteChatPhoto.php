<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class DeleteChatPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#deletechatphoto
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 *
 * @method bool send()
 */
class DeleteChatPhoto extends InputType
{
    use HasChatId;
}

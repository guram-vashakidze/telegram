<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class PromoteChatMember
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#promotechatmember
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 * @property-read bool|null $isAnonymous - Pass True if the administrator's presence in the chat is hidden
 * @property-read bool|null $canManageChat - Pass True if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
 * @property-read bool|null $canPostMessages - Pass True if the administrator can create channel posts, channels only
 * @property-read bool|null $canEdinMessages - Pass True if the administrator can edit messages of other users and can pin messages, channels only
 * @property-read bool|null $canDeleteMessages - Pass True if the administrator can delete messages of other users
 * @property-read bool|null $canManageVideoChat - Pass True if the administrator can manage video chats
 * @property-read bool|null $canRestrictMembers - Pass True if the administrator can restrict, ban or unban chat members
 * @property-read bool|null $canPromoteMembers - Pass True if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by him)
 * @property-read bool|null $canChangeInfo - Pass True if the administrator can change chat title, photo and other settings
 * @property-read bool|null $canInviteUsers - Pass True if the administrator can invite new users to the chat
 * @property-read bool|null $canPinMessages - Pass True if the administrator can pin messages, supergroups only
 *
 * @method self setUserId(int $userId)
 * @method self setIsAnonymous()
 * @method self setCanManageChat()
 * @method self setCanPostMessages()
 * @method self setCanEdinMessages()
 * @method self setCanDeleteMessages()
 * @method self setCanManageVideoChat()
 * @method self setCanRestrictMembers()
 * @method self setCanPromoteMembers()
 * @method self setCanChangeInfo()
 * @method self setCanInviteUsers()
 * @method self setCanPinMessages()
 *
 * @method bool send()
 */
class PromoteChatMember extends InputType
{
    use HasChatId;

    protected int $userId;
    protected ?bool $isAnonymous;
    protected ?bool $canManageChat;
    protected ?bool $canPostMessages;
    protected ?bool $canEdinMessages;
    protected ?bool $canDeleteMessages;
    protected ?bool $canManageVideoChat;
    protected ?bool $canRestrictMembers;
    protected ?bool $canPromoteMembers;
    protected ?bool $canChangeInfo;
    protected ?bool $canInviteUsers;
    protected ?bool $canPinMessages;
}

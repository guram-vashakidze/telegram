<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\ShippingOption;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class AnswerShippingQuery
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read string $shippingQueryId - Unique identifier for the query to be answered
 * @property-read bool $ok - Pass True if delivery to the specified address is possible and False if there are any problems (for example, if delivery to the specified address is not possible)
 * @property-read ShippingOption[]|null $shippingOptions - Required if ok is True. A JSON-serialized array of available shipping options.
 * @property-read string|null $errorMessage - Required if ok is False. Error message in human readable form that explains why it is impossible to complete the order (e.g. "Sorry, delivery to your desired address is unavailable'). Telegram will display this message to the user.
 *
 * @method self setShippingQueryId(string $shippingQueryId)
 * @method self setOk(bool $ok = true)
 * @method self setShippingOptions(ShippingOption[] $shippingOptions)
 * @method self setErrorMessage(string $errorMessage)
 *
 * @method bool send()
 */
class AnswerShippingQuery extends InputType
{
    protected string $shippingQueryId;
    protected bool $ok;
    protected array|JsonSerializable|null $shippingOptions;
    protected ?string $errorMessage;

    /**
     * @return void
     * @throws TelegramArgsException
     */
    protected function customCheckRequest(): void
    {
        if (!isset($this->ok)) {
            return;
        }
        if ($this->ok === true && !isset($this->shippingOptions)) {
            throw new TelegramArgsException('The field "shipping_options" required when "ok" is True');
        }
        if ($this->ok === false && !isset($this->errorMessage)) {
            throw new TelegramArgsException('The field "error_message" required when "ok" is False');
        }
    }
}

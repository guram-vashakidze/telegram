<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageEntity;

/**
 * Class SendPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendphoto
 *
 * @property-read InputFile|string $photo Photo to send. Pass a file_id as String to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. The photo must be at most 10 MB in size. The photo's width and height must not exceed 10000 in total. Width and height ratio must be at most 20
 * @property-read string|null $caption Photo caption (may also be used when resending photos by file_id), 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $captionEntities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
 *
 * @method self setCaption(string $caption)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 *
 * @method Message send()
 */
class SendPhoto extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected InputFile|string $photo;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $captionEntities;

    public function setPhoto(InputFile|string $photo): self
    {
        if ($photo instanceof InputFile) {
            $this->photo = $photo->setName('photo');
            return $this;
        }
        $this->photo = $photo;
        return $this;
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class PinChatMessage
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#pinchatmessage
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read int $messageId - Identifier of a message to pin
 * @property-read bool|null $disableNotification - Pass True if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels and private chats.
 *
 * @method self setMessageId(int $messageId)
 * @method self setDisableNotification()
 *
 * @method bool send()
 */
class PinChatMessage extends InputType
{
    use HasChatId;

    protected int $messageId;
    protected ?bool $disableNotification;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\InputMediaType;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasMedia;
use Vashakidze\Telegram\Api\Types\MessageEntity;

/**
 * Class InputMedia
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#inputmedia
 *
 * @property-read InputMediaType $type - Type of the result
 * @property-read string|null $caption - Caption of the document to be sent, 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode - Mode for parsing entities in the document caption. See formatting options for more details
 * @property-read MessageEntity[]|null $captionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
 *
 * @method self setCaption(string $caption)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 */
abstract class InputMedia extends InputType
{
    use HasMedia;

    protected InputMediaType $type;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $captionEntities;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedVideo
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a video file stored on the Telegram servers. By default, this video file will be sent by the user
 * with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content
 * instead of the video.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedvideo
 *
 * @property-read string $title - Recording title
 * @property-read string $videoFileId - A valid file identifier for the video file
 * @property-read string|null $description - Short description of the result
 *
 * @method self setVideoFileId(string $videoFileId)
 * @method self setDescription(string $description)
 */
class InlineQueryResultCachedVideo extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $videoFileId;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::video();
    }
}

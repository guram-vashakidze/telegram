<?php

namespace Vashakidze\Telegram\Api\InputTypes;

/**
 * Class UnbanChatSenderChat
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#unbanchatsenderchat
 *
 * @method bool send()
 */
class UnbanChatSenderChat extends BanChatSenderChat
{
}

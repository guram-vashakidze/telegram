<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class SendLocation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendmediagroup
 *
 * @property-read float $latitude - Latitude of the location
 * @property-read float $longitude - Longitude of the location
 * @property-read float|null $horizontalAccuracy - The radius of uncertainty for the location, measured in meters; 0-1500
 * @property-read int|null $livePeriod - Period in seconds for which the location will be updated (see Live Locations, should be between 60 and 86400
 * @property-read int|null $heading - For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
 * @property-read int|null $proximityAlertRadius - For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
 *
 * @method self setLatitude(float $latitude)
 * @method self setLongitude(float $longitude)
 *
 * @method Message send()
 */
class SendLocation extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected float $latitude;
    protected float $longitude;
    protected ?float $horizontalAccuracy;
    protected ?int $livePeriod;
    protected ?int $heading;
    protected ?int $proximityAlertRadius;

    /**
     * @param float $horizontalAccuracy
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHorizontalAccuracy(float $horizontalAccuracy): self
    {
        if ($horizontalAccuracy >= 0 && $horizontalAccuracy <= 1500) {
            $this->horizontalAccuracy = $horizontalAccuracy;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "horizontalAccuracy" field. This field must be between 0 and 1500');
    }

    /**
     * @param float $livePeriod
     * @return $this
     * @throws TelegramArgsException
     */
    public function setLivePeriod(float $livePeriod): self
    {
        if ($livePeriod >= 60 && $livePeriod <= 86400) {
            $this->livePeriod = $livePeriod;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "livePeriod" field. This field must be between 60 and 86400');
    }

    /**
     * @param int $heading
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHeading(int $heading): self
    {
        if ($heading >= 1 && $heading <= 360) {
            $this->heading = $heading;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "heading" field. This field must be between 1 and 360');
    }

    /**
     * @param int $proximityAlertRadius
     * @return $this
     * @throws TelegramArgsException
     */
    public function setProximityAlertRadius(int $proximityAlertRadius): self
    {
        if ($proximityAlertRadius >= 1 && $proximityAlertRadius <= 100000) {
            $this->proximityAlertRadius = $proximityAlertRadius;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "proximityAlertRadius" field. This field must be between 1 and 360');
    }
}

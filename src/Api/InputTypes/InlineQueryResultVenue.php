<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\GooglePlaceType;
use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;

/**
 * Class InlineQueryResultVenue
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a venue. By default, the venue will be sent by the user. Alternatively, you can use input_message_content
 * to send a message with the specified content instead of the venue.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultvenue
 *
 * @property-read float $latitude - Location latitude in degrees
 * @property-read float $longitude - Location longitude in degrees
 * @property-read string $title - Title of the venue
 * @property-read string $address - Address of the venue
 * @property-read string|null $foursquareId - Foursquare identifier of the venue
 * @property-read string|null $foursquareType - Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
 * @property-read string|null $googlePlaceId - Google Places identifier of the venue
 * @property-read string|null $googlePlaceType - Google Places type of the venue.
 * @property-read InlineKeyboardMarkup|null $replyMarkup - Inline keyboard attached to the message
 * @property-read InputMessageContent|null $inputMessageContent - Content of the message to be sent instead of the location
 * @property-read string|null $thumbUrl - Url of the thumbnail for the result
 * @property-read int|null $thumbWidth - Thumbnail width
 * @property-read int|null $thumbHeight - Thumbnail height
 *
 * @method self setLatitude(float $latitude)
 * @method self setLongitude(float $longitude)
 * @method self setTitle(string $title)
 * @method self setAddress(string $address)
 * @method self setFoursquareId(string $foursquareId)
 * @method self setFoursquareType(string $foursquareType)
 * @method self setGooglePlaceId(string $googlePlaceId)
 * @method self setGooglePlaceType(GooglePlaceType $googlePlaceType)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setThumbWidth(int $thumbWidth)
 * @method self setThumbHeight(int $thumbHeight)
 */
class InlineQueryResultVenue extends InlineQueryResult
{
    protected float $latitude;
    protected float $longitude;
    protected string $title;
    protected string $address;
    protected ?string $foursquareId;
    protected ?string $foursquareType;
    protected ?string $googlePlaceId;
    protected ?GooglePlaceType $googlePlaceType;
    protected ?InlineKeyboardMarkup $replyMarkup;
    protected ?InputMessageContent $inputMessageContent;
    protected ?string $thumbUrl;
    protected ?int $thumbWidth;
    protected ?int $thumbHeight;

    public function __construct()
    {
        $this->type = InlineQueryResultType::venue();
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\Message;

/**
 * Class SendMediaGroup
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can be
 * only grouped in an album with messages of the same type. On success, an array of Messages that were sent is returned.
 *
 * @link https://core.telegram.org/bots/api#sendmediagroup
 *
 * @property-read InputMedia[] $media
 * @property-read bool|null $disableNotification
 * @property-read bool|null $protectContent
 * @property-read int|null $replyToMessageId
 * @property-read bool|null $allowSendingWithoutReply
 *
 * @method self setMedia(InputMedia[] $media)
 * @method self setDisableNotification()
 * @method self setProtectContent()
 * @method self setReplyToMessageId(int $replyToMessageId)
 * @method self setAllowSendingWithoutReply()
 *
 * @method Message[] send()
 */
class SendMediaGroup extends InputType
{
    use HasChatId;

    protected array|JsonSerializable $media;
    protected ?bool $disableNotification;
    protected ?bool $protectContent;
    protected ?int $replyToMessageId;
    protected ?bool $allowSendingWithoutReply;
}

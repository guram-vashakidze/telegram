<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Enums\MimeTypeEnum;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultGif
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to an animated GIF file. By default, this animated GIF file will be sent by the user with optional
 * caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the
 * animation.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultgif
 *
 * @property-read string $gifUrl - A valid URL for the GIF file. File size must not exceed 1MB
 * @property-read int|null $gifWidth - Width of the GIF
 * @property-read int|null $gifHeight - Height of the GIF
 * @property-read int|null $gifDuration - Duration of the GIF in seconds
 * @property-read string $thumbUrl - URL of the static (JPEG or GIF) or animated (MPEG4) thumbnail for the result
 * @property-read MimeTypeEnum|null $thumbMimeType - MIME type of the thumbnail, must be one of “image/jpeg”, “image/gif”, or “video/mp4”. Defaults to “image/jpeg”
 *
 * @method self setGifUrl(string $gifUrl)
 * @method self setGifWidth(int $gifWidth)
 * @method self setGifHeight(int $gifHeight)
 * @method self setGifDuration(int $gifDuration)
 * @method self setThumbUrl(int $thumbUrl)
 */
class InlineQueryResultGif extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $gifUrl;
    protected ?int $gifWidth;
    protected ?int $gifHeight;
    protected ?int $gifDuration;
    protected string $thumbUrl;
    protected ?MimeTypeEnum $thumbMimeType;

    public function __construct()
    {
        $this->type = InlineQueryResultType::gif();
    }

    /**
     * @param MimeTypeEnum $thumbMimeType
     * @return $this
     * @throws TelegramArgsException
     */
    public function setThumbMimeType(MimeTypeEnum $thumbMimeType): self
    {
        if ($thumbMimeType->isGifMimes()) {
            $this->thumbMimeType = $thumbMimeType;
            return $this;
        }
        throw new TelegramArgsException(
            'Incorrect field "thumb_mime_type" value. Available values: ' . implode(', ', MimeTypeEnum::gifMimes())
        );
    }
}

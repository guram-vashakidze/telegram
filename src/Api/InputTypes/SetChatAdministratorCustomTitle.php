<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class SetChatAdministratorCustomTitle
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchatadministratorcustomtitle
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 * @property-read string $customTitle - New custom title for the administrator; 0-16 characters, emoji are not allowed
 *
 * @method self setUserId(int $userId)
 * @method self setCustomTitle(string $customTitle)
 *
 * @method bool send()
 */
class SetChatAdministratorCustomTitle extends InputType
{
    use HasChatId;

    protected int $userId;
    protected string $customTitle;
}

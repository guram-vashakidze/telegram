<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedMpeg4Gif
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a video animation (H.264/MPEG-4 AVC video without sound) stored on the Telegram servers. By
 * default, this animated MPEG-4 file will be sent by the user with an optional caption. Alternatively, you can use
 * input_message_content to send a message with the specified content instead of the animation.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedmpeg4gif
 *
 * @property-read string $mpeg4FileId - A valid file identifier for the MPEG4 file
 *
 * @method self setMpeg4FileId(string $mpeg4FileId)
 */
class InlineQueryResultCachedMpeg4Gif extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $mpeg4FileId;

    public function __construct()
    {
        $this->type = InlineQueryResultType::mpeg4_gif();
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInvoiceData;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\Message;

/**
 * Class SendInvoice
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read string|null $startParameter - Unique deep-linking parameter. If left empty, forwarded copies of the sent message will have a Pay button, allowing multiple users to pay directly from the forwarded message, using the same invoice. If non-empty, forwarded copies of the sent message will have a URL button with a deep link to the bot (instead of a Pay button), with the value used as the start parameter
 * @property-read InlineKeyboardMarkup|null $replyMarkup - A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button
 *
 * @method self setStartParameter(string $startParameter)
 *
 * @method Message send()
 */
class SendInvoice extends InputType
{
    use HasChatId;
    use HasDefaultFields;
    use HasInvoiceData;

    protected ?string $startParameter;
    protected ?InlineKeyboardMarkup $replyMarkup;
}

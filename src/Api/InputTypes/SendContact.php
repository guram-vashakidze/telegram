<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasVCard;
use Vashakidze\Telegram\Api\Types\Message;

/**
 * Class SendContact
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendcontact
 *
 * @property-read string $phoneNumber - Contact's phone number
 * @property-read string $firstName - Contact's first name
 * @property-read string|null $lastName - Contact's last name
 * @property-read string|null $vcard - Additional data about the contact in the form of a vCard, 0-2048 bytes
 *
 * @method self setPhoneNumber(string $phoneNumber)
 * @method self setFirstName(string $firstName)
 * @method self setLastName(string $lastName)
 *
 * @method Message send()
 */
class SendContact extends InputType
{
    use HasDefaultFields;
    use HasChatId;
    use HasVCard;

    protected string $phoneNumber;
    protected string $firstName;
    protected ?string $lastName;
    protected ?string $vcard;
}

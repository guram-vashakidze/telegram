<?php

namespace Vashakidze\Telegram\Api\InputTypes;

/**
 * Class DeclineChatJoinRequest
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#declinechatjoinrequest
 *
 * @method bool send()
 */
class DeclineChatJoinRequest extends ApproveChatJoinRequest
{
}

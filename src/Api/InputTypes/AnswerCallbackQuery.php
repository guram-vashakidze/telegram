<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class AnswerCallbackQuery
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#answercallbackquery
 *
 * @property-read string $callbackQueryId - Unique identifier for the query to be answered
 * @property-read string|null $text - Text of the notification. If not specified, nothing will be shown to the user, 0-200 characters
 * @property-read bool|null $showAlert - If True, an alert will be shown by the client instead of a notification at the top of the chat screen. Defaults to false.
 * @property-read string|null $url - URL that will be opened by the user's client. If you have created a Game and accepted the conditions via @BotFather, specify the URL that opens your game - note that this will only work if the query comes from a callback_game button. Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
 * @property-read int|null $cacheTime - The maximum amount of time in seconds that the result of the callback query may be cached client-side. Telegram apps will support caching starting in version 3.14. Defaults to 0.
 *
 * @method self setCallbackQueryId(string $callbackQueryId)
 * @method self setShowAlert()
 * @method self setUrl(string $url)
 * @method self setCacheTime(int $cacheTime)
 *
 * @method bool send()
 */
class AnswerCallbackQuery extends InputType
{
    protected string $callbackQueryId;
    protected ?string $text;
    protected ?bool $showAlert;
    protected ?string $url;
    protected ?int $cacheTime;

    /**
     * @param string $text
     * @return $this
     * @throws TelegramArgsException
     */
    public function setText(string $text): self
    {
        $length = Str::length($text);
        if ($length >= 0 && $length <= 200) {
            $this->text = $text;
            return $this;
        }
        throw new TelegramArgsException('The length of fielf "text" must be between 0 and 200 charsets');
    }
}

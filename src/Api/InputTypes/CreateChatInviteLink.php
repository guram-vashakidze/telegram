<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\ChatInviteLink;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class CreateChatInviteLink
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#createchatinvitelink
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read string|null $name - Invite link name; 0-32 characters
 * @property-read Carbon|null $expireDate - Point in time (Unix timestamp) when the link will expire
 * @property-read int|null $memberLimit - The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
 * @property-read bool|null $createsJoinRequest - True, if users joining the chat via the link need to be approved by chat administrators. If True, member_limit can't be specified
 *
 * @method ChatInviteLink send()
 */
class CreateChatInviteLink extends InputType
{
    use HasChatId;

    protected ?string $name;
    protected ?Carbon $expireDate;
    protected ?int $memberLimit;
    protected ?bool $createsJoinRequest;

    /**
     * @param string $name
     * @return $this
     * @throws TelegramArgsException
     */
    public function setName(string $name): self
    {
        if (Str::length($name) <= 32) {
            $this->name = $name;
            return $this;
        }
        throw new TelegramArgsException('Length of field "name" must be between 0 and 32 charsets');
    }

    /**
     * @param Carbon $expireDate
     * @return $this
     * @throws TelegramArgsException
     */
    public function setExpireDate(Carbon $expireDate): self
    {
        if ($expireDate->isFuture()) {
            $this->expireDate = $expireDate;
            return $this;
        }
        throw new TelegramArgsException('Date in "expire_date" must be in future');
    }

    /**
     * @param int $memberLimit
     * @return $this
     * @throws TelegramArgsException
     */
    public function setMemberLimit(int $memberLimit): self
    {
        if (isset($this->createsJoinRequest) && !empty($this->createsJoinRequest)) {
            throw new TelegramArgsException('Field "member_limit" can not be used with "creates_join_request"');
        }
        if ($memberLimit >= 1 && $memberLimit <= 99999) {
            $this->memberLimit = $memberLimit;
            return $this;
        }
        throw new TelegramArgsException('Field "member_limit" must be between 1 and 99999');
    }

    /**
     * @return $this
     * @throws TelegramArgsException
     */
    public function setCreatesJoinRequest(): self
    {
        if (isset($this->memberLimit) && !empty($this->memberLimit)) {
            throw new TelegramArgsException('Field "creates_join_request" can not be used with "member_limit"');
        }
        $this->createsJoinRequest = true;
        return $this;
    }
}

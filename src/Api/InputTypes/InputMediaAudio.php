<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InputMediaType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;

/**
 * Class InputMediaAudio
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents an audio file to be treated as music to be sent
 *
 * @link https://core.telegram.org/bots/api#inputmediaaudio
 *
 * @property-read int|null $duration - Duration of the audio in seconds
 * @property-read string|null $performer - Performer of the audio
 * @property-read string|null $title - Title of the audio
 *
 * @method self setDuration(int $duration)
 * @method self setPerformer(string $performer)
 * @method self setTitle(string $title)
 */
class InputMediaAudio extends InputMedia
{
    use HasThumb;

    protected ?int $duration;
    protected ?string $performer;
    protected ?string $title;

    public function __construct()
    {
        $this->type = InputMediaType::audio();
    }

    protected function isMultiUpload(): bool
    {
        return true;
    }
}

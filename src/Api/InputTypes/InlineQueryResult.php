<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\Traits\HasJsonSerialize;

/**
 * Class InlineQueryResult
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read InlineQueryResultType $type
 * @property-read string $id
 *
 * @method self setId(string $id)
 */
abstract class InlineQueryResult extends InputType implements JsonSerializable
{
    use HasJsonSerialize;

    protected InlineQueryResultType $type;
    protected string $id;

    protected function customCheckRequest(): void
    {
        if (!isset($this->id) || empty($this->id)) {
            $this->id = Str::uuid();
        }
    }
}

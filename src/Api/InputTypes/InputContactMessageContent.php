<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InputContactMessageContent
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents the content of a contact message to be sent as the result of an inline query.
 *
 * @link https://core.telegram.org/bots/api#inputcontactmessagecontent
 *
 * @property-read string $phoneNumber - Contact's phone number
 * @property-read string $firstName - Contact's first name
 * @property-read string|null $lastName - Contact's last name
 * @property-read string|null $vcard - Additional data about the contact in the form of a vCard, 0-2048 bytes
 *
 * @method self setPhoneNumber(string $phoneNumber)
 * @method self setFirstName(string $firstName)
 * @method self setLastName(string $lastName)
 */
class InputContactMessageContent extends InputMessageContent
{
    protected string $phoneNumber;
    protected string $firstName;
    protected ?string $lastName;
    protected ?string $vcard;

    /**
     * @param string $vcard
     * @return $this
     * @throws TelegramArgsException
     *
     * @link https://en.wikipedia.org/wiki/VCard
     * @link https://docs.fileformat.com/email/vcf/
     */
    public function setVcard(string $vcard): self
    {
        if (preg_match("/^BEGIN:VCARD\nVERSION:(2\.1|3\.0|4\.0)\n.+\nEND:VCARD$/s", $vcard)) {
            $this->vcard = $vcard;
            return $this;
        }
        throw new TelegramArgsException(
            'Field "vcard" has incorrect structure. It should begins with "BEGIN:VCARD", next line "VERSION:2.1/3.0/4.0" and end with END:VCARD'
        );
    }
}

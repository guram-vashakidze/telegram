<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Enums\MimeTypeEnum;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultMpeg4Gif
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a video animation (H.264/MPEG-4 AVC video without sound). By default, this animated MPEG-4 file
 * will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message
 * with the specified content instead of the animation.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultmpeg4gif
 *
 * @property-read string $mpeg4Url - A valid URL for the MPEG4 file. File size must not exceed 1MB
 * @property-read int|null $mpeg4Width - Width of the MPEG4
 * @property-read int|null $mpeg4Height - Height of the MPEG4
 * @property-read int|null $mpeg4Duration - Duration of the MPEG4 in seconds
 * @property-read string $thumbUrl - URL of the static (JPEG or MPEG4) or animated (MPEG4) thumbnail for the result
 * @property-read MimeTypeEnum|null $thumbMimeType - MIME type of the thumbnail, must be one of “image/jpeg”, “image/gif”, or “video/mp4”. Defaults to “image/jpeg”
 *
 * @method self setMpeg4Url(string $mpeg4Url)
 * @method self setMpeg4Width(int $mpeg4Width)
 * @method self setMpeg4Height(int $mpeg4Height)
 * @method self setMpeg4Duration(int $mpeg4Duration)
 * @method self setThumbUrl(int $thumbUrl)
 */
class InlineQueryResultMpeg4Gif extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $mpeg4Url;
    protected ?int $mpeg4Width;
    protected ?int $mpeg4Height;
    protected ?int $mpeg4Duration;
    protected string $thumbUrl;
    protected ?MimeTypeEnum $thumbMimeType;

    public function __construct()
    {
        $this->type = InlineQueryResultType::mpeg4_gif();
    }

    /**
     * @param MimeTypeEnum $thumbMimeType
     * @return $this
     * @throws TelegramArgsException
     */
    public function setThumbMimeType(MimeTypeEnum $thumbMimeType): self
    {
        if ($thumbMimeType->isMpeg4GifMimes()) {
            $this->thumbMimeType = $thumbMimeType;
            return $this;
        }
        throw new TelegramArgsException(
            'Incorrect field "thumb_mime_type" value. Available values: ' . implode(', ', MimeTypeEnum::mpeg4GifMimes())
        );
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class SetChatStickerSet
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchatstickerset
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read string $stickerSetName - Name of the sticker set to be set as the group sticker set
 *
 * @method self setStickerSetName(string $stickerSetName)
 *
 * @method bool send()
 */
class SetChatStickerSet extends InputType
{
    use HasChatId;

    protected string $stickerSetName;
}

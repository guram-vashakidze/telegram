<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultAudio
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to an MP3 audio file. By default, this audio file will be sent by the user. Alternatively, you can
 * use input_message_content to send a message with the specified content instead of the audio.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultaudio
 *
 * @property-read string $title - Recording title
 * @property-read string $audioUrl - A valid URL for the audio file
 * @property-read int|null $audioDuration - Audio duration in seconds
 * @property-read string|null $performer - Performer
 *
 * @method self setAudioUrl(string $audioUrl)
 * @method self setAudioDuration(int $audioDuration)
 * @method self setPerformer(string $performer)
 */
class InlineQueryResultAudio extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $audioUrl;
    protected ?int $audioDuration;
    protected ?string $performer;

    public function __construct()
    {
        $this->type = InlineQueryResultType::audio();
    }
}

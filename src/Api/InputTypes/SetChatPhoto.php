<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class SetChatPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchatphoto
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read InputFile $photo - New chat photo, uploaded using multipart/form-data
 *
 * @method bool send()
 */
class SetChatPhoto extends InputType
{
    use HasChatId;

    protected InputFile $photo;

    public function setPhoto(InputFile $photo): self
    {
        $this->photo = $photo->setName('photo');
        return $this;
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\MenuButton;

/**
 * Class SetChatMenuButton
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read int|null $chatId - Unique identifier for the target private chat. If not specified, default bot's menu button will be changed
 * @property-read MenuButton $menuButton - A JSON-serialized object for the bot's new menu button. Defaults to MenuButtonDefault
 *
 * @method self setChatId(int $chatId)
 * @method self setMenuButton(MenuButton $menuButton)
 *
 * @method bool send()
 */
class SetChatMenuButton extends InputType
{
    protected ?int $chatId;
    protected ?MenuButton $menuButton;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Enums\MimeTypeEnum;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultVideo
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a page containing an embedded video player or a video file. By default, this video file will be
 * sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the
 * specified content instead of the video.
 * If an InlineQueryResultVideo message contains an embedded video (e.g., YouTube), you must replace its content using
 * input_message_content.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultvideo
 *
 * @property-read string $title - Recording title
 * @property-read string $videoUrl - A valid URL for the embedded video player or video file
 * @property-read string $mimeType - MIME type of the content of the video URL, “text/html” or “video/mp4”
 * @property-read int|null $videoWidth - Width of the video
 * @property-read int|null $videoHeight - Height of the video
 * @property-read int|null $videoDuration - Duration of the video in seconds
 * @property-read string $thumbUrl - URL of the thumbnail (JPEG only) for the video
 * @property-read string|null $description - Short description of the result
 *
 * @method self setVideoUrl(string $videoUrl)
 * @method self setVideoWidth(int $videoWidth)
 * @method self setVideoHeight(int $videoHeight)
 * @method self setVideoDuration(int $videoDuration)
 * @method self setThumbUrl(int $thumbUrl)
 * @method self setDescription(string $description)
 */
class InlineQueryResultVideo extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $videoUrl;
    protected MimeTypeEnum $mimeType;
    protected ?int $videoWidth;
    protected ?int $videoHeight;
    protected ?int $videoDuration;
    protected string $thumbUrl;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::video();
    }

    /**
     * @param MimeTypeEnum $mimeType
     * @return $this
     * @throws TelegramArgsException
     */
    public function setMimeType(MimeTypeEnum $mimeType): self
    {
        if ($mimeType->isVideoMimes()) {
            $this->mimeType = $mimeType;
            return $this;
        }
        throw new TelegramArgsException(
            'Incorrect field "mime_type" value. Available values: ' . implode(', ', MimeTypeEnum::videoMimes())
        );
    }
}

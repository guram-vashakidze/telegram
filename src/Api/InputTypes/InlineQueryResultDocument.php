<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Enums\MimeTypeEnum;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultDocument
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a file. By default, this file will be sent by the user with an optional caption. Alternatively,
 * you can use input_message_content to send a message with the specified content instead of the file. Currently, only
 * .PDF and .ZIP files can be sent using this method.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultdocument
 *
 * @property-read string $title - Recording title
 * @property-read string $documentUrl - A valid URL for the file
 * @property-read string $mimeType - MIME type of the content of the file, either “application/pdf” or “application/zip”
 * @property-read string|null $thumbUrl - URL of the thumbnail (JPEG only) for the file
 * @property-read int|null $thumbWidth - Thumbnail width
 * @property-read int|null $thumbHeight - Thumbnail height
 * @property-read string|null $description - Short description of the result
 *
 * @method self setDocumentUrl(string $documentUrl)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setThumbWidth(int $thumbWidth)
 * @method self setThumbHeight(int $thumbHeight)
 * @method self setDescription(string $description)
 */
class InlineQueryResultDocument extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $documentUrl;
    protected MimeTypeEnum $mimeType;
    protected ?string $thumbUrl;
    protected ?int $thumbWidth;
    protected ?int $thumbHeight;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::document();
    }

    /**
     * @param MimeTypeEnum $mimeType
     * @return $this
     * @throws TelegramArgsException
     */
    public function setMimeType(MimeTypeEnum $mimeType): self
    {
        if ($mimeType->isDocumentMimes()) {
            $this->mimeType = $mimeType;
            return $this;
        }
        throw new TelegramArgsException(
            'Incorrect field "mime_type" value. Available values: ' . implode(', ', MimeTypeEnum::documentMimes())
        );
    }
}

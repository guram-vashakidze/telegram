<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class BanChatSenderChat
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#banchatsenderchat
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $senderChatId - Unique identifier of the target sender chat
 *
 * @method self setSenderChatId(int $senderChatId)
 *
 * @method bool send()
 */
class BanChatSenderChat extends InputType
{
    use HasChatId;

    protected int $senderChatId;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\MenuButton;

/**
 * Class GetChatMenuButton
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read int|null $chatId - Unique identifier for the target private chat. If not specified, default bot's menu button will be changed
 *
 * @method self setChatId(int $chatId)
 *
 * @method MenuButton send()
 */
class GetChatMenuButton extends InputType
{
    protected ?int $chatId;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class AnswerPreCheckoutQuery
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @property-read string $preCheckoutQueryId - Unique identifier for the query to be answered
 * @property-read bool $ok - Specify True if everything is alright (goods are available, etc.) and the bot is ready to proceed with the order. Use False if there are any problems.
 * @property-read string|null $errorMessage - Required if ok is False. Error message in human readable form that explains the reason for failure to proceed with the checkout (e.g. "Sorry, somebody just bought the last of our amazing black T-shirts while you were busy filling out your payment details. Please choose a different color or garment!"). Telegram will display this message to the user.
 *
 * @method self setPreCheckoutQueryId(string $preCheckoutQueryId)
 * @method self setOk(bool $ok = true)
 * @method self setErrorMessage(string $errorMessage)
 *
 * @method bool send()
 */
class AnswerPreCheckoutQuery extends InputType
{
    protected string $preCheckoutQueryId;
    protected bool $ok;
    protected ?string $errorMessage;

    /**
     * @return void
     * @throws TelegramArgsException
     */
    protected function customCheckRequest(): void
    {
        if (!isset($this->ok)) {
            return;
        }
        if ($this->ok === false && !isset($this->errorMessage)) {
            throw new TelegramArgsException('The field "error_message" required when "ok" is False');
        }
    }
}

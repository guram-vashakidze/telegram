<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;

/**
 * Class InlineQueryResultArticle
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to an article or web page.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultarticle
 *
 * @property-read InputMessageContent $inputMessageContent
 * @property-read InlineKeyboardMarkup|null $replyMarkup
 * @property-read string $title - Title of the result
 * @property-read string|null $url - URL of the result
 * @property-read bool|null $hideUrl - Pass True if you don't want the URL to be shown in the message
 * @property-read string|null $description - Short description of the result
 * @property-read string|null $thumbUrl - Url of the thumbnail for the result
 * @property-read int|null $thumbHeight - Thumbnail width
 * @property-read int|null $thumbWidth - Thumbnail height
 *
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 * @method self setTitle(string $title)
 * @method self setUrl(string $url)
 * @method self setHideUrl()
 * @method self setDescription(string $description)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setThumbHeight(int $thumbHeight)
 * @method self setThumbWidth(int $thumbWidth)
 */
class InlineQueryResultArticle extends InlineQueryResult
{
    protected InputMessageContent $inputMessageContent;
    protected ?InlineKeyboardMarkup $replyMarkup;
    protected string $title;
    protected ?string $url;
    protected ?bool $hideUrl;
    protected ?string $description;
    protected ?string $thumbUrl;
    protected ?int $thumbWidth;
    protected ?int $thumbHeight;

    public function __construct()
    {
        $this->type = InlineQueryResultType::article();
    }
}

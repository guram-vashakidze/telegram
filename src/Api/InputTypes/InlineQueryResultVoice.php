<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultVoice
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a voice recording in an .OGG container encoded with OPUS. By default, this voice recording will
 * be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content
 * instead of the the voice message.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultvoice
 *
 * @property-read string $title - Recording title
 * @property-read string $voiceUrl - A valid URL for the voice recording
 * @property-read int|null $voiceDuration - Voice duration in seconds
 *
 * @method self setVoiceUrl(string $voiceUrl)
 * @method self setVoiceDuration(int $voiceDuration)
 */
class InlineQueryResultVoice extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $voiceUrl;
    protected ?int $voiceDuration;

    public function __construct()
    {
        $this->type = InlineQueryResultType::voice();
    }
}

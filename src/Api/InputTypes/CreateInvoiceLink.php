<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInvoiceData;

/**
 * Class CreateInvoiceLink
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @method string send()
 */
class CreateInvoiceLink extends InputType
{
    use HasInvoiceData;
}

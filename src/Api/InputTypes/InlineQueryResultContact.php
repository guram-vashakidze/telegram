<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasVCard;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;

/**
 * Class InlineQueryResultContact
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a contact with a phone number. By default, this contact will be sent by the user. Alternatively, you can
 * use input_message_content to send a message with the specified content instead of the contact.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcontact
 *
 * @property-read string $phoneNumber - Contact's phone number
 * @property-read string $firstName - Contact's first name
 * @property-read string|null $lastName - Contact's last name
 * @property-read string|null $vcard - Additional data about the contact in the form of a vCard, 0-2048 bytes
 * @property-read InlineKeyboardMarkup|null $replyMarkup - Inline keyboard attached to the message
 * @property-read InputMessageContent|null $inputMessageContent - Content of the message to be sent instead of the location
 * @property-read string|null $thumbUrl - Url of the thumbnail for the result
 * @property-read int|null $thumbWidth - Thumbnail width
 * @property-read int|null $thumbHeight - Thumbnail height
 *
 * @method self setPhoneNumber(string $phoneNumber)
 * @method self setFirstName(string $firstName)
 * @method self setLastName(string $lastName)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setThumbWidth(int $thumbWidth)
 * @method self setThumbHeight(int $thumbHeight)
 */
class InlineQueryResultContact extends InlineQueryResult
{
    use HasVCard;

    protected string $phoneNumber;
    protected string $firstName;
    protected ?string $lastName;
    protected ?string $vcard;
    protected ?InlineKeyboardMarkup $replyMarkup;
    protected ?InputMessageContent $inputMessageContent;
    protected ?string $thumbUrl;
    protected ?int $thumbWidth;
    protected ?int $thumbHeight;

    public function __construct()
    {
        $this->type = InlineQueryResultType::contact();
    }
}

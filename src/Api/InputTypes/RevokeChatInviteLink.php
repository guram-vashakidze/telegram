<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\ChatInviteLink;

/**
 * Class RevokeChatInviteLink
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#revokechatinvitelink
 *
 * @property-read int|string $chatId - Unique identifier of the target chat or username of the target channel (in the format @channelusername)
 * @property-read string $inviteLink - The invite link to edit
 *
 * @method self setInviteLink(string $inviteLink)
 *
 * @method ChatInviteLink send()
 */
class RevokeChatInviteLink extends InputType
{
    use HasChatId;

    protected string $inviteLink;
}

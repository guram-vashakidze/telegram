<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\SentWebAppMessage;

/**
 * Class AnswerWebAppQuery
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Use this method to set the result of an interaction with a Web App and send a corresponding message on behalf of the
 * user to the chat from which the query originated. On success, a SentWebAppMessage object is returned.
 *
 * @link https://core.telegram.org/bots/api#answerwebappquery
 *
 * @property-read string $webAppQueryId - Unique identifier for the query to be answered
 * @property-read InlineQueryResult $result - A JSON-serialized object describing the message to be sent
 *
 * @method self setWebAppQueryId(string $webAppQueryId)
 * @method self setResult(InlineQueryResult $webAppQueryId)
 *
 * @method SentWebAppMessage send()
 */
class AnswerWebAppQuery extends InputType
{
    protected string $webAppQueryId;
    protected InlineQueryResult $result;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a photo stored on the Telegram servers. By default, this photo will be sent by the user with an
 * optional caption. Alternatively, you can use input_message_content to send a message with the specified content
 * instead of the photo
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedphoto
 *
 * @property-read string $photoFileId - A valid file identifier of the photo
 * @property-read string|null $description - Short description of the result
 *
 * @method self setPhotoFileId(string $photoFileId)
 * @method self setDescription(string $description)
 */
class InlineQueryResultCachedPhoto extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $photoFileId;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::photo();
    }
}

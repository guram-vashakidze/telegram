<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\GooglePlaceType;

/**
 * Class InputVenueMessageContent
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents the content of a venue message to be sent as the result of an inline query
 *
 * @link https://core.telegram.org/bots/api#inputvenuemessagecontent
 *
 * @property-read float $latitude - Latitude of the venue
 * @property-read float $longitude - Longitude of the venue
 * @property-read string $title - Name of the venue
 * @property-read string $address - Address of the venue
 * @property-read string|null $foursquareId - Foursquare identifier of the venue
 * @property-read string|null $foursquareType - Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
 * @property-read string|null $googlePlaceId - Google Places identifier of the venue
 * @property-read string|null $googlePlaceType - Google Places type of the venue.
 *
 * @method self setLatitude(float $latitude)
 * @method self setLongitude(float $longitude)
 * @method self setTitle(string $title)
 * @method self setAddress(string $address)
 * @method self setFoursquareId(string $foursquareId)
 * @method self setFoursquareType(string $foursquareType)
 * @method self setGooglePlaceId(string $googlePlaceId)
 * @method self setGooglePlaceType(GooglePlaceType $googlePlaceType)
 */
class InputVenueMessageContent extends InputMessageContent
{
    protected float $latitude;
    protected float $longitude;
    protected string $title;
    protected string $address;
    protected ?string $foursquareId;
    protected ?string $foursquareType;
    protected ?string $googlePlaceId;
    protected ?GooglePlaceType $googlePlaceType;
}

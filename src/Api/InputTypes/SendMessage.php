<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageEntity;

/**
 * Class SendMessage
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Use this method to send text messages. On success, the sent Message is returned
 *
 * @link https://core.telegram.org/bots/api#sendmessage
 *
 * @property-read string $text Text of the message to be sent, 1-4096 characters after entities parsing
 * @property-read ParseMode|null $parseMode Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $entities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
 * @property-read bool|null $disableWebPagePreview Disables link previews for links in this message
 *
 * @method self setText(string $text)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setEntities(MessageEntity[] $entities)
 * @method self setDisableWebPagePreview()
 *
 * @method Message send()
 */
class SendMessage extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected string $text;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $entities;
    protected ?bool $disableWebPagePreview;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a photo. By default, this photo will be sent by the user with optional caption. Alternatively,
 * you can use input_message_content to send a message with the specified content instead of the photo.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultphoto
 *
 * @property-read string $photoUrl - A valid URL of the photo. Photo must be in JPEG format. Photo size must not exceed 5MB
 * @property-read string $thumbUrl - URL of the thumbnail for the photo
 * @property-read int|null $photoWidth - Width of the photo
 * @property-read int|null $photoHeight - Height of the photo
 * @property-read string|null $description - Short description of the result
 *
 * @method self setPhotoUrl(string $photoUrl)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setPhotoWidth(int $photoWidth)
 * @method self setPhotoHeight(int $photoHeight)
 * @method self setDescription(string $description)
 */
class InlineQueryResultPhoto extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $photoUrl;
    protected string $thumbUrl;
    protected ?int $photoWidth;
    protected ?int $photoHeight;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::photo();
    }
}

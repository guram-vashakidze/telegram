<?php

namespace Vashakidze\Telegram\Api\InputTypes;

/**
 * Class EditChatInviteLink
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#editchatinvitelink
 *
 * @property-read string $inviteLink - The invite link to edit
 *
 * @method self setInviteLink(string $inviteLink)
 */
class EditChatInviteLink extends CreateChatInviteLink
{
    protected string $inviteLink;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\EmojiType;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\Message;

/**
 * Class SendDice
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#senddice
 *
 * @property-read EmojiType $emoji
 *
 * @method self setEmoji(EmojiType $emoji)
 *
 * @method Message send()
 */
class SendDice extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected ?EmojiType $emoji;
}

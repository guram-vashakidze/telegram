<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\ChatActionType;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class SendChatAction
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendchataction
 *
 * @property-read ChatActionType $action
 *
 * @method self setAction(ChatActionType $action)
 *
 * @method bool send()
 */
class SendChatAction extends InputType
{
    use HasChatId;

    protected ChatActionType $action;
}

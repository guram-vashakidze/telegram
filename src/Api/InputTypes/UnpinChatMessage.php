<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class UnpinChatMessage
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#unpinchatmessage
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read int|null $messageId - Identifier of a message to unpin. If not specified, the most recent pinned message (by sending date) will be unpinned.
 *
 * @method self setMessageId(int $messageId)
 *
 * @method bool send()
 */
class UnpinChatMessage extends InputType
{
    use HasChatId;

    protected ?int $messageId;
}

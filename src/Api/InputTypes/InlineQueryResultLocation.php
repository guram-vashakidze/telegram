<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultLocation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a location on a map. By default, the location will be sent by the user. Alternatively, you can use
 * input_message_content to send a message with the specified content instead of the location.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultlocation
 *
 * @property-read float $latitude - Location latitude in degrees
 * @property-read float $longitude - Location longitude in degrees
 * @property-read string $title - Location title
 * @property-read float|null $horizontalAccuracy - The radius of uncertainty for the location, measured in meters; 0-1500
 * @property-read int|null $livePeriod - Period in seconds for which the location can be updated, should be between 60 and 86400
 * @property-read int|null $heading - For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified
 * @property-read int|null $proximityAlertRadius - For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified
 * @property-read InlineKeyboardMarkup|null $replyMarkup - Inline keyboard attached to the message
 * @property-read InputMessageContent|null $inputMessageContent - Content of the message to be sent instead of the location
 * @property-read string|null $thumbUrl - Url of the thumbnail for the result
 * @property-read int|null $thumbWidth - Thumbnail width
 * @property-read int|null $thumbHeight - Thumbnail height
 *
 * @method self setLatitude(float $latitude)
 * @method self setLongitude(float $longitude)
 * @method self setTitle(string $title)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setThumbUrl(string $thumbUrl)
 * @method self setThumbWidth(int $thumbWidth)
 * @method self setThumbHeight(int $thumbHeight)
 */
class InlineQueryResultLocation extends InlineQueryResult
{
    protected float $latitude;
    protected float $longitude;
    protected string $title;
    protected ?float $horizontalAccuracy;
    protected ?int $livePeriod;
    protected ?int $heading;
    protected ?int $proximityAlertRadius;
    protected ?InlineKeyboardMarkup $replyMarkup;
    protected ?InputMessageContent $inputMessageContent;
    protected ?string $thumbUrl;
    protected ?int $thumbWidth;
    protected ?int $thumbHeight;

    public function __construct()
    {
        $this->type = InlineQueryResultType::location();
    }

    /**
     * @param float $horizontalAccuracy
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHorizontalAccuracy(float $horizontalAccuracy): self
    {
        if ($horizontalAccuracy >= 0 && $horizontalAccuracy <= 1500) {
            $this->horizontalAccuracy = $horizontalAccuracy;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "horizontalAccuracy" field. This field must be between 0 and 1500');
    }

    /**
     * @param float $livePeriod
     * @return $this
     * @throws TelegramArgsException
     */
    public function setLivePeriod(float $livePeriod): self
    {
        if ($livePeriod >= 60 && $livePeriod <= 86400) {
            $this->livePeriod = $livePeriod;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "livePeriod" field. This field must be between 60 and 86400');
    }

    /**
     * @param int $heading
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHeading(int $heading): self
    {
        if ($heading >= 1 && $heading <= 360) {
            $this->heading = $heading;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "heading" field. This field must be between 1 and 360');
    }

    /**
     * @param int $proximityAlertRadius
     * @return $this
     * @throws TelegramArgsException
     */
    public function setProximityAlertRadius(int $proximityAlertRadius): self
    {
        if ($proximityAlertRadius >= 1 && $proximityAlertRadius <= 100000) {
            $this->proximityAlertRadius = $proximityAlertRadius;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "proximityAlertRadius" field. This field must be between 1 and 360');
    }
}

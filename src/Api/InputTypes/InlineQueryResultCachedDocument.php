<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedDocument
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a file stored on the Telegram servers. By default, this file will be sent by the user with an
 * optional caption. Alternatively, you can use input_message_content to send a message with the specified content
 * instead of the file.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcacheddocument
 *
 * @property-read string $title - Recording title
 * @property-read string $documentFileId - A valid file identifier for the file
 * @property-read string|null $description - Short description of the result
 *
 * @method self setDocumentFileId(string $documentFileId)
 * @method self setDescription(string $description)
 */
class InlineQueryResultCachedDocument extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $documentFileId;
    protected ?string $description;

    public function __construct()
    {
        $this->type = InlineQueryResultType::document();
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageEntity;

/**
 * Class SendAnimation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendanimation
 *
 * @property-read InputFile|string $animation Animation file to send. Pass a file_id as String to send an animation file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an animation file from the Internet, or upload a new one using multipart/form-data
 * @property-read int|null $duration Duration of the animation in seconds
 * @property-read int|null $width Animation width
 * @property-read int|null $height Animation height
 * @property-read InputFile|string|null $thumb Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>
 * @property-read string|null $caption Photo caption (may also be used when resending animations by file_id), 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $captionEntities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
 *
 * @method self setDuration(int $duration)
 * @method self setWidth(int $width)
 * @method self setHeight(int $height)
 * @method self setCaption(string $caption)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 *
 * @method Message send()
 */
class SendAnimation extends InputType
{
    use HasThumb;
    use HasDefaultFields;
    use HasChatId;

    protected InputFile|string $animation;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $captionEntities;
    protected ?int $duration;
    protected ?string $performer;
    protected ?string $title;
    protected InputFile|string|null $thumb;

    public function setAnimation(InputFile|string $animation): self
    {
        if ($animation instanceof InputFile) {
            $this->animation = $animation->setName('animation');
            return $this;
        }
        $this->animation = $animation;
        return $this;
    }
}

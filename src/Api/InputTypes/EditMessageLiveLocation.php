<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class EditMessageLiveLocation
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#editmessagelivelocation
 *
 * @property-read int|null $messageId - Required if inline_message_id is not specified. Identifier of the message to edit
 * @property-read string|null $inlineMessageId - Required if chat_id and message_id are not specified. Identifier of the inline message
 * @property-read float $latitude - Latitude of new location
 * @property-read float $longitude - Longitude of new location
 * @property-read float|null $horizontalAccuracy - The radius of uncertainty for the location, measured in meters; 0-1500
 * @property-read int|null $heading - Direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
 * @property-read int|null $proximityAlertRadius - For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
 * @property-read InlineKeyboardMarkup $replyMarkup - A JSON-serialized object for a new inline keyboard
 *
 * @method self setMessageId(int $messageId)
 * @method self setInlineMessageId(string $inlineMessageId)
 * @method self setLatitude(float $latitude)
 * @method self setLongitude(float $longitude)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 *
 * @method Message|bool send()
 */
class EditMessageLiveLocation extends InputType
{
    use HasChatId;

    protected ?int $messageId;
    protected ?string $inlineMessageId;
    protected float $latitude;
    protected float $longitude;
    protected ?float $horizontalAccuracy;
    protected ?int $heading;
    protected ?int $proximityAlertRadius;
    protected InlineKeyboardMarkup|null $replyMarkup;

    /**
     * @param float $horizontalAccuracy
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHorizontalAccuracy(float $horizontalAccuracy): self
    {
        if ($horizontalAccuracy >= 0 && $horizontalAccuracy <= 1500) {
            $this->horizontalAccuracy = $horizontalAccuracy;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "horizontalAccuracy" field. This field must be between 0 and 1500');
    }

    /**
     * @param int $heading
     * @return $this
     * @throws TelegramArgsException
     */
    public function setHeading(int $heading): self
    {
        if ($heading >= 1 && $heading <= 360) {
            $this->heading = $heading;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "heading" field. This field must be between 1 and 360');
    }

    /**
     * @param int $proximityAlertRadius
     * @return $this
     * @throws TelegramArgsException
     */
    public function setProximityAlertRadius(int $proximityAlertRadius): self
    {
        if ($proximityAlertRadius >= 1 && $proximityAlertRadius <= 100000) {
            $this->proximityAlertRadius = $proximityAlertRadius;
            return $this;
        }
        throw new TelegramArgsException('Incorrect "proximityAlertRadius" field. This field must be between 1 and 360');
    }

    /**
     * @throws TelegramArgsException
     */
    protected function customCheckRequest(): void
    {
        if (!isset($this->inlineMessageId) || empty($this->inlineMessageId)) {
            if (!isset($this->messageId) || empty($this->messageId)) {
                throw new TelegramArgsException('The field "messageId" is required without "inlineMessageId"');
            }
            if (!isset($this->chatId) || empty($this->chatId)) {
                throw new TelegramArgsException('The field "chatId" is required without "inlineMessageId"');
            }
            return;
        }
        if ((isset($this->messageId) && !empty($this->messageId)) || (isset($this->chatId) && !empty($this->chatId))) {
            throw new TelegramArgsException('Only "inlineMessageId" or "messageId"+"chatId" can be in request');
        }
    }
}

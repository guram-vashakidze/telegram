<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;
use Vashakidze\Telegram\Api\Types\MessageEntity;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InlineQueryResultCachedAudio
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to an MP3 audio file stored on the Telegram servers. By default, this audio file will be sent by
 * the user. Alternatively, you can use input_message_content to send a message with the specified content instead of
 * the audio.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedaudio
 *
 * @property-read string $audioFileId - A valid URL for the audio file
 * @property-read string|null $caption - Caption, 0-1024 characters after entities parsing
 * @property-read ParseMode|null $parseMode - Mode for parsing entities in the voice message caption. See formatting options for more details.
 * @property-read MessageEntity[]|null $captionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
 * @property-read InputMessageContent|null $inputMessageContent
 * @property-read InlineKeyboardMarkup|null $replyMarkup
 *
 * @method self setAudioFileId(string $audioFileId)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 * @method self setInputMessageContent(InputMessageContent $inputMessageContent)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 */
class InlineQueryResultCachedAudio extends InlineQueryResult
{
    protected string $audioFileId;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected ?array $captionEntities;
    protected ?InputMessageContent $inputMessageContent;
    protected ?InlineKeyboardMarkup $replyMarkup;

    public function __construct()
    {
        $this->type = InlineQueryResultType::audio();
    }

    /**
     * @param string $caption
     * @return $this
     * @throws TelegramArgsException
     */
    public function setCaption(string $caption): self
    {
        $length = Str::length($caption);
        if ($length >= 0 && $length <= 1024) {
            $this->caption = $caption;
            return $this;
        }
        throw new TelegramArgsException('The field "caption" must be between 0 and 1024 charsets');
    }
}

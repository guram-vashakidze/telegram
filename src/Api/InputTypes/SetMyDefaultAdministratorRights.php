<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\ChatAdministratorRights;

/**
 * Class SetMyDefaultAdministratorRights
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setmydefaultadministratorrights
 *
 * @property-read ChatAdministratorRights|null $chatAdministratorRights
 * @property-read bool|null $forChannels
 *
 * @method self setChatAdministratorRights(ChatAdministratorRights $chatAdministratorRights)
 * @method self setForChannels()
 *
 * @method bool send()
 */
class SetMyDefaultAdministratorRights extends InputType
{
    protected ?ChatAdministratorRights $chatAdministratorRights;
    protected ?bool $forChannels;
}

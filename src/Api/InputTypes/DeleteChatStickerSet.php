<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class DeleteChatStickerSet
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#deletechatstickerset
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 *
 * @method bool send()
 */
class DeleteChatStickerSet extends InputType
{
    use HasChatId;
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\Types\InlineKeyboardMarkup;

/**
 * Class InlineQueryResultGame
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a Game.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultgame
 *
 * @property-read string $gameShortName - Short name of the game
 * @property-read InlineKeyboardMarkup|null $replyMarkup - Inline keyboard attached to the message
 *
 * @method self setGameShortName(string $gameShortName)
 * @method self setReplyMarkup(InlineKeyboardMarkup $replyMarkup)
 */
class InlineQueryResultGame extends InlineQueryResult
{
    protected string $gameShortName;
    protected ?InlineKeyboardMarkup $replyMarkup;

    public function __construct()
    {
        $this->type = InlineQueryResultType::game();
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Carbon;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class BanChatMember
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#banchatmember
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 * @property-read Carbon|null $untilDate - Date when the user will be unbanned, unix time. If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever. Applied for supergroups and channels only.
 * @property-read bool|null $revokeMessages - Pass True to delete all messages from the chat for the user that is being removed. If False, the user will be able to see messages in the group that were sent before the user was removed. Always True for supergroups and channels.
 *
 * @method self setUserId(int $userId)
 * @method self setRevokeMessages(bool $revokeMessages = true)
 *
 * @method bool send()
 */
class BanChatMember extends InputType
{
    use HasChatId;

    protected int $userId;
    protected ?Carbon $untilDate;
    protected ?bool $revokeMessages;

    /**
     * @param Carbon $untilDate
     * @return $this
     * @throws TelegramArgsException
     */
    public function setUntilDate(Carbon $untilDate): self
    {
        if ($untilDate->isFuture()) {
            $this->untilDate = $untilDate;
            return $this;
        }
        throw new TelegramArgsException('Field "until_date" must be in future.');
    }
}

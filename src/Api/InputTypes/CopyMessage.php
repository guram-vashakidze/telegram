<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\MessageEntity;
use Vashakidze\Telegram\Api\Types\MessageId;

/**
 * Class CopyMessage
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Use this method to copy messages of any kind. Service messages and invoice messages can't be copied. The method is
 * analogous to the method forwardMessage, but the copied message doesn't have a link to the original message. Returns
 * the MessageId of the sent message on success
 *
 * @link https://core.telegram.org/bots/api#copymessage
 *
 * @property-read int|string $fromChatId Unique identifier for the chat where the original message was sent (or channel username in the format @channelusername)
 * @property-read int $messageId Message identifier in the chat specified in from_chat_id
 * @property-read string|null $caption Text of the message to be sent, 1-4096 characters after entities parsing
 * @property-read ParseMode|null $parseMode Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $captionEntities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
 *
 * @method self setFromChatId(int|string $fromChatId)
 * @method self setMessageId(int $messageId)
 * @method self setCaption(string $caption)
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setCaptionEntities(MessageEntity[] $captionEntities)
 *
 * @method MessageId send()
 */
class CopyMessage extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected int|string $fromChatId;
    protected int $messageId;
    protected ?string $caption;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $captionEntities;
}

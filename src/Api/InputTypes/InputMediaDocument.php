<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InputMediaType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;

/**
 * Class InputMediaDocument
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a general file to be sent.
 *
 * @link https://core.telegram.org/bots/api#inputmediadocument
 *
 * @property-read bool|null $disableContentTypeDetection - Disables automatic server-side content type detection for files uploaded using multipart/form-data. Always True, if the document is sent as part of an album
 *
 * @method self setDisableContentTypeDetection()
 */
class InputMediaDocument extends InputMedia
{
    use HasThumb;

    protected ?bool $disableContentTypeDetection;

    public function __construct()
    {
        $this->type = InputMediaType::document();
    }

    protected function isMultiUpload(): bool
    {
        return true;
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\Enums\PollType;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageEntity;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class SendPoll
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendpoll
 *
 * @property-read string $question - Poll question, 1-300 characters
 * @property-read string[] $options - A JSON-serialized list of answer options, 2-10 strings 1-100 characters each
 * @property-read bool|null $isAnonymous - True, if the poll needs to be anonymous, defaults to True
 * @property-read PollType|null $type - Poll type, “quiz” or “regular”, defaults to “regular”
 * @property-read bool|null $allowsMultipleAnswers - True, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to False
 * @property-read int|null $correctOptionId - 0-based identifier of the correct answer option, required for polls in quiz mode
 * @property-read string|null $explanation - Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds after entities parsing
 * @property-read ParseMode|null $explanationParseMode - Mode for parsing entities in the explanation. See formatting options for more details.
 * @property-read MessageEntity[]|null $explanationEntities - A JSON-serialized list of special entities that appear in the poll explanation, which can be specified instead of parse_mode
 * @property-read int|null $openPeriod - Amount of time in seconds the poll will be active after creation, 5-600. Can't be used together with close_date.
 * @property-read Carbon|null $closeDate - Point in time (Unix timestamp) when the poll will be automatically closed. Must be at least 5 and no more than 600 seconds in the future. Can't be used together with open_period.
 * @property-read bool|null $isClosed - Pass True if the poll needs to be immediately closed. This can be useful for poll preview.
 *
 * @method self setIsAnonymous(bool $isAnonymous)
 * @method self setType(PollType $type)
 * @method self setAllowsMultipleAnswers()
 * @method self setCorrectOptionId(int $correctOptionId)
 * @method self setExplanationParseMode(ParseMode $explanationParseMode)
 * @method self setExplanationEntities(MessageEntity $explanationEntities)
 * @method self setIsClosed()
 *
 * @method Message send()
 */
class SendPoll extends InputType
{
    use HasDefaultFields;
    use HasChatId;

    protected string $question;
    protected array|JsonSerializable $options;
    protected ?bool $isAnonymous;
    protected ?PollType $type;
    protected ?bool $allowsMultipleAnswers;
    protected ?int $correctOptionId;
    protected ?string $explanation;
    protected ?ParseMode $explanationParseMode;
    protected array|JsonSerializable|null $explanationEntities;
    protected ?int $openPeriod;
    protected ?Carbon $closeDate;
    protected ?bool $isClosed;

    /**
     * @param string $question
     * @return $this
     * @throws TelegramArgsException
     */
    public function setQuestion(string $question): self
    {
        $length = Str::length($question);
        if ($length >= 1 && $length <= 300) {
            $this->question = $question;
            return $this;
        }
        throw new TelegramArgsException('Length of "question" field must be between 1 and 300 charsets');
    }

    /**
     * @param array $options
     * @return $this
     * @throws TelegramArgsException
     */
    public function setOptions(array $options): self
    {
        $length = count($options);
        if ($length < 2 || $length > 10) {
            throw new TelegramArgsException('Count of "options" must be between 2 and 10');
        }
        foreach ($options as $option) {
            $length = Str::length($option);
            if ($length >= 1 && $length <= 100) {
                continue;
            }
            throw new TelegramArgsException(
                'Incorrect option variant length: "' . $option . '". The length must be between 1 and 100 charset'
            );
        }
        $this->options = $options;
        return $this;
    }

    /**
     * @param string $explanation
     * @return $this
     * @throws TelegramArgsException
     */
    public function setExplanation(string $explanation): self
    {
        $length = Str::length($explanation);
        if ($length >= 1 && $length <= 200) {
            $this->explanation = $explanation;
            return $this;
        }
        throw new TelegramArgsException('Length of "explanation" field must be between 1 and 200 charsets');
    }

    /**
     * @param int $openPeriod
     * @return $this
     * @throws TelegramArgsException
     */
    public function setOpenPeriod(int $openPeriod): self
    {
        if (isset($this->closeDate) && !empty($this->closeDate)) {
            throw new TelegramArgsException('Field "open_period" can not be used together with "close_date"');
        }
        if ($openPeriod >= 5 && $openPeriod <= 600) {
            $this->openPeriod = $openPeriod;
            return $this;
        }
        throw new TelegramArgsException('Field "open_period" must be between 5 and 600');
    }

    /**
     * @param Carbon $closeDate
     * @return $this
     * @throws TelegramArgsException
     */
    public function setCloseDate(Carbon $closeDate): self
    {
        if (isset($this->openPeriod) && !empty($this->openPeriod)) {
            throw new TelegramArgsException('Field "close_date" can not be used together with "open_period"');
        }
        if ($closeDate->isPast()) {
            throw new TelegramArgsException('Field "close_date" must be in future');
        }
        $diff = $closeDate->diffInSeconds(Carbon::now());
        if ($diff >= 5 || $diff <= 600) {
            $this->closeDate = $closeDate;
            return $this;
        }
        throw new TelegramArgsException(
            'Field "close_date" must be at least 5 and no more than 600 seconds in the future'
        );
    }

    /**
     * @throws TelegramArgsException
     */
    protected function customCheckRequest(): void
    {
        if (!isset($this->type) || empty($this->type)) {
            return;
        }
        if (!$this->type->isQuiz()) {
            return;
        }
        if (!isset($this->correctOptionId) || $this->correctOptionId === null) {
            throw new TelegramArgsException('Field "correct_option_id" is required for type "quiz"');
        }
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\ChatMember;

/**
 * Class GetChatMember
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#getchatmember
 *
 * @property-read int|string $chatId - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
 * @property-read int $userId - Unique identifier of the target user
 *
 * @method self setUserId(int $userId)
 *
 * @method ChatMember send()
 */
class GetChatMember extends InputType
{
    use HasChatId;

    protected int $userId;
}

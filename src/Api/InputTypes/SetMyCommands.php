<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\LanguageCode;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\Types\BotCommand;
use Vashakidze\Telegram\Api\Types\BotCommandScope;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class SetMyCommands
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setmycommands
 *
 * @property-read BotCommand[] $commands - A JSON-serialized list of bot commands to be set as the list of the bot's commands. At most 100 commands can be specified.
 * @property-read BotCommandScope|null $scope - A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
 * @property-read LanguageCode|null $languageCode - A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
 *
 * @method self setScope(BotCommandScope $scope)
 * @method self setLanguageCode(LanguageCode $code)
 *
 * @method bool send()
 */
class SetMyCommands extends InputType
{
    protected array|JsonSerializable $commands;
    protected ?BotCommandScope $scope;
    protected ?LanguageCode $languageCode;

    /**
     * @param BotCommand[] $commands
     * @return $this
     * @throws TelegramArgsException
     */
    public function setCommands(array $commands): self
    {
        if (count($commands) <= 100) {
            $this->commands = $commands;
            return $this;
        }
        throw new TelegramArgsException('Max value of commands count is 100');
    }
}

<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InputMediaType;

/**
 * Class InputMediaPhoto
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a photo to be sent.
 *
 * @link https://core.telegram.org/bots/api#inputmediaphoto
 */
class InputMediaPhoto extends InputMedia
{
    public function __construct()
    {
        $this->type = InputMediaType::photo();
    }

    protected function isMultiUpload(): bool
    {
        return true;
    }
}

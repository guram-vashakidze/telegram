<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Str;
use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\ParseMode;
use Vashakidze\Telegram\Api\Types\MessageEntity;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class InputTextMessageContent
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#inputtextmessagecontent
 *
 * @property-read string $messageText - Text of the message to be sent, 1-4096 characters
 * @property-read ParseMode|null $parseMode - Mode for parsing entities in the message text.
 * @property-read MessageEntity[]|null $entities - List of special entities that appear in message text, which can be specified instead of parse_mode
 * @property-read bool|null $disableWebPagePreview - List of special entities that appear in message text, which can be specified instead of parse_mode
 *
 * @method self setParseMode(ParseMode $parseMode)
 * @method self setEntities(MessageEntity[] $entities)
 * @method self setDisableWebPagePreview()
 */
class InputTextMessageContent extends InputMessageContent
{
    protected string $messageText;
    protected ?ParseMode $parseMode;
    protected array|JsonSerializable|null $entities;
    protected ?bool $disableWebPagePreview;

    /**
     * @param string $messageText
     * @return $this
     * @throws TelegramArgsException
     */
    public function setMessageText(string $messageText): self
    {
        $length = Str::length($messageText);

        if ($length >= 1 && $length <= 4096) {
            $this->messageText = $messageText;
            return $this;
        }
        throw new TelegramArgsException('The field "message_text" must be between 1 and 4096 charsets');
    }
}

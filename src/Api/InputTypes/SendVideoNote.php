<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasDefaultFields;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasThumb;
use Vashakidze\Telegram\Api\Types\Message;

/**
 * Class SendVideoNote
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#sendvideoNote
 *
 * @property-read InputFile|string $videoNote VideoNote file to send. Pass a file_id as String to send an videoNote file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an videoNote file from the Internet, or upload a new one using multipart/form-data
 * @property-read int|null $duration Duration of the videoNote in seconds
 * @property-read int|null $length VideoNote length
 * @property-read InputFile|string|null $thumb Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's length and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>
 *
 * @method self setDuration(int $duration)
 * @method self setLength(int $length)
 *
 * @method Message send()
 */
class SendVideoNote extends InputType
{
    use HasThumb;
    use HasDefaultFields;
    use HasChatId;

    protected InputFile|string $videoNote;
    protected ?int $duration;
    protected ?int $length;
    protected InputFile|string|null $thumb;

    public function setVideoNote(InputFile|string $videoNote): self
    {
        if ($videoNote instanceof InputFile) {
            $this->videoNote = $videoNote->setName('video_note');
            return $this;
        }
        $this->videoNote = $videoNote;
        return $this;
    }
}

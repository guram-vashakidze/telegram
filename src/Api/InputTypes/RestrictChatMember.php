<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Illuminate\Support\Carbon;
use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\ChatPermissions;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class RestrictChatMember
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#restrictchatmember
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 * @property-read ChatPermissions $permissions - A JSON-serialized object for new user permissions
 * @property-read bool|null $revokeMessages - Pass True to delete all messages from the chat for the user that is being removed. If False, the user will be able to see messages in the group that were sent before the user was removed. Always True for supergroups and channels.
 *
 * @method self setUserId(int $userId)
 * @method self setRevokeMessages(bool $revokeMessages = true)
 * @method self setPermissions(ChatPermissions $permissions)
 *
 * @method bool send()
 */
class RestrictChatMember extends InputType
{
    use HasChatId;

    protected int $userId;
    protected ChatPermissions $permissions;
    protected ?Carbon $untilDate;

    /**
     * @param Carbon $untilDate
     * @return $this
     * @throws TelegramArgsException
     */
    public function setUntilDate(Carbon $untilDate): self
    {
        if ($untilDate->isFuture()) {
            $this->untilDate = $untilDate;
            return $this;
        }
        throw new TelegramArgsException('Field "until_date" must be in future.');
    }
}

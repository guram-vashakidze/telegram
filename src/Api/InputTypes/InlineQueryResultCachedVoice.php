<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\Enums\InlineQueryResultType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasInlineQueryResultDefaultFields;

/**
 * Class InlineQueryResultCachedVoice
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * Represents a link to a voice message stored on the Telegram servers. By default, this voice message will be sent by
 * the user. Alternatively, you can use input_message_content to send a message with the specified content instead of
 * the voice message.
 *
 * @link https://core.telegram.org/bots/api#inlinequeryresultcachedvoice
 *
 * @property-read string $title - Recording title
 * @property-read string $voiceFileId - A valid file identifier for the voice message
 *
 * @method self setVoiceUrl(string $voiceFileId)
 */
class InlineQueryResultCachedVoice extends InlineQueryResult
{
    use HasInlineQueryResultDefaultFields;

    protected string $title;
    protected string $voiceFileId;

    public function __construct()
    {
        $this->type = InlineQueryResultType::voice();
    }
}

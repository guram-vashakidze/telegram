<?php

namespace Vashakidze\Telegram\Api\InputTypes;

use Vashakidze\Telegram\Api\InputType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;
use Vashakidze\Telegram\Api\Types\ChatPermissions;

/**
 * Class SetChatPermissions
 * @package Vashakidze\Telegram\Api\InputTypes
 *
 * @link https://core.telegram.org/bots/api#setchatpermissions
 *
 * @property-read int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read ChatPermissions $permissions - A JSON-serialized object for new default chat permissions
 *
 * @method self setPermissions(ChatPermissions $permissions)
 *
 * @method bool send()
 */
class SetChatPermissions extends InputType
{
    use HasChatId;

    protected ChatPermissions $permissions;
}

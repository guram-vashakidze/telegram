<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;

/**
 * Class UserProfilePhotos
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#userprofilephotos
 *
 * @property-read int $totalCount - Total number of profile pictures the target user has
 * @property-read PhotoSize[][] $photos - Requested profile pictures (in up to 4 sizes each)
 */
class UserProfilePhotos extends Type
{
    protected int $totalCount;
    protected array $photos;

    public static function init(array $data): self
    {
        $photos = new self();
        $photos->totalCount = $data['total_count'];
        for ($i = 0, $max = count($data['photos']); $i < $max; $i++) {
            foreach ($data['photos'][$i] as $row) {
                $photos->photos[$i][] = PhotoSize::init($row);
            }
        }
        return $photos;
    }
}

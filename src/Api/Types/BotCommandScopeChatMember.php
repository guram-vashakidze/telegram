<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class BotCommandScopeChatMember
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopechatadministrators
 *
 * @property-read string|int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 * @property-read int $userId - Unique identifier of the target user
 *
 * @method self setUserId(int $userId)
 */
class BotCommandScopeChatMember extends BotCommandScope
{
    use HasChatId;

    protected int $userId;

    public function __construct()
    {
        $this->type = BotCommandScopeType::chat_member();
    }

    public static function init(array $data): self
    {
        $botCommandScope = new self();
        $botCommandScope->chatId = $data['chat_id'];
        $botCommandScope->userId = $data['user_id'];
        return $botCommandScope;
    }
}

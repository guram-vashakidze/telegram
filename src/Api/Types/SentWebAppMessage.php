<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;

/**
 * Class SentWebAppMessage
 * @package Vashakidze\Telegram\Api\Types
 *
 * Describes an inline message sent by a Web App on behalf of a user.
 *
 * @link https://core.telegram.org/bots/api#sentwebappmessage
 *
 * @property-read string|null $inlineMessageId - Identifier of the sent inline message. Available only if there is an inline keyboard attached to the message.
 */
class SentWebAppMessage extends Type
{
    protected ?string $inlineMessageId;

    public static function init(array $data): self
    {
        $sentWebAppMessage = new self();
        $sentWebAppMessage->inlineMessageId = $data['inline_message_id'] ?? null;
        return $sentWebAppMessage;
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use JsonSerializable;
use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasJsonSerialize;

/**
 * Class ChatAdministratorRights
 * @package Vashakidze\Telegram\Api\Types
 *
 * Represents the rights of an administrator in a chat
 *
 * @link https://core.telegram.org/bots/api#chatadministratorrights
 *
 * @property-read bool $isAnonymous - True, if the user's presence in the chat is hidden
 * @property-read bool $canManageChat - True, if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
 * @property-read bool $canDeleteMessages - True, if the administrator can delete messages of other users
 * @property-read bool $canManageVideoChats - True, if the administrator can manage video chats
 * @property-read bool $canRestrictMembers - True, if the administrator can restrict, ban or unban chat members
 * @property-read bool $canPromoteMembers - True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by the user)
 * @property-read bool $canChangeInfo - True, if the user is allowed to change the chat title, photo and other settings
 * @property-read bool $canInviteUsers - True, if the user is allowed to change the chat title, photo and other settings
 * @property-read bool|null $canPostMessages - True, if the administrator can post in the channel; channels only
 * @property-read bool|null $canEditMessages - True, if the administrator can edit messages of other users and can pin messages; channels only
 * @property-read bool|null $canPinMessages - True, if the user is allowed to pin messages; groups and supergroups only
 *
 * @method self setIsAnonymous(bool $isAnonymous = true)
 * @method self setCanManageChat(bool $canManageChat = true)
 * @method self setCanDeleteMessages(bool $canDeleteMessages = true)
 * @method self setCanManageVideoChats(bool $canManageVideoChats = true)
 * @method self setCanRestrictMembers(bool $canRestrictMembers = true)
 * @method self setCanPromoteMembers(bool $canPromoteMembers = true)
 * @method self setCanChangeInfo(bool $canChangeInfo = true)
 * @method self setCanInviteUsers(bool $canInviteUsers = true)
 * @method self setCanPostMessages(bool $canPostMessages = true)
 * @method self setCanEditMessages(bool $canEditMessages = true)
 * @method self setCanPinMessages(bool $canPinMessages = true)
 */
class ChatAdministratorRights extends Type implements JsonSerializable
{
    use HasJsonSerialize;

    protected bool $isAnonymous;
    protected bool $canManageChat;
    protected bool $canDeleteMessages;
    protected bool $canManageVideoChats;
    protected bool $canRestrictMembers;
    protected bool $canPromoteMembers;
    protected bool $canChangeInfo;
    protected bool $canInviteUsers;
    protected ?bool $canPostMessages;
    protected ?bool $canEditMessages;
    protected ?bool $canPinMessages;

    public static function init(array $data): self
    {
        $chatAdministratorRights = new self();
        $chatAdministratorRights->isAnonymous = $data['is_anonymous'];
        $chatAdministratorRights->canManageChat = $data['can_manage_chat'];
        $chatAdministratorRights->canDeleteMessages = $data['can_delete_messages'];
        $chatAdministratorRights->canManageVideoChats = $data['can_manage_video_chats'];
        $chatAdministratorRights->canRestrictMembers = $data['can_restrict_members'];
        $chatAdministratorRights->canPromoteMembers = $data['can_promote_members'];
        $chatAdministratorRights->canChangeInfo = $data['can_change_info'];
        $chatAdministratorRights->canInviteUsers = $data['can_invite_users'];
        $chatAdministratorRights->canPostMessages = array_key_exists(
            'can_post_messages',
            $data
        ) ? $data['can_post_messages'] : null;
        $chatAdministratorRights->canEditMessages = array_key_exists(
            'can_edit_messages',
            $data
        ) ? $data['can_edit_messages'] : null;
        $chatAdministratorRights->canPinMessages = array_key_exists(
            'can_pin_messages',
            $data
        ) ? $data['can_pin_messages'] : null;
        return $chatAdministratorRights;
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasCurrencyAndTotalAmount;

/**
 * Class SuccessfulPayment
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object contains basic information about a successful payment
 *
 * @link https://core.telegram.org/bots/api#successfulpayment
 *
 * @property-read string $invoicePayload Bot specified invoice payload
 * @property-read string|null $shippingOptionId Identifier of the shipping option chosen by the user
 * @property-read OrderInfo|null $orderInfo Order info provided by the user
 * @property-read string $telegramPaymentChargeId Telegram payment identifier
 * @property-read string $providerPaymentChargeId Provider payment identifier
 */
class SuccessfulPayment extends Type
{
    use HasCurrencyAndTotalAmount;

    protected string $invoicePayload;
    protected ?string $shippingOptionId;
    protected ?OrderInfo $orderInfo;
    protected string $telegramPaymentChargeId;
    protected string $providerPaymentChargeId;

    public static function init(array $data): self
    {
        $payment = new self();
        $payment->invoicePayload = $data['invoice_payload'];
        $payment->shippingOptionId = $data['shipping_option_id'] ?? null;
        $payment->orderInfo = !empty($data['order_info']) ? OrderInfo::init($data['order_info']) : null;
        $payment->telegramPaymentChargeId = $data['telegram_payment_charge_id'];
        $payment->providerPaymentChargeId = $data['provider_payment_charge_id'];
        $payment->initCurrencyAndTotalAmount($data);
        return $payment;
    }
}

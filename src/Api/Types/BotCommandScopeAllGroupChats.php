<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;

/**
 * Class BotCommandScopeAllGroupChats
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopeallgroupchats
 */
class BotCommandScopeAllGroupChats extends BotCommandScope
{
    public function __construct()
    {
        $this->type = BotCommandScopeType::all_group_chats();
    }
}

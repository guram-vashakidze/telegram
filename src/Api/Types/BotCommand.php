<?php

namespace Vashakidze\Telegram\Api\Types;

use Illuminate\Support\Str;
use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Exceptions\TelegramArgsException;

/**
 * Class BotCommand
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object represents a bot command
 *
 * @link https://core.telegram.org/bots/api#botcommand
 *
 * @property-read string $command - Text of the command; 1-32 characters. Can contain only lowercase English letters, digits and underscores.
 * @property-read string $description - Description of the command; 1-256 characters.
 */
class BotCommand extends Type
{
    protected string $command;
    protected string $description;

    public static function init(array $data): self
    {
        $botCommand = new self();
        $botCommand->command = $data['command'];
        $botCommand->description = $data['description'];
        return $botCommand;
    }

    /**
     * @param string $command
     * @return $this
     * @throws TelegramArgsException
     */
    public function setCommand(string $command): self
    {
        if (preg_match("/^[a-z0-9_]{1,32}$/", $command)) {
            $this->command = $command;
            return $this;
        }
        throw new TelegramArgsException(
            'Incorrect value in "command" field. It can contain only lowercase English letters, digits and underscores'
        );
    }

    /**
     * @param string $description
     * @return $this
     * @throws TelegramArgsException
     */
    public function setDescription(string $description): self
    {
        $length = Str::length($description);
        if ($length >= 1 && $length <= 256) {
            $this->description = $description;
            return $this;
        }
        throw new TelegramArgsException('The length of field "description" must be between 1 and 256 charsets');
    }
}

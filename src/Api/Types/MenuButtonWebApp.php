<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\MenuButtonType;

/**
 * Class MenuButtonWebApp
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#menubuttonwebapp
 *
 * @property-read string $text
 * @property-read WebAppInfo $webInfo
 *
 * @method self setText(string $text)
 * @method self setWebApp(WebAppInfo $webApp)
 */
class MenuButtonWebApp extends MenuButton
{
    protected string $text;
    protected WebAppInfo $webApp;

    public function __construct()
    {
        $this->type = MenuButtonType::web_app();
    }

    public static function init(array $data): self
    {
        $menuButtonWebApp = new self();
        $menuButtonWebApp->text = $data['text'];
        $menuButtonWebApp->webApp = WebAppInfo::init($data['web_app']);
        return $menuButtonWebApp;
    }
}

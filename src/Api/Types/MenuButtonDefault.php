<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\MenuButtonType;

/**
 * Class MenuButtonDefault
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#menubuttondefault
 */
class MenuButtonDefault extends MenuButton
{
    public function __construct()
    {
        $this->type = MenuButtonType::default();
    }

    public static function init(array $data): self
    {
        return new self();
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use Illuminate\Support\Facades\File as IlluminateFile;
use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Telegram;

/**
 * Class File
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object represents a file ready to be downloaded. The file can be downloaded via the link
 * https://api.telegram.org/file/bot<token>/<file_path>. It is guaranteed that the link will be valid for at least 1 hour.
 * When the link expires, a new one can be requested by calling getFile. The maximum file size to download is 20 MB
 *
 * @link https://core.telegram.org/bots/api#file
 *
 * @property-read string $fileId Identifier for this file, which can be used to download or reuse the file
 * @property-read string $fileUniqueId Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
 * @property-read int|null $fileSize File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this value
 * @property-read string|null $filePath File path. Use https://api.telegram.org/file/bot<token>/<file_path> to get the file.
 */
class File extends Type
{
    protected string $fileId;
    protected string $fileUniqueId;
    protected ?int $fileSize;
    protected ?string $filePath;

    public static function init(array $data): self
    {
        $document = new self();
        $document->fileId = $data['file_id'];
        $document->fileUniqueId = $data['file_unique_id'];
        $document->fileSize = $data['file_size'] ?? null;
        $document->filePath = $data['file_path'] ?? null;
        return $document;
    }

    /**
     * Download file
     * @param string|null $name - name for saving. As default name from telegram
     * @param string|null $path - path for saving. As default storage_path()
     * @return bool|null - null if file can not download.
     */
    public function download(string $name = null, string $path = null): ?bool
    {
        if (empty($this->filePath)) {
            return null;
        }
        $pathInfo = pathinfo($this->filePath);
        $savePath = ($path ?? storage_path(
                )) . DIRECTORY_SEPARATOR . ($name ?? $pathInfo['filename']) . '.' . $pathInfo['extension'];
        return IlluminateFile::put($savePath, Telegram::downloadFile($this->filePath));
    }
}

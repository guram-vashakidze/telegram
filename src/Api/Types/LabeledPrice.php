<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;

/**
 * Class LabeledPrice
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object represents a portion of the price for goods or services.
 *
 * @link https://core.telegram.org/bots/api#labeledprice
 *
 * @property-read string $label - Portion label
 * @property-read int $amount - Price of the product in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies)
 *
 * @method self setLabel(string $label)
 * @method self setAmount(int $amount)
 */
class LabeledPrice extends Type
{
    protected string $label;
    protected int $amount;

    public static function init(array $data): self
    {
        $labeledPrice = new self();

        $labeledPrice->label = $data['label'];
        $labeledPrice->amount = $data['amount'];

        return $labeledPrice;
    }
}

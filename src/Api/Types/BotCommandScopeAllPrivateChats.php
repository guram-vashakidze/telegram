<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;

/**
 * Class BotCommandScopeAllPrivateChats
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopeallprivatechats
 */
class BotCommandScopeAllPrivateChats extends BotCommandScope
{
    public function __construct()
    {
        $this->type = BotCommandScopeType::all_private_chats();
    }
}

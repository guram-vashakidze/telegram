<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\StickerType;
use Vashakidze\Telegram\Api\Type;

/**
 * Class StickerSet
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object represents a sticker set.
 *
 * https://core.telegram.org/bots/api#stickerset
 *
 * @property-read string $name Sticker set name
 * @property-read string $title Sticker set title
 * @property-read StickerType $stickerType Type of stickers in the set, currently one of “regular”, “mask”, “custom_emoji”
 * @property-read bool $isAnimated True, if the sticker set contains animated stickers
 * @property-read bool $isVideo True, if the sticker set contains video stickers
 * @property-read Sticker[] $stickers List of all set stickers
 * @property-read PhotoSize|null $thumb Sticker set thumbnail in the .WEBP, .TGS, or .WEBM format
 */
class StickerSet extends Type
{
    protected string $name;
    protected string $title;
    protected StickerType $stickerType;
    protected bool $isAnimated;
    protected bool $isVideo;
    protected array $stickers;
    protected ?PhotoSize $thumb;

    public static function init(array $data): self
    {
        $stickerSet = new self();

        $stickerSet->name = $data['name'];
        $stickerSet->title = $data['title'];
        $stickerSet->stickerType = StickerType::fromValue($data['sticker_type']);
        $stickerSet->isAnimated = (bool)$data['is_animated'];
        $stickerSet->isVideo = (bool)$data['is_video'];
        foreach ($data['stickers'] as $sticker) {
            $stickerSet->stickers[] = Sticker::init($sticker);
        }
        $stickerSet->thumb = !empty($data['thumb']) ? PhotoSize::init($data['thumb']) : null;

        return $stickerSet;
    }
}

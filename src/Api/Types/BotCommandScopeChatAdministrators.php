<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class BotCommandScopeChatAdministrators
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopechatadministrators
 *
 * @property-read string|int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 */
class BotCommandScopeChatAdministrators extends BotCommandScope
{
    use HasChatId;

    public function __construct()
    {
        $this->type = BotCommandScopeType::chat_administrators();
    }

    public static function init(array $data): self
    {
        $botCommandScope = new self();
        $botCommandScope->chatId = $data['chat_id'];
        return $botCommandScope;
    }
}

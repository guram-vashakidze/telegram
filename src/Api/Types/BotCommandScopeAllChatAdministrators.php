<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;

/**
 * Class BotCommandScopeAllChatAdministrators
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopeallchatadministrators
 */
class BotCommandScopeAllChatAdministrators extends BotCommandScope
{
    public function __construct()
    {
        $this->type = BotCommandScopeType::all_chat_administrators();
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;

/**
 * Class BotCommandScopeDefault
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopedefault
 */
class BotCommandScopeDefault extends BotCommandScope
{
    public function __construct()
    {
        $this->type = BotCommandScopeType::default();
    }
}

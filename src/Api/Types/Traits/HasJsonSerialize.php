<?php

namespace Vashakidze\Telegram\Api\Types\Traits;

use Vashakidze\Telegram\Exceptions\TelegramArgsException;

trait HasJsonSerialize
{
    /**
     * @return array
     * @throws TelegramArgsException
     */
    public function jsonSerialize(): array
    {
        return $this->toRequest();
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types\Traits;

use Vashakidze\Telegram\Api\Enums\Currency;

/**
 * Trait HasCurrencyAndTotalAmount
 * @package Vashakidze\Telegram\Api\Types\Traits
 *
 * @property-read Currency $currency Three-letter ISO 4217 currency code
 * @property-read int $totalAmount Total price in the smallest units of the currency (integer, not float/double).
 */
trait HasCurrencyAndTotalAmount
{
    protected Currency $currency;
    protected int $totalAmount;

    public function getRealTotalAmount(): float
    {
        if (!$this->currency->exp) {
            return (float)$this->totalAmount;
        }
        return (float)($this->totalAmount / $this->currency->exp);
    }

    protected function initCurrencyAndTotalAmount(array $data): void
    {
        $this->currency = Currency::fromValue($data['currency']);
        $this->totalAmount = $data['total_amount'];
    }
}

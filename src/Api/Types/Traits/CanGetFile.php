<?php

namespace Vashakidze\Telegram\Api\Types\Traits;

use Vashakidze\Telegram\Api\InputTypes\GetFile;
use Vashakidze\Telegram\Api\Types\File;
use Vashakidze\Telegram\Telegram;

trait CanGetFile
{
    public function getFile(): File
    {
        return Telegram::getFile(
            GetFile::make()
                ->setFileId($this->fileId)
        );
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasCurrencyAndTotalAmount;

/**
 * Class PreCheckoutQuery
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object contains information about an incoming pre-checkout query
 *
 * @link https://core.telegram.org/bots/api#precheckoutquery
 *
 * @property-read string $id Unique query identifier
 * @property-read User $from User who sent the query
 * @property-read string $invoicePayload Bot specified invoice payload
 * @property-read string|null $shippingOptionId Identifier of the shipping option chosen by the user
 * @property-read OrderInfo|null $orderInfo Order info provided by the user
 */
class PreCheckoutQuery extends Type
{
    use HasCurrencyAndTotalAmount;

    protected string $id;
    protected User $from;
    protected string $invoicePayload;
    protected ?string $shippingOptionId;
    protected ?OrderInfo $orderInfo;

    public static function init(array $data): self
    {
        $checkout = new self();
        $checkout->id = $data['id'];
        $checkout->from = User::init($data['from']);
        $checkout->shippingOptionId = $data['shipping_option_id'] ?? null;
        $checkout->orderInfo = !empty($data['order_info']) ? OrderInfo::init($data['order_info']) : null;
        $checkout->initCurrencyAndTotalAmount($data);
        return $checkout;
    }
}

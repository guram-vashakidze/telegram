<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;

/**
 * Class ShippingOption
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object represents one shipping option.
 *
 * @link https://core.telegram.org/bots/api#shippingoption
 *
 * @property-read string $id - Shipping option identifier
 * @property-read string $title - Option title
 * @property-read LabeledPrice[] $prices - List of price portions
 *
 * @method self setId(string $id)
 * @method self setTitle(string $title)
 * @method self setPrices(LabeledPrice[] $prices)
 */
class ShippingOption extends Type
{
    protected string $id;
    protected string $title;
    protected array $prices;

    public static function init(array $data): self
    {
        $shippingOption = new self();

        $shippingOption->id = $data['id'];
        $shippingOption->title = $data['title'];

        foreach ($data['prices'] as $price) {
            $shippingOption->prices[] = LabeledPrice::init($price);
        }

        return $shippingOption;
    }
}

<?php

namespace Vashakidze\Telegram\Api\Types;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\MenuButtonType;
use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasJsonSerialize;

/**
 * Class MenuButton
 * @package Vashakidze\Telegram\Api\Types
 *
 * @property-read MenuButtonType $type
 *
 * @method self setType(MenuButtonType $type)
 */
abstract class MenuButton extends Type implements JsonSerializable
{
    use HasJsonSerialize;

    protected MenuButtonType $type;

    public static function init(array $data): self
    {
        return MenuButtonType::fromValue($data['type'])
            ->init($data);
    }
}

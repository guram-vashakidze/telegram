<?php

namespace Vashakidze\Telegram\Api\Types;

use JsonSerializable;
use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;
use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasJsonSerialize;

/**
 * Class BotCommandScope
 * @package Vashakidze\Telegram\Api\Types
 *
 * @property-read BotCommandScopeType $type - Scope type
 *
 * @link https://core.telegram.org/bots/api#botcommandscope
 */
abstract class BotCommandScope extends Type implements JsonSerializable
{
    use HasJsonSerialize;

    protected BotCommandScopeType $type;

    public static function init(array $data): self
    {
        return new static();
    }
}

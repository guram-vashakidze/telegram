<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\BotCommandScopeType;
use Vashakidze\Telegram\Api\InputTypes\Traits\HasChatId;

/**
 * Class BotCommandScopeChat
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#botcommandscopechat
 *
 * @property-read string|int $chatId - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
 */
class BotCommandScopeChat extends BotCommandScope
{
    use HasChatId;

    public function __construct()
    {
        $this->type = BotCommandScopeType::chat();
    }

    public static function init(array $data): self
    {
        $botCommandScope = new self();
        $botCommandScope->chatId = $data['chat_id'];
        return $botCommandScope;
    }
}

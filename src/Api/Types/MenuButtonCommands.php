<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Enums\MenuButtonType;

/**
 * Class MenuButtonCommands
 * @package Vashakidze\Telegram\Api\Types
 *
 * @link https://core.telegram.org/bots/api#menubuttoncommands
 */
class MenuButtonCommands extends MenuButton
{
    public function __construct()
    {
        $this->type = MenuButtonType::commands();
    }

    public static function init(array $data): self
    {
        return new self();
    }
}

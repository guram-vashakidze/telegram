<?php

namespace Vashakidze\Telegram\Api\Types;

use Vashakidze\Telegram\Api\Type;
use Vashakidze\Telegram\Api\Types\Traits\HasCurrencyAndTotalAmount;

/**
 * Class Invoice
 * @package Vashakidze\Telegram\Api\Types
 *
 * This object contains basic information about an invoice
 *
 * @link https://core.telegram.org/bots/api#invoice
 *
 * @property-read string $title Product name
 * @property-read string $description Product description
 * @property-read string $startParameter Unique bot deep-linking parameter that can be used to generate this invoice
 */
class Invoice extends Type
{
    use HasCurrencyAndTotalAmount;

    protected string $title;
    protected string $description;
    protected string $startParameter;

    public static function init(array $data): self
    {
        $invoice = new self();
        $invoice->title = $data['title'];
        $invoice->description = $data['description'];
        $invoice->startParameter = $data['start_parameter'];
        $invoice->initCurrencyAndTotalAmount($data);
        return $invoice;
    }
}

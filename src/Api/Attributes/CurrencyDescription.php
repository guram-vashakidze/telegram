<?php

namespace Vashakidze\Telegram\Api\Attributes;

use Attribute;

#[Attribute]
class CurrencyDescription
{
    public function __construct(
        public string $symbol,
        public string $thousandsSep,
        public string $decimalSep,
        public bool $symbolLeft,
        public bool $spaceBetween,
        public int $exp,
        public int $minAmount,
        public int $maxAmount
    ) {
    }
}

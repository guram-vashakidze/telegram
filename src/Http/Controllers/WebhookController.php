<?php

namespace Vashakidze\Telegram\Http\Controllers;

use Illuminate\Routing\Controller;
use Vashakidze\Telegram\Events\UpdateEvent;
use Vashakidze\Telegram\Http\Requests\WebhookRequest;

use function event;

class WebhookController extends Controller
{
    public function __invoke(WebhookRequest $request)
    {
        $update = $request->toType();
        event(new UpdateEvent($update));
        $event = $update->type->getEvent();
        event(new $event($update->{$update->type->toPropertyName()}));
    }
}

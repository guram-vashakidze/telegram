<?php

namespace Vashakidze\Telegram;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Exceptions\InvalidEnumKeyException;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionException;

abstract class TelegramEnum extends Enum
{
    private static array $constCacheArray = [];
    protected static bool $useSnake = true;

    /**
     * @param string $method
     * @param mixed $parameters
     * @return mixed
     * @throws InvalidEnumKeyException
     */
    public function __call($method, $parameters): mixed
    {
        $result = $this->checkIsNotMethod($method);
        if ($result !== null) {
            return $result;
        }
        $result = $this->checkIsMethod($method);
        if ($result !== null) {
            return $result;
        }
        return parent::__call($method, $parameters);
    }

    /**
     * @param string $method
     * @return bool|null
     * @throws InvalidEnumKeyException
     */
    private function checkIsNotMethod(string $method): ?bool
    {
        if (!preg_match("/^isNot[A-Z]/", $method)) {
            return null;
        }
        $key = preg_replace("/^isNot/", "", $method);
        $key = static::$useSnake ? Str::snake($key) : $key;
        return $this->isNot(self::fromKey($key));
    }

    /**
     * @param string $method
     * @return bool|null
     * @throws InvalidEnumKeyException
     */
    private function checkIsMethod(string $method): ?bool
    {
        if (!preg_match("/^is[A-Z]/", $method)) {
            return null;
        }
        $key = preg_replace("/^is/", "", $method);
        $key = static::$useSnake ? Str::snake($key) : $key;
        return $this->is(self::fromKey($key));
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    protected static function getConstants(): array
    {
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, static::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            foreach ($reflect->getReflectionConstants() as $constant) {
                if ($constant->isPrivate()) {
                    continue;
                }
                static::$constCacheArray[$calledClass][$constant->getName()] = $constant->getValue();
            }
        }
        return static::$constCacheArray[$calledClass];
    }
}

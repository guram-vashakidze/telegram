<?php

namespace Vashakidze\Telegram;

use Illuminate\Support\Facades\Facade;
use Vashakidze\Telegram\Api\InputTypes\AnswerCallbackQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerInlineQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerPreCheckoutQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerShippingQuery;
use Vashakidze\Telegram\Api\InputTypes\AnswerWebAppQuery;
use Vashakidze\Telegram\Api\InputTypes\ApproveChatJoinRequest;
use Vashakidze\Telegram\Api\InputTypes\BanChatMember;
use Vashakidze\Telegram\Api\InputTypes\BanChatSenderChat;
use Vashakidze\Telegram\Api\InputTypes\CopyMessage;
use Vashakidze\Telegram\Api\InputTypes\CreateChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\CreateInvoiceLink;
use Vashakidze\Telegram\Api\InputTypes\DeclineChatJoinRequest;
use Vashakidze\Telegram\Api\InputTypes\DeleteChatPhoto;
use Vashakidze\Telegram\Api\InputTypes\DeleteChatStickerSet;
use Vashakidze\Telegram\Api\InputTypes\DeleteMyCommands;
use Vashakidze\Telegram\Api\InputTypes\EditChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\EditMessageLiveLocation;
use Vashakidze\Telegram\Api\InputTypes\ExportChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\ForwardMessage;
use Vashakidze\Telegram\Api\InputTypes\GetChat;
use Vashakidze\Telegram\Api\InputTypes\GetChatAdministrators;
use Vashakidze\Telegram\Api\InputTypes\GetChatMember;
use Vashakidze\Telegram\Api\InputTypes\GetChatMemberCount;
use Vashakidze\Telegram\Api\InputTypes\GetChatMenuButton;
use Vashakidze\Telegram\Api\InputTypes\GetFile;
use Vashakidze\Telegram\Api\InputTypes\GetMyDefaultAdministratorRights;
use Vashakidze\Telegram\Api\InputTypes\GetUpdates;
use Vashakidze\Telegram\Api\InputTypes\GetUserProfilePhotos;
use Vashakidze\Telegram\Api\InputTypes\LeaveChat;
use Vashakidze\Telegram\Api\InputTypes\PinChatMessage;
use Vashakidze\Telegram\Api\InputTypes\PromoteChatMember;
use Vashakidze\Telegram\Api\InputTypes\RestrictChatMember;
use Vashakidze\Telegram\Api\InputTypes\RevokeChatInviteLink;
use Vashakidze\Telegram\Api\InputTypes\SendAnimation;
use Vashakidze\Telegram\Api\InputTypes\SendAudio;
use Vashakidze\Telegram\Api\InputTypes\SendChatAction;
use Vashakidze\Telegram\Api\InputTypes\SendContact;
use Vashakidze\Telegram\Api\InputTypes\SendDice;
use Vashakidze\Telegram\Api\InputTypes\SendDocument;
use Vashakidze\Telegram\Api\InputTypes\SendInvoice;
use Vashakidze\Telegram\Api\InputTypes\SendLocation;
use Vashakidze\Telegram\Api\InputTypes\SendMediaGroup;
use Vashakidze\Telegram\Api\InputTypes\SendMessage;
use Vashakidze\Telegram\Api\InputTypes\SendPhoto;
use Vashakidze\Telegram\Api\InputTypes\SendPoll;
use Vashakidze\Telegram\Api\InputTypes\SendVenue;
use Vashakidze\Telegram\Api\InputTypes\SendVideo;
use Vashakidze\Telegram\Api\InputTypes\SendVideoNote;
use Vashakidze\Telegram\Api\InputTypes\SendVoice;
use Vashakidze\Telegram\Api\InputTypes\SetChatAdministratorCustomTitle;
use Vashakidze\Telegram\Api\InputTypes\SetChatDescription;
use Vashakidze\Telegram\Api\InputTypes\SetChatMenuButton;
use Vashakidze\Telegram\Api\InputTypes\SetChatPermissions;
use Vashakidze\Telegram\Api\InputTypes\SetChatPhoto;
use Vashakidze\Telegram\Api\InputTypes\SetChatStickerSet;
use Vashakidze\Telegram\Api\InputTypes\SetChatTitle;
use Vashakidze\Telegram\Api\InputTypes\SetMyCommands;
use Vashakidze\Telegram\Api\InputTypes\SetMyDefaultAdministratorRights;
use Vashakidze\Telegram\Api\InputTypes\SetWebhook;
use Vashakidze\Telegram\Api\InputTypes\StopMessageLiveLocation;
use Vashakidze\Telegram\Api\InputTypes\UnbanChatMember;
use Vashakidze\Telegram\Api\InputTypes\UnbanChatSenderChat;
use Vashakidze\Telegram\Api\InputTypes\UnpinAllChatMessages;
use Vashakidze\Telegram\Api\InputTypes\UnpinChatMessage;
use Vashakidze\Telegram\Api\Types\Chat;
use Vashakidze\Telegram\Api\Types\ChatAdministratorRights;
use Vashakidze\Telegram\Api\Types\ChatInviteLink;
use Vashakidze\Telegram\Api\Types\ChatMember;
use Vashakidze\Telegram\Api\Types\File;
use Vashakidze\Telegram\Api\Types\MenuButton;
use Vashakidze\Telegram\Api\Types\Message;
use Vashakidze\Telegram\Api\Types\MessageId;
use Vashakidze\Telegram\Api\Types\SentWebAppMessage;
use Vashakidze\Telegram\Api\Types\Update;
use Vashakidze\Telegram\Api\Types\User;
use Vashakidze\Telegram\Api\Types\UserProfilePhotos;
use Vashakidze\Telegram\Api\Types\WebhookInfo;

/**
 * Class Telegram
 * @package Vashakidze\Telegram
 *
 * @see TelegramApi
 *
 * @link https://core.telegram.org/bots/api
 *
 * @method static User getMe()
 * @method static bool logOut()
 * @method static bool close()
 * @method static WebhookInfo getWebhookInfo()
 * @method static bool setWebhook(SetWebhook $args)
 * @method static bool deleteWebhook()
 * @method static Update[]|null getUpdates(?GetUpdates $args = null)
 * @method static Message sendMessage(SendMessage $args)
 * @method static Message forwardMessage(ForwardMessage $args)
 * @method static MessageId copyMessage(CopyMessage $args)
 * @method static Message sendPhoto(SendPhoto $args)
 * @method static Message sendAudio(SendAudio $args)
 * @method static Message sendDocument(SendDocument $args)
 * @method static Message sendVideo(SendVideo $args)
 * @method static Message sendAnimation(SendAnimation $args)
 * @method static Message sendVoice(SendVoice $args)
 * @method static Message sendVideoNote(SendVideoNote $args)
 * @method static Message[] sendMediaGroup(SendMediaGroup $args)
 * @method static Message sendLocation(SendLocation $args)
 * @method static Message|bool editMessageLiveLocation(EditMessageLiveLocation $args)
 * @method static Message|bool stopMessageLiveLocation(StopMessageLiveLocation $args)
 * @method static Message sendVenue(SendVenue $args)
 * @method static Message sendContact(SendContact $args)
 * @method static Message sendPoll(SendPoll $args)
 * @method static Message sendDice(SendDice $args)
 * @method static bool sendChatAction(SendChatAction $args)
 * @method static UserProfilePhotos getUserProfilePhotos(GetUserProfilePhotos $args)
 * @method static File getFile(GetFile $args)
 * @method static string downloadFile(string $filePath)
 * @method static bool banChatMember(BanChatMember $args)
 * @method static bool unbanChatMember(UnbanChatMember $args)
 * @method static bool restrictChatMember(RestrictChatMember $args)
 * @method static bool promoteChatMember(PromoteChatMember $args)
 * @method static bool setChatAdministratorCustomTitle(SetChatAdministratorCustomTitle $args)
 * @method static bool banChatSenderChat(BanChatSenderChat $args)
 * @method static bool unbanChatSenderChat(UnbanChatSenderChat $args)
 * @method static bool setChatPermissions(SetChatPermissions $args)
 * @method static string exportChatInviteLink(ExportChatInviteLink $args)
 * @method static ChatInviteLink createChatInviteLink(CreateChatInviteLink $args)
 * @method static ChatInviteLink editChatInviteLink(EditChatInviteLink $args)
 * @method static ChatInviteLink revokeChatInviteLink(RevokeChatInviteLink $args)
 * @method static bool approveChatJoinRequest(ApproveChatJoinRequest $args)
 * @method static bool declineChatJoinRequest(DeclineChatJoinRequest $args)
 * @method static bool setChatPhoto(SetChatPhoto $args)
 * @method static bool deleteChatPhoto(DeleteChatPhoto $args)
 * @method static bool setChatTitle(SetChatTitle $args)
 * @method static bool setChatDescription(SetChatDescription $args)
 * @method static bool pinChatMessage(PinChatMessage $args)
 * @method static bool unpinChatMessage(UnpinChatMessage $args)
 * @method static bool unpinAllChatMessages(UnpinAllChatMessages $args)
 * @method static bool leaveChat(LeaveChat $args)
 * @method static Chat getChat(GetChat $args)
 * @method static ChatMember[] getChatAdministrators(GetChatAdministrators $args)
 * @method static ChatMember getChatMember(GetChatMember $args)
 * @method static int getChatMemberCount(GetChatMemberCount $args)
 * @method static bool setChatStickerSet(SetChatStickerSet $args)
 * @method static bool deleteChatStickerSet(DeleteChatStickerSet $args)
 * @method static bool answerCallbackQuery(AnswerCallbackQuery $args)
 * @method static bool setMyCommands(SetMyCommands $args)
 * @method static bool deleteMyCommands(DeleteMyCommands $args)
 * @method static bool setChatMenuButton(SetChatMenuButton $args)
 * @method static MenuButton getChatMenuButton(GetChatMenuButton $args)
 * @method static bool setMyDefaultAdministratorRights(SetMyDefaultAdministratorRights $args)
 * @method static ChatAdministratorRights getMyDefaultAdministratorRights(GetMyDefaultAdministratorRights $args)
 * @method static bool answerInlineQuery(AnswerInlineQuery $args)
 * @method static SentWebAppMessage answerWebAppQuery(AnswerWebAppQuery $args)
 * @method static Message sendInvoice(SendInvoice $args)
 * @method static string createInvoiceLink(CreateInvoiceLink $args)
 * @method static bool answerShippingQuery(AnswerShippingQuery $args)
 * @method static bool answerPreCheckoutQuery(AnswerPreCheckoutQuery $args)
 */
class Telegram extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'vashakidze_telegram';
    }
}
